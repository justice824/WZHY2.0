package com.supermap.wzhy.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * TMicroIdent entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_MICRO_IDENT")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "tmicroidenmetas", "tmicroidenvls", "handler"})

public class TMicroIdent implements java.io.Serializable {

	// Fields

	private int miitid;
	private String name;
	private String memo;
	private int status;
	private Set<TMicroIdenmeta> TMicroidenmetas = new HashSet<TMicroIdenmeta>(0);
	private Set<TMicroIdenvl> TMicroidenvls = new HashSet<TMicroIdenvl>(0);

	// Constructors

	/** default constructor */
	public TMicroIdent() {
	}

	/** full constructor */
	public TMicroIdent(String name, String memo, int status, Set<TMicroIdenmeta> TMicroIdenmetas,
			Set<TMicroIdenvl> TMicroIdenvls) {
		this.name = name;
		this.memo = memo;
		this.status = status;
		this.TMicroidenmetas = TMicroIdenmetas;
		this.TMicroidenvls = TMicroIdenvls;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "MIITID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getMiitid() {
		return this.miitid;
	}

	public void setMiitid(int miitid) {
		this.miitid = miitid;
	}

	@Column(name = "NAME", length = 100)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "MEMO", length = 100)
	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TMicroIdent")
	public Set<TMicroIdenmeta> getTMicroidenmetas() {
		return this.TMicroidenmetas;
	}

	public void setTMicroidenmetas(Set<TMicroIdenmeta> TMicroIdenmetas) {
		this.TMicroidenmetas = TMicroIdenmetas;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TMicroIdent")
	public Set<TMicroIdenvl> getTMicroidenvls() {
		return this.TMicroidenvls;
	}

	public void setTMicroidenvls(Set<TMicroIdenvl> TMicroIdenvls) {
		this.TMicroidenvls = TMicroIdenvls;
	}

}