package com.supermap.wzhy.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * TUserpower entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_USERPOWER")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "tuserpowerrelations", "trolepowerrelations", "handler"})
public class TUserpower implements java.io.Serializable {

	// Fields

	private int powerid;
	private com.supermap.wzhy.entity.TUserpowercatalog TUserpowercatalog;
    @NotBlank(message = "{power.powername.notblank}")
	private String powerName;
	private String powerValue;
	private int parid;
	private int status;
	private int powerType;
	private int orderby;
	private String memo;
	private Set<TRolepowerrelation> TRolepowerrelations = new HashSet<TRolepowerrelation>(0);
	private Set<TUserpowerrelation> TUserpowerrelations = new HashSet<TUserpowerrelation>(0);

	// Constructors

	/** default constructor */
	public TUserpower() {
	}

	/** full constructor */
	public TUserpower(com.supermap.wzhy.entity.TUserpowercatalog TUserpowercatalog, String powerName, String powerValue, int parid, int status,
			int powerType, int orderby, String memo, Set<TRolepowerrelation> TRolepowerrelations,
			Set<TUserpowerrelation> TUserpowerrelations) {
		this.TUserpowercatalog = TUserpowercatalog;
		this.powerName = powerName;
		this.powerValue = powerValue;
		this.parid = parid;
		this.status = status;
		this.powerType = powerType;
		this.orderby = orderby;
		this.memo = memo;
		this.TRolepowerrelations = TRolepowerrelations;
		this.TUserpowerrelations = TUserpowerrelations;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "POWERID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getPowerid() {
		return this.powerid;
	}

	public void setPowerid(int powerid) {
		this.powerid = powerid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PCATAID")
	public com.supermap.wzhy.entity.TUserpowercatalog getTUserpowercatalog() {
		return this.TUserpowercatalog;
	}

	public void setTUserpowercatalog(com.supermap.wzhy.entity.TUserpowercatalog TUserpowercatalog) {
		this.TUserpowercatalog = TUserpowercatalog;
	}

	@Column(name = "POWER_NAME", length = 50)
	public String getPowerName() {
		return this.powerName;
	}

	public void setPowerName(String powerName) {
		this.powerName = powerName;
	}

	@Column(name = "POWER_VALUE", length = 20)
	public String getPowerValue() {
		return this.powerValue;
	}

	public void setPowerValue(String powerValue) {
		this.powerValue = powerValue;
	}

	@Column(name = "PARID", precision = 10, scale = 0)
	public int getParid() {
		return this.parid;
	}

	public void setParid(int parid) {
		this.parid = parid;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "POWER_TYPE", precision = 4, scale = 0)
	public int getPowerType() {
		return this.powerType;
	}

	public void setPowerType(int powerType) {
		this.powerType = powerType;
	}

	@Column(name = "ORDERBY", precision = 10, scale = 0)
	public int getOrderby() {
		return this.orderby;
	}

	public void setOrderby(int orderby) {
		this.orderby = orderby;
	}

	@Column(name = "MEMO", length = 50)
	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUserpower")
	public Set<TRolepowerrelation> getTRolepowerrelations() {
		return this.TRolepowerrelations;
	}

	public void setTRolepowerrelations(Set<TRolepowerrelation> TRolepowerrelations) {
		this.TRolepowerrelations = TRolepowerrelations;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUserpower")
	public Set<TUserpowerrelation> getTUserpowerrelations() {
		return this.TUserpowerrelations;
	}

	public void setTUserpowerrelations(Set<TUserpowerrelation> TUserpowerrelations) {
		this.TUserpowerrelations = TUserpowerrelations;
	}

}