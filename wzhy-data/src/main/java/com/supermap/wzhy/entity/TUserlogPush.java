package com.supermap.wzhy.entity;// default package

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * TUserlogPush entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_USERLOG_PUSH")
public class TUserlogPush implements java.io.Serializable {

	// Fields

	private int tulpid;
	private String username;
	private String userid;
	private Date fromtime;
	private Date totime;
	private String tablecode;
	private String tablename;
	private int successrecord;
	private String type;
	private Date starttime;
	private Date endtime;

	// Constructors

	/** default constructor */
	public TUserlogPush() {
	}

    public TUserlogPush(int tulpid) {
        this.tulpid = tulpid;
    }

    /** full constructor */
    public TUserlogPush(int tulpid, String username, String userid, Date fromtime, Date totime, String tablecode, String tablename, int successrecord, String type, Date starttime, Date endtime) {
        this.tulpid = tulpid;
        this.username = username;
        this.userid = userid;
        this.fromtime = fromtime;
        this.totime = totime;
        this.tablecode = tablecode;
        this.tablename = tablename;
        this.successrecord = successrecord;
        this.type = type;
        this.starttime = starttime;
        this.endtime = endtime;
    }

    // Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "TULPID", unique = true, nullable = false, precision = 22, scale = 0)
	public int getTulpid() {
		return this.tulpid;
	}

	public void setTulpid(int tulpid) {
		this.tulpid = tulpid;
	}

	@Column(name = "USERNAME", length = 50)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "USERID", length = 50)
	public String getUserid() {
		return this.userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "FROMTIME")
	public Date getFromtime() {
		return this.fromtime;
	}

	public void setFromtime(Date fromtime) {
		this.fromtime = fromtime;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "TOTIME")
	public Date getTotime() {
		return this.totime;
	}

	public void setTotime(Date totime) {
		this.totime = totime;
	}

	@Column(name = "TABLECODE", length = 100)
	public String getTablecode() {
		return this.tablecode;
	}

	public void setTablecode(String tablecode) {
		this.tablecode = tablecode;
	}

	@Column(name = "TABLENAME", length = 200)
	public String getTablename() {
		return this.tablename;
	}

	public void setTablename(String tablename) {
		this.tablename = tablename;
	}

	@Column(name = "SUCCESSRECORD", precision = 22, scale = 0)
	public int getSuccessrecord() {
		return this.successrecord;
	}

	public void setSuccessrecord(int successrecord) {
		this.successrecord = successrecord;
	}

	@Column(name = "TYPE", length = 50)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "STARTTIME")
	public Date getStarttime() {
		return this.starttime;
	}

	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "ENDTIME")
	public Date getEndtime() {
		return this.endtime;
	}

	public void setEndtime(Date endtime) {
		this.endtime = endtime;
	}

}