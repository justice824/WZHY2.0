package com.supermap.wzhy.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * TMicroTablemeta entity. @author MyEclipse Persistence Tools
 * 基层对象元数据信息
 */
@Entity
@Table(name = "T_MICRO_TABLEMETA")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "tmicrotableinfos", "tmicroidenmetas", "handler"})
public class TMicroTablemeta implements java.io.Serializable {

    // Fields

    private int mitmid;
    private com.supermap.wzhy.entity.TRegioncatalog TRegioncatalog;
    private TUsermajor tUsermajor;

    //	private int majorid;
    private String tableName;
    private String tableCode;
    private int mictableType;
    private String name;//基层对象名称
    private int refTableid;
    private int parid;
    private int year;
    private int month;
    private int micdataType;
    private int micmetaType;
    private int reportType;
    private String reportTypeName;
    private int permission;
    private String module;
    private int status;
    private String memo;
    private int orderby;
    private String flagA;
    private String flagB;
    private String flagC;
    private String viewName;
    private String miccatalog;
    private Set<TMicroTableinfo> TMicrotableinfos = new HashSet<TMicroTableinfo>(0);
    private Set<TMicroIdenmeta> TMicroidenmetas = new HashSet<TMicroIdenmeta>(0);//指标信息

    // Constructors

    /** default constructor */
    public TMicroTablemeta() {
    }

    /** full constructor */
    public TMicroTablemeta(com.supermap.wzhy.entity.TRegioncatalog TRegioncatalog, TUsermajor usermajor, String tableName, String tableCode,
                           int mictableType, String name, int refTableid, int parid, int year, int month, int micdataType,
                           int micmetaType, int reportType, String reportTypeName, int permission, String module, int status,
                           String memo, int orderby, String flagA, String flagB, String flagC, String viewName, Set<TMicroTableinfo> TMicroTableinfos,
                           Set<TMicroIdenmeta> TMicroIdenmetas) {
        this.TRegioncatalog = TRegioncatalog;
//		this.majorid = majorid;
        this.tUsermajor = usermajor;
        this.tableName = tableName;
        this.tableCode = tableCode;
        this.mictableType = mictableType;
        this.name = name;
        this.refTableid = refTableid;
        this.parid = parid;
        this.year = year;
        this.month = month;
        this.micdataType = micdataType;
        this.micmetaType = micmetaType;
        this.reportType = reportType;
        this.reportTypeName = reportTypeName;
        this.permission = permission;
        this.module = module;
        this.status = status;
        this.memo = memo;
        this.orderby = orderby;
        this.flagA = flagA;
        this.flagB = flagB;
        this.flagC = flagC;
        this.viewName = viewName;
        this.TMicrotableinfos = TMicroTableinfos;
        this.TMicroidenmetas = TMicroIdenmetas;
    }

    // Property accessors
    @GenericGenerator(name = "generator", strategy = "increment")
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "MITMID", unique = true, nullable = false, precision = 10, scale = 0)
    public int getMitmid() {
        return this.mitmid;
    }

    public void setMitmid(int mitmid) {
        this.mitmid = mitmid;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "RCID")
    public com.supermap.wzhy.entity.TRegioncatalog getTRegioncatalog() {
        return this.TRegioncatalog;
    }

    public void setTRegioncatalog(com.supermap.wzhy.entity.TRegioncatalog TRegioncatalog) {
        this.TRegioncatalog = TRegioncatalog;
    }

//	@Column(name = "MAJORID", precision = 10, scale = 0)
//	public int getMajorid() {
//		return this.majorid;
//	}
//
//	public void setMajorid(int majorid) {
//		this.majorid = majorid;
//	}

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MAJORID")
    public TUsermajor gettUsermajor() {
        return tUsermajor;
    }

    public void settUsermajor(TUsermajor tUsermajor) {
        this.tUsermajor = tUsermajor;
    }

    @Column(name = "TABLE_NAME", length = 40)
    public String getTableName() {
        return this.tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Column(name = "TABLE_CODE", length = 50)
    public String getTableCode() {
        return this.tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    @Column(name = "MICTABLE_TYPE", precision = 4, scale = 0)
    public int getMictableType() {
        return this.mictableType;
    }

    public void setMictableType(int mictableType) {
        this.mictableType = mictableType;
    }

    @Column(name = "NAME", length = 50)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "REF_TABLEID", precision = 10, scale = 0)
    public int getRefTableid() {
        return this.refTableid;
    }

    public void setRefTableid(int refTableid) {
        this.refTableid = refTableid;
    }

    @Column(name = "PARID", precision = 10, scale = 0)
    public int getParid() {
        return this.parid;
    }

    public void setParid(int parid) {
        this.parid = parid;
    }

    @Column(name = "YEAR", precision = 4, scale = 0)
    public int getYear() {
        return this.year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Column(name = "MONTH", precision = 4, scale = 0)
    public int getMonth() {
        return this.month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    @Column(name = "MICDATA_TYPE", precision = 4, scale = 0)
    public int getMicdataType() {
        return this.micdataType;
    }

    public void setMicdataType(int micdataType) {
        this.micdataType = micdataType;
    }

    @Column(name = "MICMETA_TYPE", precision = 4, scale = 0)
    public int getMicmetaType() {
        return this.micmetaType;
    }

    public void setMicmetaType(int micmetaType) {
        this.micmetaType = micmetaType;
    }

    @Column(name = "REPORT_TYPE", precision = 4, scale = 0)
    public int getReportType() {
        return this.reportType;
    }

    public void setReportType(int reportType) {
        this.reportType = reportType;
    }

    @Column(name = "REPORT_TYPE_NAME", length = 20)
    public String getReportTypeName() {
        return this.reportTypeName;
    }

    public void setReportTypeName(String reportTypeName) {
        this.reportTypeName = reportTypeName;
    }

    @Column(name = "PERMISSION", precision = 4, scale = 0)
    public int getPermission() {
        return this.permission;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    @Column(name = "MODULE", length = 15)
    public String getModule() {
        return this.module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    @Column(name = "STATUS", precision = 4, scale = 0)
    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Column(name = "MEMO", length = 200)
    public String getMemo() {
        return this.memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    @Column(name = "ORDERBY", precision = 10, scale = 0)
    public int getOrderby() {
        return this.orderby;
    }

    public void setOrderby(int orderby) {
        this.orderby = orderby;
    }

    @Column(name = "FLAG_A", length = 20)
    public String getFlagA() {
        return this.flagA;
    }

    public void setFlagA(String flagA) {
        this.flagA = flagA;
    }

    @Column(name = "FLAG_B", length = 20)
    public String getFlagB() {
        return this.flagB;
    }

    public void setFlagB(String flagB) {
        this.flagB = flagB;
    }

    @Column(name = "FLAG_C", length = 20)
    public String getFlagC() {
        return this.flagC;
    }

    public void setFlagC(String flagC) {
        this.flagC = flagC;
    }

    @Column(name = "VIEW_NAME", length = 50)
    public String getViewName() { return this.viewName; }

    public void setViewName(String viewName) { this.viewName = viewName; }


    @Column(name = "MICCATALOG", length = 100)
    public String getMiccatalog() {
        return this.miccatalog;
    }

    public void setMiccatalog(String miccatalog) {
        this.miccatalog = miccatalog;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TMicroTablemeta")
    public Set<TMicroTableinfo> getTMicrotableinfos() {
        return this.TMicrotableinfos;
    }

    public void setTMicrotableinfos(Set<TMicroTableinfo> TMicroTableinfos) {
        this.TMicrotableinfos = TMicroTableinfos;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TMicroTablemeta")
    public Set<TMicroIdenmeta> getTMicroidenmetas() {
        return this.TMicroidenmetas;
    }

    public void setTMicroidenmetas(Set<TMicroIdenmeta> TMicroIdenmetas) {
        this.TMicroidenmetas = TMicroIdenmetas;
    }

}