package com.supermap.wzhy.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * TMicroIdenvl entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_MICRO_IDENVL")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer","tmajortraderelations", "handler"})
public class TMicroIdenvl implements java.io.Serializable {

	// Fields

	private int mimvid;
	private com.supermap.wzhy.entity.TMicroIdent TMicroIdent;
	private String code;
	private String name;
	private int status;
	private String parid;
	private String memo;


    // Constructors

	/** default constructor */
	public TMicroIdenvl() {
	}

	/** full constructor */
	public TMicroIdenvl(com.supermap.wzhy.entity.TMicroIdent TMicroIdent, String code, String name, int status, String parid, String memo) {
		this.TMicroIdent = TMicroIdent;
		this.code = code;
		this.name = name;
		this.status = status;
		this.parid = parid;
		this.memo = memo;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "MIMVID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getMimvid() {
		return this.mimvid;
	}

	public void setMimvid(int mimvid) {
		this.mimvid = mimvid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MIITID")
	public com.supermap.wzhy.entity.TMicroIdent getTMicroIdent() {
		return this.TMicroIdent;
	}

	public void setTMicroIdent(com.supermap.wzhy.entity.TMicroIdent TMicroIdent) {
		this.TMicroIdent = TMicroIdent;
	}

	@Column(name = "CODE", length = 10)
	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "PARID", length = 10)
	public String getParid() {
		return this.parid;
	}

	public void setParid(String parid) {
		this.parid = parid;
	}

	@Column(name = "MEMO", length = 50)
	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

}