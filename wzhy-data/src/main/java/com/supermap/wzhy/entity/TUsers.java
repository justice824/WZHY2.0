package com.supermap.wzhy.entity;// default package

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * TUsers entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_USERS")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "tuserrolerelations", "tusermajorrelations","tdatauserpowers","tuserpowerrelations","password", "handler"})
public class TUsers implements java.io.Serializable {

	// Fields

	private int userid;
	private String userName;
	private String userCaption;
	private String userRegion;
	private String userPartment;
	private String password;
	private String memo;
	private int status;//状态：1表示可用，0表示不可用
	private String email;
	private String phone;
	private String flagA;
	private String flagB;
	private String flagC;
	private Set<TUserpowerrelation> TUserpowerrelations = new HashSet<TUserpowerrelation>(
			0);
	private Set<TUsermajorrelation> TUsermajorrelations = new HashSet<TUsermajorrelation>(
			0);
	private Set<TUserrolerelation> TUserrolerelations = new HashSet<TUserrolerelation>(
			0);
    private int sys_role;//角色：1表示管理员角色，2表示名录库角色，3表示法人库角色
	// Constructors

	/** default constructor */
	public TUsers() {
	}

	/** full constructor */
	public TUsers(String userName, String userCaption, String userRegion,
			String userPartment, String password, String memo, Short status,
			String email, String phone, String flagA, String flagB,
			String flagC, Set<TUserpowerrelation> TUserpowerrelations,
			Set<TUsermajorrelation> TUsermajorrelations,
			Set<TUserrolerelation> TUserrolerelations) {
		this.userName = userName;
		this.userCaption = userCaption;
		this.userRegion = userRegion;
		this.userPartment = userPartment;
		this.password = password;
		this.memo = memo;
		this.status = status;
		this.email = email;
		this.phone = phone;
		this.flagA = flagA;
		this.flagB = flagB;
		this.flagC = flagC;
		this.TUserpowerrelations = TUserpowerrelations;
		this.TUsermajorrelations = TUsermajorrelations;
		this.TUserrolerelations = TUserrolerelations;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "USERID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getUserid() {
		return this.userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	@Column(name = "USER_NAME", length = 50)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "USER_CAPTION", length = 50)
	public String getUserCaption() {
		return this.userCaption;
	}

	public void setUserCaption(String userCaption) {
		this.userCaption = userCaption;
	}

	@Column(name = "USER_REGION", length = 12)
	public String getUserRegion() {
		return this.userRegion;
	}

	public void setUserRegion(String userRegion) {
		this.userRegion = userRegion;
	}

	@Column(name = "USER_PARTMENT", length = 50)
	public String getUserPartment() {
		return this.userPartment;
	}

	public void setUserPartment(String userPartment) {
		this.userPartment = userPartment;
	}

	@Column(name = "PASSWORD", length = 32)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "MEMO", length = 100)
	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "EMAIL", length = 50)
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "PHONE", length = 20)
	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name = "FLAG_A", length = 20)
	public String getFlagA() {
		return this.flagA;
	}

	public void setFlagA(String flagA) {
		this.flagA = flagA;
	}

	@Column(name = "FLAG_B", length = 20)
	public String getFlagB() {
		return this.flagB;
	}

	public void setFlagB(String flagB) {
		this.flagB = flagB;
	}

	@Column(name = "FLAG_C", length = 20)
	public String getFlagC() {
		return this.flagC;
	}

	public void setFlagC(String flagC) {
		this.flagC = flagC;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUsers")
	public Set<TUserpowerrelation> getTUserpowerrelations() {
		return this.TUserpowerrelations;
	}

	public void setTUserpowerrelations(
			Set<TUserpowerrelation> TUserpowerrelations) {
		this.TUserpowerrelations = TUserpowerrelations;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUsers")
	public Set<TUsermajorrelation> getTUsermajorrelations() {
		return this.TUsermajorrelations;
	}

	public void setTUsermajorrelations(
			Set<TUsermajorrelation> TUsermajorrelations) {
		this.TUsermajorrelations = TUsermajorrelations;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUsers")
	public Set<TUserrolerelation> getTUserrolerelations() {
		return this.TUserrolerelations;
	}

	public void setTUserrolerelations(Set<TUserrolerelation> TUserrolerelations) {
		this.TUserrolerelations = TUserrolerelations;
	}


    @Column(name= "SYS_ROLE" ,precision = 4,scale = 0)
    public int getSys_role(){
        return this.sys_role;
    }
    public void setSys_role(int sys_role){
        this.sys_role = sys_role;
    }
}