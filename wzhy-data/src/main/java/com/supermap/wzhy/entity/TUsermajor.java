package com.supermap.wzhy.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * TUsermajor entity. @author MyEclipse Persistence Tools
 * 专业信息
 */
@Entity
@Table(name = "T_USERMAJOR")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "tusermajorrelations","tmajortraderelations", "handler"})
public class TUsermajor implements java.io.Serializable {

	// Fields

	private int majorid;
	private String majorname;
	private String majorvalue;
	private String memo;
	private int status;
    private int datatype;
	private Set<TUsermajorrelation> TUsermajorrelations = new HashSet<TUsermajorrelation>(0);

	// Constructors

	/** default constructor */
	public TUsermajor() {
	}

	/** full constructor */
	public TUsermajor(String majorname, String majorvalue, String memo, int status,
			Set<TUsermajorrelation> TUsermajorrelations) {
		this.majorname = majorname;
		this.majorvalue = majorvalue;
		this.memo = memo;
		this.status = status;
		this.TUsermajorrelations = TUsermajorrelations;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "MAJORID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getMajorid() {
		return this.majorid;
	}

	public void setMajorid(int majorid) {
		this.majorid = majorid;
	}

	@Column(name = "MAJORNAME", length = 50)
	public String getMajorname() {
		return this.majorname;
	}

	public void setMajorname(String majorname) {
		this.majorname = majorname;
	}

	@Column(name = "MAJORVALUE", length = 10)
	public String getMajorvalue() {
		return this.majorvalue;
	}

	public void setMajorvalue(String majorvalue) {
		this.majorvalue = majorvalue;
	}

	@Column(name = "MEMO", length = 100)
	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Column(name = "DATATYPE", precision = 4, scale = 0)
	public int getDatatype() {
		return this.datatype;
	}

	public void setDatatype(int datatype) {
		this.datatype = datatype;
	}


    @Column(name = "STATUS", precision = 4, scale = 0)
    public int getStatus() {
        return this.status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUsermajor")
	public Set<TUsermajorrelation> getTUsermajorrelations() {
		return this.TUsermajorrelations;
	}

	public void setTUsermajorrelations(Set<TUsermajorrelation> TUsermajorrelations) {
		this.TUsermajorrelations = TUsermajorrelations;
	}

}