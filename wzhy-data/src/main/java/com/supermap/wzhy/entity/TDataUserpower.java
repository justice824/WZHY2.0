package com.supermap.wzhy.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * TDataUserpower entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_DATA_USERPOWER")
public class TDataUserpower implements java.io.Serializable {

	// Fields

	private int dupid;
	private com.supermap.wzhy.entity.TUsers TUsers;
	private int dataType;
	private int dataId;
	private int status;

	// Constructors

	/** default constructor */
	public TDataUserpower() {
	}

	/** full constructor */
	public TDataUserpower(com.supermap.wzhy.entity.TUsers TUsers, int dataType, int dataId, int status) {
		this.TUsers = TUsers;
		this.dataType = dataType;
		this.dataId = dataId;
		this.status = status;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "DUPID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getDupid() {
		return this.dupid;
	}

	public void setDupid(int dupid) {
		this.dupid = dupid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USERID")
	public com.supermap.wzhy.entity.TUsers getTUsers() {
		return this.TUsers;
	}

	public void setTUsers(com.supermap.wzhy.entity.TUsers TUsers) {
		this.TUsers = TUsers;
	}

	@Column(name = "DATA_TYPE", precision = 4, scale = 0)
	public int getDataType() {
		return this.dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	@Column(name = "DATA_ID", precision = 10, scale = 0)
	public int getDataId() {
		return this.dataId;
	}

	public void setDataId(int dataId) {
		this.dataId = dataId;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}