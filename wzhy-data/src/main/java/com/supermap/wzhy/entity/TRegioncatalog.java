package com.supermap.wzhy.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * TRegioncatalog entity. @author MyEclipse Persistence Tools
 * 地图配置信息（行政区划类型）
 */
@Entity
@Table(name = "T_REGIONCATALOG")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "tregioninfos","tmacroperiods", "tmicrotablemetas", "handler"})
public class TRegioncatalog implements java.io.Serializable {

	// Fields(目前图层服务URL未使用)

	private int rcid;//地图编号
	private String name;//地图名
	private int status;//状态（0不可用，1可用，2默认显示）
	private int year;//地图所属年份
	private String dataSource;//数据源别名
	private String dataSet;//包含数据集
	private String utfGrid;//UTFGrid服务URL
	private String dataService;//数据集查询服务URL
	private String analysisService;//空间分析服务URL
    private String queryService;//图层查询服务URL
    private String boundarylayer;//区划边界图层服务URL
    private String dbusername;//数据源所在DB用户名




    private String regiontype;
    private String memo;
	private String remark;
	private Set<TRegioninfo> TRegioninfos = new HashSet<TRegioninfo>(0);//关联行政区划
	private Set<TMicroTablemeta> TMicrotablemetas = new HashSet<TMicroTablemeta>(0);//基层对象元数据信息

	// Constructors

	/** default constructor */
	public TRegioncatalog() {
	}

	/** full constructor */
	public TRegioncatalog(String name, int status, int year, String dataSource, String dataSet, String utfGrid,
			String dataService, String analysisService, String queryService, String boundarylayer, String dbusername,
            String memo, Set<TRegioninfo> TRegioninfos, Set<TMicroTablemeta> TMicroTablemetas) {
		this.name = name;
		this.status = status;
		this.year = year;
		this.dataSource = dataSource;
		this.dataSet = dataSet;
		this.utfGrid = utfGrid;
		this.dataService = dataService;
		this.analysisService = analysisService;
        this.queryService = queryService;
        this.boundarylayer = boundarylayer;
        this.dbusername = dbusername;
		this.memo = memo;
		this.TRegioninfos = TRegioninfos;
		this.TMicrotablemetas = TMicroTablemetas;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "RCID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getRcid() {
		return this.rcid;
	}
	public void setRcid(int rcid) {
		this.rcid = rcid;
	}

	@Column(name = "NAME", length = 20)
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}
	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "YEAR", precision = 4, scale = 0)
	public int getYear() {
		return this.year;
	}
	public void setYear(int year) {
		this.year = year;
	}

	@Column(name = "DATA_SOURCE", length = 20)
	public String getDataSource() {
		return this.dataSource;
	}
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}

	@Column(name = "DATA_SET", length = 20)
	public String getDataSet() {
		return this.dataSet;
	}
	public void setDataSet(String dataSet) {
		this.dataSet = dataSet;
	}

	@Column(name = "UTFGRID", length = 200)
	public String getUtfGrid() { return this.utfGrid;}
	public void setUtfGrid(String utfGrid) {
		this.utfGrid = utfGrid;
	}

	@Column(name = "DATASERVICE", length = 200)
	public String getDataService() {
		return this.dataService;
	}
	public void setDataService(String dataService) {
		this.dataService = dataService;
	}

	@Column(name = "ANALYSISSERVICE", length = 200)
	public String getAnalysisService() {
		return this.analysisService;
	}
	public void setAnalysisService(String analysisService) {
		this.analysisService = analysisService;
	}

    @Column(name = "QUERYSERVICE", length = 200)
    public String getQueryService() {
        return this.queryService;
    }
    public void setQueryService(String queryService) {
        this.queryService = queryService;
    }

    @Column(name = "BOUNDARYLAYER", length = 300)
    public String getBoundaryLayer() {
        return this.boundarylayer;
    }
    public void setBoundaryLayer(String boundarylayer) {
        this.boundarylayer = boundarylayer;
    }

    @Column(name = "DB_USERNAME", length = 20)
    public String getDbusername() {
        return this.dbusername;
    }
    public void setDbusername(String dbusername) { this.dbusername = dbusername;}

    @Column(name="REGIONTYPE",length = 20)
    public String getRegiontype() { return regiontype; }
    public void setRegiontype(String regiontype) {  this.regiontype = regiontype; }

	@Column(name = "REMARK", length = 200)
	public String getRemark() {
		return this.remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}

    @Column(name = "MEMO", length = 200)
    public String getMemo() { return memo;}
    public void setMemo(String memo) { this.memo = memo;}

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TRegioncatalog")
	public Set<TRegioninfo> getTRegioninfos() {
		return this.TRegioninfos;
	}
	public void setTRegioninfos(Set<TRegioninfo> TRegioninfos) {
		this.TRegioninfos = TRegioninfos;
	}


	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TRegioncatalog")
	public Set<TMicroTablemeta> getTMicrotablemetas() {
		return this.TMicrotablemetas;
	}
	public void setTMicrotablemetas(Set<TMicroTablemeta> TMicroTablemetas) {
		this.TMicrotablemetas = TMicroTablemetas;
	}
}