package com.supermap.wzhy.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * TUserpowercatalog entity. @author MyEclipse Persistence Tools
 * 用户权限分类信息
 */
@Entity
@Table(name = "T_USERPOWERCATALOG")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "tuserpowers", "handler"})
public class TUserpowercatalog implements java.io.Serializable {

	// Fields

	private int pcataid;
	private String name;
	private int status;
	private String memo;
	private Set<TUserpower> TUserpowers = new HashSet<TUserpower>(0);

	// Constructors

	/** default constructor */
	public TUserpowercatalog() {
	}

	/** full constructor */
	public TUserpowercatalog(String name, int status, String memo, Set<TUserpower> TUserpowers) {
		this.name = name;
		this.status = status;
		this.memo = memo;
		this.TUserpowers = TUserpowers;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "PCATAID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getPcataid() {
		return this.pcataid;
	}

	public void setPcataid(int pcataid) {
		this.pcataid = pcataid;
	}

	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "MEMO", length = 50)
	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUserpowercatalog")
	public Set<TUserpower> getTUserpowers() {
		return this.TUserpowers;
	}

	public void setTUserpowers(Set<TUserpower> TUserpowers) {
		this.TUserpowers = TUserpowers;
	}

}