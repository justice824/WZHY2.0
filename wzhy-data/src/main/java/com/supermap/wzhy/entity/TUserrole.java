package com.supermap.wzhy.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * TUserrole entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_USERROLE")
@JsonIgnoreProperties(value = {"hibernateLazyInitializer", "trolepowerrelations", "tdatarolepowers", "tuserrolerelations","handler"})
public class TUserrole implements java.io.Serializable {

	// Fields

	private int roleid;
	private String rolename;
	private String rolememo;
	private int status;
	private int parid;
	private Set<TRolepowerrelation> TRolepowerrelations = new HashSet<TRolepowerrelation>(0);
	private Set<TDataRolepower> TDatarolepowers = new HashSet<TDataRolepower>(0);
	private Set<TUserrolerelation> TUserrolerelations = new HashSet<TUserrolerelation>(0);

	// Constructors

	/** default constructor */
	public TUserrole() {
	}

	/** full constructor */
	public TUserrole(String rolename, String rolememo, int status, int parid,
			Set<TRolepowerrelation> TRolepowerrelations, Set<TDataRolepower> TDatarolepowers,
			Set<TUserrolerelation> TUserrolerelations) {
		this.rolename = rolename;
		this.rolememo = rolememo;
		this.status = status;
		this.parid = parid;
		this.TRolepowerrelations = TRolepowerrelations;
		this.TDatarolepowers = TDatarolepowers;
		this.TUserrolerelations = TUserrolerelations;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "ROLEID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getRoleid() {
		return this.roleid;
	}

	public void setRoleid(int roleid) {
		this.roleid = roleid;
	}

	@Column(name = "ROLENAME", length = 110)
	public String getRolename() {
		return this.rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

	@Column(name = "ROLEMEMO", length = 100)
	public String getRolememo() {
		return this.rolememo;
	}

	public void setRolememo(String rolememo) {
		this.rolememo = rolememo;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "PARID", precision = 10, scale = 0)
	public int getParid() {
		return this.parid;
	}

	public void setParid(int parid) {
		this.parid = parid;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUserrole")
	public Set<TRolepowerrelation> getTRolepowerrelations() {
		return this.TRolepowerrelations;
	}

	public void setTRolepowerrelations(Set<TRolepowerrelation> TRolepowerrelations) {
		this.TRolepowerrelations = TRolepowerrelations;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUserrole")
	public Set<TDataRolepower> getTDatarolepowers() {
		return this.TDatarolepowers;
	}

	public void setTDatarolepowers(Set<TDataRolepower> TDatarolepowers) {
		this.TDatarolepowers = TDatarolepowers;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "TUserrole")
	public Set<TUserrolerelation> getTUserrolerelations() {
		return this.TUserrolerelations;
	}

	public void setTUserrolerelations(Set<TUserrolerelation> TUserrolerelations) {
		this.TUserrolerelations = TUserrolerelations;
	}

}