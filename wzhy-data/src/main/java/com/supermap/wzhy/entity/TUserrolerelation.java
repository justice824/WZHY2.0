package com.supermap.wzhy.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * TUserrolerelation entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_USERROLERELATION")
public class TUserrolerelation implements java.io.Serializable {

	// Fields

	private int urrid;
	private com.supermap.wzhy.entity.TUsers TUsers;
	private com.supermap.wzhy.entity.TUserrole TUserrole;

	// Constructors

	/** default constructor */
	public TUserrolerelation() {
	}

	/** full constructor */
	public TUserrolerelation(com.supermap.wzhy.entity.TUsers TUsers, com.supermap.wzhy.entity.TUserrole TUserrole) {
		this.TUsers = TUsers;
		this.TUserrole = TUserrole;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "URRID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getUrrid() {
		return this.urrid;
	}

	public void setUrrid(int urrid) {
		this.urrid = urrid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USERID")
	public com.supermap.wzhy.entity.TUsers getTUsers() {
		return this.TUsers;
	}

	public void setTUsers(com.supermap.wzhy.entity.TUsers TUsers) {
		this.TUsers = TUsers;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROLEID")
	public com.supermap.wzhy.entity.TUserrole getTUserrole() {
		return this.TUserrole;
	}

	public void setTUserrole(com.supermap.wzhy.entity.TUserrole TUserrole) {
		this.TUserrole = TUserrole;
	}

}