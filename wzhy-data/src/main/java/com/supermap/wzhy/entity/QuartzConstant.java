package com.supermap.wzhy.entity;

/**
 * Created by Sun'fei on 17-2-17.
 * 定时器常量类
 */
public class QuartzConstant {

    //文件柜系统定时器开关
    private boolean WjgSwitch = false;

    //文件柜系统定时器时间间隔
    private String wjgIntervals = "2000-01-01";

    //法人库定时器开关
    private boolean FrkSwitch = false;

    //法人库定时器时间间隔
    private String FrkIntervals = "2000-01-01";

    //名录库定时器开关
    private boolean MlkSwitch = false;

    //名录库定时器时间间隔
    private String MlkIntervals = "2000-01-01";


    public String getMlkIntervals() {
        return MlkIntervals;
    }

    public void setMlkIntervals(String mlkIntervals) {
        MlkIntervals = mlkIntervals;
    }

    public boolean isWjgSwitch() {
        return WjgSwitch;
    }

    public void setWjgSwitch(boolean wjgSwitch) { WjgSwitch = wjgSwitch; }

    public String getWjgIntervals() {
        return wjgIntervals;
    }

    public void setWjgIntervals(String wjgIntervals) {
        this.wjgIntervals = wjgIntervals;
    }

    public boolean isFrkSwitch() {
        return FrkSwitch;
    }

    public void setFrkSwitch(boolean frkSwitch) {
        FrkSwitch = frkSwitch;
    }

    public String getFrkIntervals() {
        return FrkIntervals;
    }

    public void setFrkIntervals(String frkIntervals) {
        FrkIntervals = frkIntervals;
    }

    public boolean isMlkSwitch() {
        return MlkSwitch;
    }

    public void setMlkSwitch(boolean mlkSwitch) {
        MlkSwitch = mlkSwitch;
    }

}
