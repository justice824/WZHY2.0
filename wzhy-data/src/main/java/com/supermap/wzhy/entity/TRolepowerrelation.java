package com.supermap.wzhy.entity;// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 * TRolepowerrelation entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_ROLEPOWERRELATION")
public class TRolepowerrelation implements java.io.Serializable {

	// Fields

	private Long rprid;
	private TUserpower TUserpower;
	private TUserrole TUserrole;

	// Constructors

	/** default constructor */
	public TRolepowerrelation() {
	}

	/** full constructor */
	public TRolepowerrelation(TUserpower TUserpower, TUserrole TUserrole) {
		this.TUserpower = TUserpower;
		this.TUserrole = TUserrole;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "RPRID", unique = true, nullable = false, precision = 10, scale = 0)
	public Long getRprid() {
		return this.rprid;
	}

	public void setRprid(Long rprid) {
		this.rprid = rprid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "POWERID")
	public TUserpower getTUserpower() {
		return this.TUserpower;
	}

	public void setTUserpower(TUserpower TUserpower) {
		this.TUserpower = TUserpower;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROLEID")
	public TUserrole getTUserrole() {
		return this.TUserrole;
	}

	public void setTUserrole(TUserrole TUserrole) {
		this.TUserrole = TUserrole;
	}

}