package com.supermap.wzhy.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * TDataRolepower entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_DATA_ROLEPOWER")
public class TDataRolepower implements java.io.Serializable {

	// Fields

	private int drpid;
	private com.supermap.wzhy.entity.TUserrole TUserrole;
	private int dataType;
	private int dataId;
	private int status;

	// Constructors

	/** default constructor */
	public TDataRolepower() {
	}

	/** full constructor */
	public TDataRolepower(com.supermap.wzhy.entity.TUserrole TUserrole, int dataType, int dataId, int status) {
		this.TUserrole = TUserrole;
		this.dataType = dataType;
		this.dataId = dataId;
		this.status = status;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "DRPID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getDrpid() {
		return this.drpid;
	}

	public void setDrpid(int drpid) {
		this.drpid = drpid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ROLEID")
	public com.supermap.wzhy.entity.TUserrole getTUserrole() {
		return this.TUserrole;
	}

	public void setTUserrole(com.supermap.wzhy.entity.TUserrole TUserrole) {
		this.TUserrole = TUserrole;
	}

	@Column(name = "DATA_TYPE", precision = 4, scale = 0)
	public int getDataType() {
		return this.dataType;
	}

	public void setDataType(int dataType) {
		this.dataType = dataType;
	}

	@Column(name = "DATA_ID", precision = 10, scale = 0)
	public int getDataId() {
		return this.dataId;
	}

	public void setDataId(int dataId) {
		this.dataId = dataId;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

}