package com.supermap.wzhy.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * TUserpowerrelation entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_USERPOWERRELATION")
public class TUserpowerrelation implements java.io.Serializable {

	// Fields

	private int uprid;
	private com.supermap.wzhy.entity.TUserpower TUserpower;
	private com.supermap.wzhy.entity.TUsers TUsers;

	// Constructors

	/** default constructor */
	public TUserpowerrelation() {
	}

	/** full constructor */
	public TUserpowerrelation(com.supermap.wzhy.entity.TUserpower TUserpower, com.supermap.wzhy.entity.TUsers TUsers) {
		this.TUserpower = TUserpower;
		this.TUsers = TUsers;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "UPRID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getUprid() {
		return this.uprid;
	}

	public void setUprid(int uprid) {
		this.uprid = uprid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "POWERID")
	public com.supermap.wzhy.entity.TUserpower getTUserpower() {
		return this.TUserpower;
	}

	public void setTUserpower(com.supermap.wzhy.entity.TUserpower TUserpower) {
		this.TUserpower = TUserpower;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USERID")
	public com.supermap.wzhy.entity.TUsers getTUsers() {
		return this.TUsers;
	}

	public void setTUsers(com.supermap.wzhy.entity.TUsers TUsers) {
		this.TUsers = TUsers;
	}

}