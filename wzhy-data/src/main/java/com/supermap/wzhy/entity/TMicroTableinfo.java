package com.supermap.wzhy.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * TMicroTableinfo entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_MICRO_TABLEINFO")
public class TMicroTableinfo implements java.io.Serializable {

	// Fields

	private int mitiid;
	private com.supermap.wzhy.entity.TMicroTablemeta TMicroTablemeta;
	private String tableName;
	private String tableCode;
	private int year;
	private int month;
	private int reportType;
	private String reportTypeName;
	private String name;
	private int status;
	private String flagA;
	private String flagB;
	private String flagC;

	// Constructors

	/** default constructor */
	public TMicroTableinfo() {
	}

	/** full constructor */
	public TMicroTableinfo(com.supermap.wzhy.entity.TMicroTablemeta TMicroTablemeta, String tableName, String tableCode, int year, int month,
			int reportType, String reportTypeName, String name, int status, String flagA, String flagB, String flagC) {
		this.TMicroTablemeta = TMicroTablemeta;
		this.tableName = tableName;
		this.tableCode = tableCode;
		this.year = year;
		this.month = month;
		this.reportType = reportType;
		this.reportTypeName = reportTypeName;
		this.name = name;
		this.status = status;
		this.flagA = flagA;
		this.flagB = flagB;
		this.flagC = flagC;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "MITIID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getMitiid() {
		return this.mitiid;
	}

	public void setMitiid(int mitiid) {
		this.mitiid = mitiid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MITMID")
	public com.supermap.wzhy.entity.TMicroTablemeta getTMicroTablemeta() {
		return this.TMicroTablemeta;
	}

	public void setTMicroTablemeta(com.supermap.wzhy.entity.TMicroTablemeta TMicroTablemeta) {
		this.TMicroTablemeta = TMicroTablemeta;
	}

	@Column(name = "TABLE_NAME", length = 40)
	public String getTableName() {
		return this.tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	@Column(name = "TABLE_CODE", length = 50)
	public String getTableCode() {
		return this.tableCode;
	}

	public void setTableCode(String tableCode) {
		this.tableCode = tableCode;
	}

	@Column(name = "YEAR", precision = 4, scale = 0)
	public int getYear() {
		return this.year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Column(name = "MONTH", precision = 4, scale = 0)
	public int getMonth() {
		return this.month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	@Column(name = "REPORT_TYPE", precision = 4, scale = 0)
	public int getReportType() {
		return this.reportType;
	}

	public void setReportType(int reportType) {
		this.reportType = reportType;
	}

	@Column(name = "REPORT_TYPE_NAME", length = 20)
	public String getReportTypeName() {
		return this.reportTypeName;
	}

	public void setReportTypeName(String reportTypeName) {
		this.reportTypeName = reportTypeName;
	}

	@Column(name = "NAME", length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "FLAG_A", length = 20)
	public String getFlagA() {
		return this.flagA;
	}

	public void setFlagA(String flagA) {
		this.flagA = flagA;
	}

	@Column(name = "FLAG_B", length = 20)
	public String getFlagB() {
		return this.flagB;
	}

	public void setFlagB(String flagB) {
		this.flagB = flagB;
	}

	@Column(name = "FLAG_C", length = 20)
	public String getFlagC() {
		return this.flagC;
	}

	public void setFlagC(String flagC) {
		this.flagC = flagC;
	}

}