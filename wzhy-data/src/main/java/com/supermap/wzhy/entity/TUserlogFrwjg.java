package com.supermap.wzhy.entity;// default package

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * TUserlogFrwjg entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_USERLOG_FRWJG")
public class TUserlogFrwjg implements java.io.Serializable {

	// Fields

    private BigDecimal id;
    private String username;
    private int userid;
    private int unitrecord;
    private Date fromtime;
    private Date totime;
    private Date starttime;
    private Date endtime;

    @GenericGenerator(name = "generator", strategy = "increment")
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "ID", precision = 22, scale = 0)
    public BigDecimal getId() {
        return this.id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    @Column(name = "USERNAME", length = 50)
    public String getUsername() {
        return this.username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "USERID", precision = 22, scale = 0)
    public int getUserid() {
        return this.userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    @Column(name = "UNITRECORD", precision = 22, scale = 0)
    public int getUnitrecord() {
        return this.unitrecord;
    }

    public void setUnitrecord(int unitrecord) {
        this.unitrecord = unitrecord;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "fromtime", length = 7)
    public Date getFromtime() {
        return this.fromtime;
    }

    public void setFromtime(Date fromtime) {
        this.fromtime = fromtime;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "TOTIME", length = 7)
    public Date getTotime() {
        return this.totime;
    }

    public void setTotime(Date totime) {
        this.totime = totime;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "STARTTIME", length = 7)
    public Date getStarttime() {
        return this.starttime;
    }

    public void setStarttime(Date starttime) {
        this.starttime = starttime;
    }

    @Temporal(TemporalType.DATE)
    @Column(name = "ENDTIME", length = 7)
    public Date getEndtime() {
        return this.endtime;
    }

    public void setEndtime(Date endtime) {
        this.endtime = endtime;
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + (getId() == null ? 0 : this.getId().hashCode());
        result = 37 * result
                + (getUsername() == null ? 0 : this.getUsername().hashCode());
      /*  result = 37 * result
                + (getUserid() == 0 ? 0 : this.getUserid().hashCode());
        result = 37
                * result
                + (getUnitrecord() == 0 ? 0 : this.getUnitrecord()
                .hashCode());*/
        result = 37 * result
                + (getFromtime() == null ? 0 : this.getFromtime().hashCode());
        result = 37 * result
                + (getTotime() == null ? 0 : this.getTotime().hashCode());
        result = 37 * result
                + (getStarttime() == null ? 0 : this.getStarttime().hashCode());
        result = 37 * result
                + (getEndtime() == null ? 0 : this.getEndtime().hashCode());
        return result;
    }
}