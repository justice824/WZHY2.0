package com.supermap.wzhy.entity;// default package

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * TParameters entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_PARAMETERS")
public class TParameters implements java.io.Serializable {

	// Fields


    // Fields

    private BigDecimal id;
    private String key;
    private String value;

    // Property accessors
    @GenericGenerator(name = "generator", strategy = "increment")
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "ID", precision = 22, scale = 0)
    public BigDecimal getId() {
        return this.id;
    }

    public void setId(BigDecimal id) {
        this.id = id;
    }

    @Column(name = "KEY", length = 100)
    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Column(name = "VALUE", length = 200)
    public String getValue() {
        return this.value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}