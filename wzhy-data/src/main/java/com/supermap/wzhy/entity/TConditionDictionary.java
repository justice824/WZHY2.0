package com.supermap.wzhy.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TUserlog entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_CONDITION_DICTIONARY")
public class TConditionDictionary implements java.io.Serializable {

    // Fields

    private int id;
    private String name;
    private String code;
    private String mark;

    public TConditionDictionary(){}

    public TConditionDictionary(int id, String name, String code, String mark) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.mark = mark;
    }

    // Property accessors
    @GenericGenerator(name = "generator", strategy = "increment")
    @Id
    @GeneratedValue(generator = "generator")
    @Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "NAME", length = 100)
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "CODE", length = 100)
    public String getCode() {
        return this.code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Column(name = "MARK", length = 100)
    public String getMark() {
        return this.mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

}