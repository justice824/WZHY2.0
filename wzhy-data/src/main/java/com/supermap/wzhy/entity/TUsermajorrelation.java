package com.supermap.wzhy.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * TUsermajorrelation entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_USERMAJORRELATION")
public class TUsermajorrelation implements java.io.Serializable {

	// Fields

	private int umrid;
	private com.supermap.wzhy.entity.TUsermajor TUsermajor;
	private com.supermap.wzhy.entity.TUsers TUsers;

	// Constructors

	/** default constructor */
	public TUsermajorrelation() {
	}

	/** full constructor */
	public TUsermajorrelation(com.supermap.wzhy.entity.TUsermajor TUsermajor, com.supermap.wzhy.entity.TUsers TUsers) {
		this.TUsermajor = TUsermajor;
		this.TUsers = TUsers;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "UMRID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getUmrid() {
		return this.umrid;
	}

	public void setUmrid(int umrid) {
		this.umrid = umrid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MAJORID")
	public com.supermap.wzhy.entity.TUsermajor getTUsermajor() {
		return this.TUsermajor;
	}

	public void setTUsermajor(com.supermap.wzhy.entity.TUsermajor TUsermajor) {
		this.TUsermajor = TUsermajor;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USERID")
	public com.supermap.wzhy.entity.TUsers getTUsers() {
		return this.TUsers;
	}

	public void setTUsers(com.supermap.wzhy.entity.TUsers TUsers) {
		this.TUsers = TUsers;
	}

}