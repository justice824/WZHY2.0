package com.supermap.wzhy.entity;// default package

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.GenericGenerator;

/**
 * TUserlog entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "T_USERLOG")
public class TUserlog implements java.io.Serializable {

	// Fields

	private BigDecimal id;
	private String username;
	private BigDecimal userid;
	private String ip;
	private String type;
	private Date time;
	private String msg;

	// Constructors

	/** default constructor */
	public TUserlog() {
	}

	/** full constructor */
	public TUserlog(String username, BigDecimal userid, String ip, String type,
			Date time, String msg) {
		this.username = username;
		this.userid = userid;
		this.ip = ip;
		this.type = type;
		this.time = time;
		this.msg = msg;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "ID", unique = true, nullable = false, precision = 22, scale = 0)
	public BigDecimal getId() {
		return this.id;
	}

	public void setId(BigDecimal id) {
		this.id = id;
	}

	@Column(name = "USERNAME", length = 50)
	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name = "USERID", precision = 22, scale = 0)
	public BigDecimal getUserid() {
		return this.userid;
	}

	public void setUserid(BigDecimal userid) {
		this.userid = userid;
	}

	@Column(name = "IP", length = 50)
	public String getIp() {
		return this.ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "TYPE", length = 50)
	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "TIME", length = 7)
	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Column(name = "MSG")
	public String getMsg() {
		return this.msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}