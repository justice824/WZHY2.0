package com.supermap.wzhy.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * TMicroIdenmeta entity. @author MyEclipse Persistence Tools
 * 基层指标信息
 */
@Entity
@Table(name = "T_MICRO_IDENMETA")
public class TMicroIdenmeta implements java.io.Serializable {

	// Fields

	private int miimid;
	private com.supermap.wzhy.entity.TMicroTablemeta TMicroTablemeta;
	private com.supermap.wzhy.entity.TMicroIdent TMicroIdent;
	private String idenCode;
	private String idenName;
	private int idenType;
	private int idenLength;
	private int idenPrecision;
	private String idenUnit;
	private int permission;
	private int status;
	private int orderby;
	private String memo;
	private String config;
    private BigDecimal isfjg;
    private String wjgName;
	// Constructors

	/** default constructor */
	public TMicroIdenmeta() {
	}

	/** full constructor */
	public TMicroIdenmeta(com.supermap.wzhy.entity.TMicroTablemeta TMicroTablemeta, com.supermap.wzhy.entity.TMicroIdent TMicroIdent, String idenCode, String idenName,
			int idenType, int idenLength, int idenPrecision, String idenUnit, int permission, int status, int orderby,
			String memo, String config,String wjgName) {
		this.TMicroTablemeta = TMicroTablemeta;
		this.TMicroIdent = TMicroIdent;
		this.idenCode = idenCode;
		this.idenName = idenName;
		this.idenType = idenType;
		this.idenLength = idenLength;
		this.idenPrecision = idenPrecision;
		this.idenUnit = idenUnit;
		this.permission = permission;
		this.status = status;
		this.orderby = orderby;
		this.memo = memo;
		this.config = config;
        this.wjgName = wjgName;
	}

	// Property accessors
	@GenericGenerator(name = "generator", strategy = "increment")
	@Id
	@GeneratedValue(generator = "generator")
	@Column(name = "MIIMID", unique = true, nullable = false, precision = 10, scale = 0)
	public int getMiimid() {
		return this.miimid;
	}

	public void setMiimid(int miimid) {
		this.miimid = miimid;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MITMID")
	public com.supermap.wzhy.entity.TMicroTablemeta getTMicroTablemeta() {
		return this.TMicroTablemeta;
	}

	public void setTMicroTablemeta(com.supermap.wzhy.entity.TMicroTablemeta TMicroTablemeta) {
		this.TMicroTablemeta = TMicroTablemeta;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MIITID")
	public com.supermap.wzhy.entity.TMicroIdent getTMicroIdent() {
		return this.TMicroIdent;
	}

	public void setTMicroIdent(com.supermap.wzhy.entity.TMicroIdent TMicroIdent) {
		this.TMicroIdent = TMicroIdent;
	}

	@Column(name = "IDEN_CODE", length = 10)
	public String getIdenCode() {
		return this.idenCode;
	}

	public void setIdenCode(String idenCode) {
		this.idenCode = idenCode;
	}

	@Column(name = "IDEN_NAME", length = 100)
	public String getIdenName() {
		return this.idenName;
	}

	public void setIdenName(String idenName) {
		this.idenName = idenName;
	}

	@Column(name = "IDEN_TYPE", precision = 4, scale = 0)
	public int getIdenType() {
		return this.idenType;
	}

	public void setIdenType(int idenType) {
		this.idenType = idenType;
	}

	@Column(name = "IDEN_LENGTH", precision = 10, scale = 0)
	public int getIdenLength() {
		return this.idenLength;
	}

	public void setIdenLength(int idenLength) {
		this.idenLength = idenLength;
	}

	@Column(name = "IDEN_PRECISION", precision = 10, scale = 0)
	public int getIdenPrecision() {
		return this.idenPrecision;
	}

	public void setIdenPrecision(int idenPrecision) {
		this.idenPrecision = idenPrecision;
	}

	@Column(name = "IDEN_UNIT", length = 20)
	public String getIdenUnit() {
		return this.idenUnit;
	}

	public void setIdenUnit(String idenUnit) {
		this.idenUnit = idenUnit;
	}

	@Column(name = "PERMISSION", precision = 4, scale = 0)
	public int getPermission() {
		return this.permission;
	}

	public void setPermission(int permission) {
		this.permission = permission;
	}

	@Column(name = "STATUS", precision = 4, scale = 0)
	public int getStatus() {
		return this.status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	@Column(name = "ORDERBY", precision = 10, scale = 0)
	public int getOrderby() {
		return this.orderby;
	}

	public void setOrderby(int orderby) {
		this.orderby = orderby;
	}

	@Column(name = "MEMO", length = 50)
	public String getMemo() {
		return this.memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	@Column(name = "CONFIG", length = 50)
	public String getConfig() {
		return this.config;
	}

	public void setConfig(String config) {
		this.config = config;
	}


    @Column(name = "ISFJG", precision = 22, scale = 0)
    public BigDecimal getIsfjg() {
        return this.isfjg;
    }

    public void setIsfjg(BigDecimal isfjg) {
        this.isfjg = isfjg;
    }

    @Column(name = "WJG_NAME", length = 10)
    public String getWjgName() {
        return this.wjgName;
    }

    public void setWjgName(String wjgName) {
        this.wjgName = wjgName;
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }
}