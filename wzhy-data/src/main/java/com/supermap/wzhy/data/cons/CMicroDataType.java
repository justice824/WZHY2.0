package com.supermap.wzhy.data.cons;

/**
 * MICDATA_TYPE：基层数据类型（1经济数据、2人口数据、3农业数据）
 *
 * @author Linhao on 15-02-09
 */
public class CMicroDataType {

    /**基层数据类型:经济数据*/
    public static final int UNIT_TYPE = 1;

    /**基层数据类型:人口数据*/
    public static final int POPULATION_TYPE = 2;

    /**基层数据类型:农业数据*/
    public static final int AGRICULTURE_TYPE = 3;
}
