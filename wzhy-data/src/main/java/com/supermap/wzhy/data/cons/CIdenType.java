package com.supermap.wzhy.data.cons;

/**
 * 指标类型
 *
 * @author Linhao by Linhao on 05-02-09
 */
public class CIdenType {

    /**
     * 指标类型：文本型
     */
    public static final int STRING = 1;

    /**
     * 指标类型：数值型
     */
    public static final int NUMBER = 2;

    /**
     * 指标类型：日期型
     */
    public static final int DATE = 3;

    /**
     * 指标类型：枚举型
     */
    public static final int ENUM = 4;

    /**
     * propertie number 属性数值，比如人口的年龄，身高等，不可用于累加计算的类型
     */
    public static final int PNUMBER = 5;
}
