package com.supermap.wzhy.data;

import java.security.MessageDigest;

/**
 * MD5 算法
 *
 * @author Created by W.Qiong on 14-8-15.
 */
public class MD5Util {

    /**
     * MD5编码
     * @param inStr
     * 			要编码的字符串
     * @return MD5编码后字符串
     */
    public static String encode(String inStr) {
        MessageDigest md5 = null;
        try {
            md5 = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            MessagePrintUtil.printException(e.toString());
            e.printStackTrace();
            return "";
        }
        char[] charArray = inStr.toCharArray();
        byte[] byteArray = new byte[charArray.length];

        for (int i = 0,len=charArray.length; i < len; i++)
            byteArray[i] = (byte) charArray[i];

        byte[] md5Bytes = md5.digest(byteArray);

        StringBuffer hexValue = new StringBuffer();

        for (int i = 0,len=md5Bytes.length; i < len; i++) {
            int val = ((int) md5Bytes[i]) & 0xff;
            if (val < 16)
                hexValue.append("0");
            hexValue.append(Integer.toHexString(val));
        }

        return hexValue.toString();
    }

}
