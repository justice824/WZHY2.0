package com.supermap.wzhy.data;

/**
 * 中文转拼音首字母
 *
 * @author Created by W.Qiong on 14-3-5.
 */
public class CnToSpell {

    /**
     * 将中文转为拼音首字母
     * <p style="color:red;">
     * &nbsp;&nbsp;说明：<br/>
     * &nbsp;&nbsp;最多只转换前四个中文字符
     * </p>
     * <p style="color:red;">
     * &nbsp;&nbsp;例如：<br/>
     * &nbsp;&nbsp;"中国" 返回 "ZG"
     * </p>
     *
     * @param cnstr
     *            中文
     * @return 拼音首字母(大写)
     */
    public static String getEn(String cnstr) {
        if (cnstr.length() > 4) {
            cnstr = cnstr.substring(0, 4);	// 只截取四位
        }
        String enstr = "";
        for (int index=0,len=cnstr.length(); index < len; index++) {
            char c = cnstr.charAt(index);
            if (c >= 33 && c <= 126) {		// 字符和数字保留
                enstr += c;
            } else {
                enstr += getFirstAlpha(String.valueOf(c));
            }
        }
        return enstr.toUpperCase();
    }

    /**
     * 将第一个中文转为拼音首字母
     *
     * @param cnchar
     * 			中文
     * @return 第一个中文字符对应的中文首字母（小写）
     */
    public static String getFirstAlpha(String cnchar) {
        byte[] array = new byte[2];
        try {
            array = String.valueOf(cnchar).getBytes("GBK");
        } catch (Exception e) {
            e.printStackTrace();
        }
        int i = (short) (array[0] - '\0' + 256) * 256
                + ((short) (array[1] - '\0' + 256));
        if (i < 0xB0A1)
            return "*";

        if (i < 0xB0C5)
            return "a";

        if (i < 0xB2C1)
            return "b";

        if (i < 0xB4EE)
            return "c";

        if (i < 0xB6EA)
            return "d";

        if (i < 0xB7A2)
            return "e";

        if (i < 0xB8C1)
            return "f";

        if (i < 0xB9FE)
            return "g";

        if (i < 0xBBF7)
            return "h";

        if (i < 0xBFA6)
            return "j";

        if (i < 0xC0AC)
            return "k";

        if (i < 0xC2E8)
            return "l";

        if (i < 0xC4C3)
            return "m";

        if (i < 0xC5B6)
            return "n";

        if (i < 0xC5BE)
            return "o";

        if (i < 0xC6DA)
            return "p";

        if (i < 0xC8BB)
            return "q";

        if (i < 0xC8F6)
            return "r";

        if (i < 0xCBFA)
            return "s";

        if (i < 0xCDDA)
            return "t";

        if (i < 0xCEF4)
            return "w";

        if (i < 0xD1B9)
            return "x";

        if (i < 0xD4D1)
            return "y";

        if (i < 0xD7FA)
            return "z";

        return "* ";
    }
}

