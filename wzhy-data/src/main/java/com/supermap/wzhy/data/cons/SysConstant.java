package com.supermap.wzhy.data.cons;

/**
 * 系统用户Session属性名配置
 * <p>
 * 说明：<br/>
 * 	用户存储session的name,取session值时，通过name来取值
 * </p>
 * @author Created by W.Hao on 14-2-11.
 */
public class SysConstant {
    /**
     * 当前登录用户信息(基本信息、专业对应、角色对应、系统权限对应、数据权限组)
     */
    public static final String CURRENT_USER = "current_user";

    /**
     * 当前登录用户所有专业信息
     */
    public static final String CURRENT_USRERMAJORS = "current_usermajors";

    /**
     * 当前登录用户所有角色信息
     */
    public static final String CURRENT_USRERROLES = "current_userroles";

    /**
     * 当前登录用户的所有系统权限信息（用户特有权限+用户角色权限）
     */
    public static final String CURRENT_POWERS = "current_powers";
}
