package com.supermap.wzhy.data.cons;

/**
 * 用来描述任务类型
 *
 * @author Created by Administrator on 14-3-8.
 */
public class TaskType {

    /**任务类型：数据分配任务*/
    public static final int DATA_DEAL_TASK = 1;
}
