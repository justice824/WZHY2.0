package com.supermap.wzhy.data;

/**
 * Created by Augustine on 2015/2/3.
 */
public class TipData {
    private String title;
    private String value;
    private String count;

    public TipData(String title){
        this.title = title;
    }

    public TipData(String title,String value){
        this.title = title;
        this.value = value;
    }

    public TipData(String title,String value,String count){
        this.title = title;
        this.value = value;
        this.count = count;
    }



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
