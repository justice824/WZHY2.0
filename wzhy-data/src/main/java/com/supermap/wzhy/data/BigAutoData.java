package com.supermap.wzhy.data;

/**
 * Created by Augustine on 2015/2/3.
 */
public class BigAutoData {
    private Object data;
    private String msg;

    public BigAutoData(){
    }

    public BigAutoData(Object obj){
        this.data = obj;
    }

    public  Object getData() {
        return data;
    }
    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
