package com.supermap.wzhy.data.cons;

/**
 * REPORT_TYPE：报告期类型（年月季、经济普查、人口普查）
 *
 * @author Linhao on 15-02-09
 */
public class CReportType {

    /**报告期类型：经济普查*/
    public static final int CENSUS_TYPE = 1;

    /**报告期类型：人口普查*/
    public static final int POPULATION_TYPE = 2;

    /**报告期类型：农业普查*/
    public static final int AGRICULTURE_TYPE = 3;

    /**报告期类型：年鉴*/
    public static final int YEARBOOK_TYPE = 4;

    /**报告期类型：年报*/
    public static final int YEAR_TYPE = 11;

    /**报告期类型：季报*/
    public static final int SESSION_TYPE = 12;

    /**报告期类型：月报*/
    public static final int MONTH_TYPE = 13;

    /**报告期类型：半年报*/
    public static final int HALFYEAR_TYPE = 14;

    /**
     * 报告期类型中文名
     *
     * @param reportType
     * 			报告期类型（年月季、经济普查、人口普查）
     * @return 报告期类型中文名
     */
    public static String getReportTypeName(int reportType) {
        String reportTypeName = "";
        switch (reportType) {
            case CENSUS_TYPE:
                reportTypeName = "经济普查";
                break;
            case POPULATION_TYPE:
                reportTypeName = "人口普查";
                break;
            case YEARBOOK_TYPE:
                reportTypeName = "年鉴";
                break;
            case AGRICULTURE_TYPE:
                reportTypeName = "农业普查";
                break;
            case YEAR_TYPE:
                reportTypeName = "年报";
                break;
            case SESSION_TYPE:
                reportTypeName = "季报";
                break;
            case MONTH_TYPE:
                reportTypeName = "月报";
                break;
            case HALFYEAR_TYPE:
                reportTypeName = "半年报";
                break;
        }
        return reportTypeName;
    }
}
