package com.supermap.wzhy.data.cons;

/**
 * 系统模块定义 
 * <p>
 * 说明：<br/>
 * 	模块的定义包括：基层 、综合、 专题、 地图汇 、可视化、  全库检索等
 * </p>
 * @author Created by W.Qiong on 14-4-8.
 */
public class SysModule {

    /**系统模块的：基层*/
    public static String micro_module ="micro";

    /**系统模块的：综合*/
    public static String macro_module ="macro";

    /**系统模块的：专题*/
    public static String special_module ="special";

    /**系统模块的：地图汇*/
    public static String dituhui_module ="dituhui";

    /**系统模块的：可视化*/
    public static String visual_module ="visual";

    /**系统模块的：全库检索*/
    public static String search_module ="search";
}
