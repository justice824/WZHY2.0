package com.supermap.wzhy.data.cons;

/**
 * 用户专业常量定义
 *
 * @author Created by Linhao on 2015/3/2.
 *
 */
public class CMajorDataType {

    /**用户专业常量:基层订报专业*/
    public static final int MAJOR_PRIMARY_ORDER = 1;

    /**用户专业常量:专业-行业*/
    public static final int MAJOR_INDUSTRY = 3;
}