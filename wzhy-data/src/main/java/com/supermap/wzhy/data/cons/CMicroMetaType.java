package com.supermap.wzhy.data.cons;

/**
 * MICMETA_TYPE：元数据内容类型（1调查对象、2统计范围、3报表模板、4目录、90系统显示模板）
 *
 * @author Linhao on 15-02-09
 */
public class CMicroMetaType {

    /**元数据内容类型:调查对象*/
    public static final int OBJECT_TYPE = 1;

    /**元数据内容类型:统计范围*/
    public static final int RANGE_TYPE = 2;

    /**元数据内容类型:报表模板 被当成系统系统模板来用了*/
    public static final int TABLE_TEMPLATE_TYPE = 3;

    /**元数据内容类型:目录*/
    public static final int CATALOG_TYPE = 4;

    /**元数据内容类型:系统显示模板*/
    public static final int SYS_TEMPLATE_TYPE = 90;

    /**元数据内容类型:基层定报模板 */
    public static final int REGULARTABLE_TEM_TYPE = 10;

    /**元数据内容类型:基层定报类型 */
    public static final int REGULARTABLE_TAB_TYPE = 11;
}
