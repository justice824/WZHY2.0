package com.supermap.wzhy.data.cons;

/**
 * XTABLE_TYPE：数据表类型（实体表、视图、API获取等等）
 *
 * @author Linhao on 15-02-09
 */
public class CMicroTableType {

    /**数据表类型:实体表*/
    public static final int ACTUAL_TABLE_TYPE = 1;

    /**数据表类型:视图*/
    public static final int VIEW_TYPE = 2;

    /**数据表类型:API获取*/
    public static final int API_TYPE = 3;

    /**数据表类型:??*/
    public static final int ACTUAL_API_MIXED_TYPE = 4;
}