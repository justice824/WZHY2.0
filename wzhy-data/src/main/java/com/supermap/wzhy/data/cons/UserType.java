package com.supermap.wzhy.data.cons;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Linhao on 2014/10/24.
 *
 * 用户类型常量操作
 *
 */
public class UserType {

    /** 匿名用户 */
    public static final int TYPE_ANONYOUS = 1;

    /** 普通用户 */
    public static final int TYPE_NORMAL = 2;

    /** 操作员用户 */
    public static final int TYPE_OPERATOR = 3;

    /** 管理用户 */
    public static final int TYPE_ADMIN = 4;

    /** 超级管理用户 */
    public static final int TYPE_SUPERADMIN = 5;

    /**
     * 获取类型名（中文说明）
     *
     * @param type
     *            类型
     * @return 类型名（中文说明）
     */
    public static String getTypeName(int type) {
        String re = null;
        switch (type) {
            case TYPE_ANONYOUS:
                re = "匿名用户";
                break;
            case TYPE_NORMAL:
                re = "普通用户";
                break;
            case TYPE_OPERATOR:
                re = "操作员";
                break;
            case TYPE_ADMIN:
                re = "系统管理员";
                break;
            case TYPE_SUPERADMIN:
                re = "超级管理员";
                break;
            default:
                re = "";
        }
        return re;
    }

    /**
     * 取得所有类型
     *
     * @return 所有类型（{"userTypeValue":"","userTypeName":""}）
     */
    public static List getAllType() {

        List list = new ArrayList();

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("userTypeValue", TYPE_NORMAL);
        map.put("userTypeName", getTypeName(TYPE_NORMAL));
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("userTypeValue", TYPE_ANONYOUS);
        map.put("userTypeName", getTypeName(TYPE_ANONYOUS));
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("userTypeValue", TYPE_OPERATOR);
        map.put("userTypeName", getTypeName(TYPE_OPERATOR));
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("userTypeValue", TYPE_ADMIN);
        map.put("userTypeName", getTypeName(TYPE_ADMIN));
        list.add(map);

        map = new HashMap<String, Object>();
        map.put("userTypeValue", TYPE_SUPERADMIN);
        map.put("userTypeName", getTypeName(TYPE_SUPERADMIN));
        list.add(map);

        return list;
    }
}
