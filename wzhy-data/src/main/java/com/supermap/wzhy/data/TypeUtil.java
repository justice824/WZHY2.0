package com.supermap.wzhy.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * 类型转换工具
 * Created by RRP on 2015/5/6.
 */
public class TypeUtil {

    /**
     * 数值Map转List
     *
     * @param mapdata Map对象
     * @return List对象
     */
    public static List<Object> mapToList(Map mapdata) {
        List<Object> newData = new ArrayList<>();
        Iterator it = mapdata.keySet().iterator();
        while (it.hasNext()) {
            String key = it.next().toString();
            newData.add(mapdata.get(key));//数值
        }
        return newData;
    }

}
