package com.supermap.wzhy.data;

import com.supermap.wzhy.entity.TUsermajor;
import com.supermap.wzhy.entity.TUsers;

import java.util.ArrayList;
import java.util.List;

/**
 * 存储用户信息
 * <p>
 * 说明：<br/>
 * 存储用户详细信息 包括：基本信息、角色 、专业、行政区划等信息
 * </p>
 *
 * @author Created by W.Qiong on 14-9-15.
 */
public class UserInfo {

    /** 用户id */
    private int userid;

    /** 用户名（登录名） */
    private String userName;

    /** 用户名称（如姓名等） */
    private String userCaption;

    /** 用户行政区划 */
    private String userRegion;

    /** 用户所在行政区划的中文简称 */
    private String userRegionCnName;

    /** 用户所在部门 */
    private String userPartment;

    /** 用户密码 */
    private String password;

    /** 备注信息 */
    private String memo;

    /** 用户状态 */
    private int status;

    /** 用户电子邮件 */
    private String email;

    /** 用户电话号码 */
    private String phone;

    /** 用户的角色，用于控制API权限 */
    private int sys_role;

    /** 用户角色的名称 */
    private String sys_rolename;

    /** 用户的所属专业列表 */
    private List<TUsermajor> majors = new ArrayList<>();

    /** 用户所属区划所在的纬度 */
    private double smx;

    /** 用户所属区划所在的经度 */
    private double smy;

    /** 无参构造函数 */
    public UserInfo() {
    }

    /**
     * 构造函数，初始化参数
     *
     * @param user
     *            用户对象
     * @param majors
     *            用户列表
     */
    public UserInfo(TUsers user, List<TUsermajor> majors) {
        if (user != null) {
            this.userid = user.getUserid();
            this.userName = user.getUserName();
            this.userCaption = user.getUserCaption();
            this.password = user.getPassword();

            String userRegion = user.getUserRegion();
            if (userRegion != null && userRegion.contains("@")) {
                this.userRegion = user.getUserRegion().split("@")[1]; // z只取code
                this.userRegionCnName = user.getUserRegion().split("@")[0]; // z只取name
            } else {
                this.userRegion = "";
                this.userRegionCnName = "";
            }

            this.userPartment = user.getUserPartment();
            this.memo = user.getMemo();
            this.status = user.getStatus();
            this.phone = user.getPhone();
            this.email = user.getEmail();
//            this.sys_role = user.getSys_role();
        }// end if(user != null)

        this.majors = majors;
    }

    /**
     * 取得用户角色的名称，用于控制API权限
     *
     * @return 用户角色的名称，用于控制API权限
     */
    public String getSys_rolename() {
        return sys_rolename;
    }

    /**
     * 设置用户角色的名称，用于控制API权限
     *
     * @param sys_rolename
     *            用户角色的名称，用于控制API权限
     */
    public void setSys_rolename(String sys_rolename) {
        this.sys_rolename = sys_rolename;
    }

    /**
     * 取得用户id
     *
     * @return 用户id
     */
    public int getUserid() {
        return userid;
    }

    /**
     * 设置用户id
     *
     * @param userid
     *            用户id
     */
    public void setUserid(int userid) {
        this.userid = userid;
    }

    /**
     * 取得用户名（登录名）
     *
     * @return 用户名（登录名）
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 设置用户名（登录名）
     *
     * @param userName
     *            用户名（登录名）
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 取得用户名称（如姓名等）
     *
     * @return 用户名称（如姓名等）
     */
    public String getUserCaption() {
        return userCaption;
    }

    /**
     * 设置用户名称（如姓名等）
     *
     * @param userCaption
     *            用户名称（如姓名等）
     */
    public void setUserCaption(String userCaption) {
        this.userCaption = userCaption;
    }

    /**
     * 取得用户行政区划
     *
     * @return 用户行政区划
     */
    public String getUserRegion() {
        return userRegion;
    }

    /**
     * 设置用户行政区划
     *
     * @param userRegion
     *            用户行政区划
     */
    public void setUserRegion(String userRegion) {
        this.userRegion = userRegion;
    }

    /**
     * 取得用户所在行政区划的中文简称
     *
     * @return 用户所在行政区划的中文简称
     */
    public String getUserRegionCnName() {
        return userRegionCnName;
    }

    /**
     * 设置用户所在行政区划的中文简称
     *
     * @param userRegionCnName
     *            用户所在行政区划的中文简称
     */
    public void setUserRegionCnName(String userRegionCnName) {
        this.userRegionCnName = userRegionCnName;
    }

    /**
     * 取得用户所在的部门
     *
     * @return 用户所在的部门
     */
    public String getUserPartment() {
        return userPartment;
    }

    /**
     * 设置用户所在的部门
     *
     * @param userPartment
     *            用户所在的部门
     */
    public void setUserPartment(String userPartment) {
        this.userPartment = userPartment;
    }

    /**
     * 取得用户密码
     *
     * @return 用户密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置用户密码
     *
     * @param password
     *            用户密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 取得 备注信息
     *
     * @return 备注信息
     */
    public String getMemo() {
        return memo;
    }

    /**
     * 设置备注信息
     *
     * @param memo
     *            备注信息
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * 取得用户状态
     *
     * @return 用户状态
     */
    public int getStatus() {
        return status;
    }

    /**
     * 设置用户状态
     *
     * @param status
     *            用户状态
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * 取得用户电子邮件
     *
     * @return 用户电子邮件
     */
    public String getEmail() {
        return email;
    }

    /**
     * 设置用户电子邮件
     *
     * @param email
     *            用户电子邮件
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 取得用户的电话号码
     *
     * @return 用户的电话号码
     */
    public String getPhone() {
        return phone;
    }

    /**
     * 设置用户的电话号码
     *
     * @param phone
     *            用户的电话号码
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * 取得用户的角色，用于控制API权限
     *
     * @return 用户的角色，用于控制API权限
     */
    public int getSys_role() {
        return sys_role;
    }

    /**
     * 设置用户的角色，用于控制API权限
     *
     * @param sys_role
     *            用户的角色，用于控制API权限
     */
    public void setSys_role(int sys_role) {
        this.sys_role = sys_role;
    }

    /**
     * 取得用户的所属专业列表
     *
     * @return 用户的所属专业列表
     */
    public List<TUsermajor> getMajors() {
        return majors;
    }

    /**
     * 设置用户的所属专业列表
     *
     * @param majors
     *            用户的所属专业列表
     */
    public void setMajors(List<TUsermajor> majors) {
        this.majors = majors;
    }

    /**
     * 取得用户所属区划所在的纬度
     *
     * @return 用户所属区划所在的纬度
     */
    public double getSmx() {
        return smx;
    }

    /**
     * 设置用户所属区划所在的纬度
     *
     * @param smx
     *            用户所属区划所在的纬度
     */
    public void setSmx(double smx) {
        this.smx = smx;
    }

    /**
     * 取得用户所属区划所在的经度
     *
     * @return 用户所属区划所在的经度
     */
    public double getSmy() {
        return smy;
    }

    /**
     * 设置用户所属区划所在的经度
     *
     * @param smy
     *            用户所属区划所在的经度
     */
    public void setSmy(double smy) {
        this.smy = smy;
    }

}
