package com.supermap.wzhy.data;

/**
 * 错误的状态对象
 *
 * Created by W.Hao on 14-2-24.
 * 异常状态
 */
public class ErrorStatus extends OpStatus{

    /**
     * 状态码
     */
    String code;

    /**
     * 异常描述
     */
    String description;

    /**
     * 构造函数
     *
     * @param msg
     * 			错误消息
     */
    public ErrorStatus(String msg) {
        super(false, msg, "");
    }

    /**
     * 取得状态码
     *
     * @return 状态码
     */
    public String getCode() {
        return code;
    }

    /**
     * 设置状态码
     *
     * @param code
     * 			状态码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 取得描述信息
     *
     * @return 描述信息
     */
    public String getDescription() {
        return description;
    }

    /**
     * 设置描述信息
     *
     * @param description
     * 			描述信息
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
