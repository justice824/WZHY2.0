package com.supermap.wzhy.data.cons;

/**
 * 所有树的共通常量
 *
 * @author Linhao on 15-02-20
 *
 */
public class CTree {

    /**树根：数据库中统一使用 0 代表*/
    public static final int ROOT_VALUE = 0;

    /**树根：系统权限树的根值：sys*/
    public static final String ROOT_SYS = "sys";

    /**树根：基层权限树的根值：micro*/
    public static final String ROOT_MICRO = "micro";

    /**树根：综合权限树的根值：macro*/
    public static final String ROOT_MACRO = "macro";

    /**树根：后台权限树的根值：sdms*/
    public static final String ROOT_SDMS = "sdms";

    /**树根：综合权限树的节点值：reportType*/
    public static final String NODE_MACRO_REPORTTYPE = "reportType";

    /**树根：综合权限树的节点值：level*/
    public static final String NODE_MACRO_REPORTTYPE_LEVEL = "level";
}	
