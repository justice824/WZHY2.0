package com.supermap.wzhy.data;

import java.io.File;

/**
 * 常量信息
 *
 * @author Created by duanxiaofei on 2014/11/2.
 */
public class MacroFinalInfo {
    /**
     * SGIS4.0统一上传文件本地路径（%user.home%/supermap/fileupload）
     */
    public static String userHome = System.getProperty("user.home")
            + File.separator +"supermap"+File.separator+"fileupload"+File.separator;
}