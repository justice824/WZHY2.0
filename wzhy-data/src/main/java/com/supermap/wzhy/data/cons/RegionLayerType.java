package com.supermap.wzhy.data.cons;

/**
 * 行政区划图层类型: 国家、省级、市级、县级、乡镇级、村级、小区（暂时不用）
 *
 * @author Created by RRP on 2014/8/15.
 */
public class RegionLayerType {

    /**行政区划图层类型: 国家*/
    public static final String ST_R_WD = "ST_R_WD";

    /**行政区划图层类型: 省级*/
    public static final String ST_R_SN = "ST_R_SN";

    /**行政区划图层类型: 市级*/
    public static final String ST_R_SH = "ST_R_SH";

    /**行政区划图层类型: 县级*/
    public static final String ST_R_XN = "ST_R_XN";

    /**行政区划图层类型: 乡镇级*/
    public static final String ST_R_XA = "ST_R_XA";

    /**行政区划图层类型: 村级*/
    public static final String ST_R_CN = "ST_R_CN";

    /**行政区划图层类型: 小区*/
    public static final String ST_R_XQ = "ST_R_XQ";
}
