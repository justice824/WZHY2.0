package com.supermap.wzhy.data.cons;

/**
 * 权限数据类型
 *
 * @author Created by W.Qiong on 14-10-20.
 */
public class CPowerDataType {

    /**权限数据类型：基层调查对象*/
    public final static int MICRO_SUERVER = 1;

    /**权限数据类型：基层指标*/
    public final static int MICRO_IDE = 2;

    /**权限数据类型：综合数据*/
    public final static int MACRO = 3;

    /**权限数据类型：基层专业*/
    public final static int MICRO_MAJOR = 4;
}
