package com.supermap.wzhy.data;

import com.supermap.wzhy.common.annotation.Role;
import com.supermap.wzhy.common.annotation.RoleContext;

import javax.servlet.*;
import java.io.IOException;


/**
 * 登录过滤器
 *
 * @author Created by W.Qiong on 14-3-3.
 */
public class LoginFilter implements Filter {

    /**
     * 过滤器初始化
     */
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    /**
     * 过滤器销毁
     */
    public void destroy() {
    }

    /**
     * 过滤方法
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        RoleContext.INSTANCE.setCurrentRole(Role.ADMIN);

        System.out.println("进入到loginFilter");
    }
}