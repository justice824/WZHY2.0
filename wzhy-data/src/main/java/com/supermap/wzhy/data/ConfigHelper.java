package com.supermap.wzhy.data;

import org.omg.CORBA.Environment;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 *
 * 读取data配置文件
 * <p>
 * &nbsp;&nbsp;说明：<br/>
 * &nbsp;&nbsp;从config.properties文件中读取配置信息
 * </p>
 *
 * @author Created by W.Qiong on 14-8-22.
 */
public class ConfigHelper {

    /**data配置文件config.properties*/
    public static final String configName = "config.properties";

    /**存储配置文件config.properties中的所有key,value值*/
    public static Map<String, String> config;

    /** 静态解析data配置文件config.properties */
    static {
        config = new HashMap<String, String>();
        InputStream is = ConfigHelper.getResourceAsStream(configName);
        Properties pro = new Properties();

        try {
            pro.load(is);
            Set<Map.Entry<Object, Object>> set = pro.entrySet();
            for (Map.Entry<Object, Object> entry : set) {
                config.put(
                        entry.getKey().toString(),
                        new String((entry.getValue().toString())
                                .getBytes("ISO-8859-1"), "GBK"));
            }
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 取得配置文件的输入流形式
     *
     * @param strResource
     * 			配置文件名
     * @return 输入流
     */
    public static InputStream getResourceAsStream(String strResource) {
        InputStream is = null;

        is = Thread.currentThread().getContextClassLoader()
                .getResourceAsStream(strResource);
        if (is == null) {
            is = Environment.class.getResourceAsStream(strResource);
        }
        if (is == null) {
            is = Environment.class.getClassLoader().getResourceAsStream(
                    strResource);
        }
        return is;
    }

    /**
     * 取得config.properties中name对应的value值
     *
     * @param name
     * 			map的key（config.properties中的name）
     * @return value值
     */
    public static String get(String name) {
        return config.get(name);
    }

}
