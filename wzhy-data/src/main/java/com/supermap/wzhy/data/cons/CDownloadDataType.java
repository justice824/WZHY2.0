package com.supermap.wzhy.data.cons;

/**
 * 数据下载权限定义，与标准库同步对应。
 * 目前权限没有名称标示，以数据库记录ID作为唯一标识。
 * 如果数据库记录ID有变化，则做相应改变。
 */
public class CDownloadDataType {

    /** 无下载权限 */

    public static final int ZERO = 12;

    /** 下载全部 */

    public static final int ALL = 15;

}
