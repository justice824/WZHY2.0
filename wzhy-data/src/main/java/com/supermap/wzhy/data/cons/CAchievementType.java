package com.supermap.wzhy.data.cons;

/**
 * 成果信息表 行为类型
 *
 * @author Created by W.Qiong on 14-4-8.
 */
public class CAchievementType {

    /** 查询条件 */
    public static int condition = 1;

    /** 行政区划 */
    public static int region = 2;

    /** 基层果 */
    public static int micro_achieve = 3;

    /**综合成果*/
    public static int macro_achieve = 4;
}
