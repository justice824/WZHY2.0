package com.supermap.wzhy.data;

import com.supermap.wzhy.data.cons.CMacroReportType;

import java.util.ArrayList;
import java.util.List;

/**
 * 行政区划编码的统一处理方法
 *
 * @author Created by zhangjunfeng on 14-3-19.
 * @author Updated by ruanrp on 14-12-1
 * @author Modify by Linhao on 14-2-4
 */
public class PeriodSupport {

    //判断是否年报
    public static boolean isYearType(int reportType){
        if(reportType == CMacroReportType.YEAR_TYPE ||
                reportType == CMacroReportType.ANNALES_TYPE||
                reportType == CMacroReportType.POPULATION_TYPE ||
                reportType == CMacroReportType.ECONOMICCENSUS_TYPE ||
                reportType == CMacroReportType.AGRICULTURE_TYPE){
            return true;
        }
        //当年报处理
        if(reportType == 0){
            return  true ;
        }
        return false;
    }


    public static  String getYearRank(int fromYear, int toYear) {
        String result = "";
        for (int i = fromYear; i <= toYear; i++) {
            if (i == toYear) {
                result += i;
            } else
                result += i + ",";
        }
        return result;
    }


    /**
     *获取报告期前缀
     * @param reportType
     * @param year
     * @param month
     * @return
     */
    public static String getPrefix(int reportType, int year, int month){
        String prefix = "";
        switch (reportType) {
            case 11:
                prefix = year + "年";
                break;
            case 12:
                prefix = year + "年第" + (month>12?(month-12):month) + "季度";
                break;
            case 13:
                prefix = year + "年" + month + "月";
                break;
            case 14:
                int num = month>=17?(month-17):month;
                prefix = year + "年" + (num==0?"上":"下") + "半年";
                break;
            default: //经济普查人口普查
                prefix = year + "年";
                break;
        }
        return  prefix;
    }

    /**
     * 获取报告期类型的名称
     * @param reportType
     * @return
     */
    public static  String getReportTypeName(int reportType){
        String name = "";
        switch (reportType){
            case 11:
                name ="年报" ;
                break;
            case 12:
                name ="季报" ;
                break;
            case 13:
                name ="月报" ;
                break;
            case 14:
                name ="半年报" ;
                break;
            case 1:
                name ="年度经普" ;
                break;
            case 2:
                name ="年度人普" ;
                break;
            case 3:
                name ="年度农普" ;
                break;
            case 4:
                name ="年鉴" ;
                break;
            default:
                name ="年度" ;//经济普查人口普查
                break;
        }
        return name ;
    }
}
