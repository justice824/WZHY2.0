package com.supermap.wzhy.data;

/**
 * 消息打印工具类<br/>
 *
 * 服务端所有消息打印在此处处理
 *
 * @author Linhao on 2015-01-28
 *
 */
public class MessagePrintUtil {

    /**是否打印sql,默认true*/
    public static boolean isPrintSql = true;

    /**是否打印异常,默认true*/
    public static boolean isPrintException = true;

    /**是否打印一般消息,默认true*/
    public static boolean isPrintMsg = true;

    /**是否打印错误消息,默认true*/
    public static boolean isPrintErr = true;

    /**
     * 打印sql
     *
     * @param sql
     */
    public static void printSql(String sql){
        if(isPrintSql)
            System.out.println(sql);
    }

    /**
     * 打印异常
     *
     * @param exception
     * 			异常消息
     */
    public static void printException(String exception){
        if(isPrintException)
            System.out.println(exception);
    }

    /**
     * 打印消息
     *
     * @param msg
     * 			消息
     */
    public static void printMsg(String msg){
        if(isPrintMsg)
            System.out.println(msg);
    }

    /**
     * 打印错误消息
     *
     * @param err
     * 			错误消息
     */
    public static void printErr(String err){
        if(isPrintErr)
            System.err.println(err);
    }

}
