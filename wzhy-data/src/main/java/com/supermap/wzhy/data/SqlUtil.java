package com.supermap.wzhy.data;

import java.util.HashMap;
import java.util.Set;

/**
 * Created by RRP on 2015/5/7.
 */
public class SqlUtil {
    /**
     * 数组值codes转IN枚举值
     * @param codes 数组值
     */
    public static String arrayToInStr(String[] codes) {
        String sql = "";
        if(codes==null) return "";
        for(String c : codes) {
            sql += "'"+c+"',";
        }
        if(!sql.equals("")){
            sql = sql.substring(0,sql.length()-1);
        }
        return sql;
    }

    /**
     * 获取主键字段关联条件
     * @param Pkey_index 主键字段组<字段名，值>
     * @param table1 关联表1
     * @param table2 关联表2
     */
    public static String getKeycondition(HashMap<String, Integer> Pkey_index, String table1, String table2){
        String condition = "";//构建主键条件
        Set<String> keyFields = Pkey_index.keySet();
        for(String keyField: keyFields){
            condition += table1+"."+keyField+" = "+table2+"."+keyField+" and ";
        }
        if(!condition.equals("")){
            condition = condition.substring(0,condition.length()-5);
        }
        return condition;
    }
}
