package com.supermap.wzhy.common.service;

import java.util.List;

/**
 *
 * 基本的增删改查service（抽象类）
 *
 * @author W.Hao
 * @param <T>
 */
public abstract class CRUDService <T> extends BaseService {

    /**
     * 创建数据库记录的方法
     * @param entity
     * 			数据库实体对象
     * @return 创建后的数据库实体对象
     */
    public abstract T create(T entity);

    /**
     * 创建数据库记录的方法
     * @param entity
     * 			数据库实体对象
     * @return 是否删除成功
     */
    public abstract boolean delete(T entity);

    /**
     * 更新后的数据库记录的方法
     *
     * @param entity
     * 			数据库实体对象
     * @return 更新后的数据库实体对象
     */
    public abstract T update(T entity);

    /**
     * 查询数据
     * @param entity
     * 			数据库实体对象
     * @return 数据列表
     */
    public abstract List<T> query(T entity);

    /**
     * 读取一条数据
     *
     * @param entity
     * 			数据库实体对象
     * @return 读取的数据对象
     */
    public abstract T one(T entity);
}
