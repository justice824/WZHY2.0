package com.supermap.wzhy.common.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.JpaRepositoryFactory;
import org.springframework.data.repository.core.RepositoryMetadata;

import javax.persistence.EntityManager;
import java.io.Serializable;

/**
 * DAO工厂基础类
 *
 * @author Created by W.Qiong on 14-3-6.
 */
public class BaseDaoFactory<S, ID extends Serializable> extends
        JpaRepositoryFactory {

    /**
     * 构造方法
     *
     * @param entityManager
     *            实体管理对象
     */
    public BaseDaoFactory(EntityManager entityManager) {
        super(entityManager);
    }

    /**
     * 取得Dao接口类实现类对象
     *
     * @param entityManager
     *            实体管理对象
     */
    @Override
    protected <T, ID extends Serializable> JpaRepository<?, ?> getTargetRepository(
            RepositoryMetadata metadata, EntityManager entityManager) {
        return new BaseDaoImpl<T, ID>((Class) metadata.getDomainType(),
                entityManager);
    }

    @Override
    protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
        return BaseDao.class;
    }
}