package com.supermap.wzhy.common.service;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

/**
 * 基础服务 提供一些常用方法等
 *
 * @author W.Hao
 */
public class BaseService {

    /**
     * 二元检查
     *
     * @param content
     * @param ifEmpty
     * @return
     */
    protected String check(String content, String ifEmpty) {
        return content != null ? content : ifEmpty;
    }

    /**
     * 三元判断
     *
     * @param obj
     * @param ifExist
     * @param ifNull
     * @return
     */
    protected String check(Object obj, String ifExist, String ifNull) {
        return obj != null ? ifExist : ifNull;
    }

    /**
     * 从一个对象填充属性到另一个对象 可设置排除属性 兼容非同类型
     *
     * @param from
     * @param to
     * @param args
     * @return
     */
    public Object convertBeanTOBean(Object from, Object to, Object... args) {
        if(null == from || to == from){
            return to ;
        }
        try {
            //同一个类型对象
            if(from.getClass().equals(to.getClass())){
                BeanInfo beanInfo = Introspector.getBeanInfo(to.getClass());
                PropertyDescriptor[] ps = beanInfo.getPropertyDescriptors();
                for (PropertyDescriptor p : ps) {
                    Method getMethod = p.getReadMethod();
                    Method setMethod = p.getWriteMethod();
                    Class<?> retureType = getMethod.getReturnType();
                    String type = retureType.getSimpleName() ;
                    if(type.equalsIgnoreCase("Set")
                            ||type.equalsIgnoreCase("Class")){
                        continue;
                    }
                    String field = getMethod.getName().substring("get".length()).toLowerCase();
                    boolean flag = false;
                    if(null!=args){
                        for (int index = 0, size = args.length; index < size; index++) {
                            if (args[index].equals(field)) {
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (flag||getMethod == null || setMethod == null) {
                        continue;
                    }
                    try {
                        Object result = getMethod.invoke(from);
                        if (result != null) {
                            setMethod.invoke(to, result);
                        }
                    } catch (Exception e) {
                        continue;
                    }
                }
            }else{
                //非同一类型 根据字段名称进行填充
                BeanInfo toBeanInfo = Introspector.getBeanInfo(to.getClass());
                PropertyDescriptor[] tops = toBeanInfo.getPropertyDescriptors();

                BeanInfo fromBeanInfo = Introspector.getBeanInfo(from.getClass());
                PropertyDescriptor[] fromps = fromBeanInfo.getPropertyDescriptors();

                for (PropertyDescriptor p : tops) {
                    Method getMethod = p.getReadMethod();
                    Method setMethod = p.getWriteMethod() ;
                    String field = getMethod.getName().substring("get".length()).toLowerCase();
                    boolean flag = false;
                    if(null!=args){
                        for (int index = 0, size = args.length; index < size; index++) {
                            if (args[index].equals(field)) {
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (flag||null == getMethod) {
                        continue;
                    }
                    Object value = null;
                    for(PropertyDescriptor p1:fromps){
                        Method getMethod1 = p1.getReadMethod();
                        if(getMethod1.getName().equals(getMethod.getName())){
                            value = getMethod1.invoke(from);
                            break;
                        }
                    }
                    if(null == value){
                        continue;
                    }
                    try {
                        setMethod.invoke(to, value);
                    } catch (Exception e) {
                        continue;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return to;
    }

}
