package com.supermap.wzhy.module.fr.api;

import com.supermap.wzhy.data.OpStatus;
import com.supermap.wzhy.entity.TUsertask;
import com.supermap.wzhy.module.fr.service.FrGarbledConversionService;
import com.supermap.wzhy.module.fr.service.FrWjgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 法人相关API
 * Created by sun'f on 16-12-27.
 */
@Controller
@RequestMapping(value = {"/fr","/service/fr"})
public class FrController {

    @Autowired
    FrWjgService frWjgService ;
    @Autowired
    FrGarbledConversionService frGarbledConversionService;



    /**
     * 获取informix数据库下所有表名
     * @return
     */
    @RequestMapping(value = "/wjg/tabname", method = RequestMethod.GET)
    @ResponseBody
    public List getAllTable(){
        return  frWjgService.getAllTable();
    }


    /**
     * 工商基本信息表名查询
     * @return
     */
    @RequestMapping(value = "/gs/table/name", method = RequestMethod.GET)
    @ResponseBody
    public List getGSTable(){
        return  frWjgService.getGSTable();
    }

    /**
     * 工商基本信息表指标查询
     * @return
     */
    @RequestMapping(value = "/gs/iden/name", method = RequestMethod.GET)
    @ResponseBody
    public List getGSIden(String mitmid){
        return  frWjgService.getGSIden(mitmid);
    }

    /**
     * 获取informix数据库下所有表名
     * @return
     */
    @RequestMapping(value = "/nb/tabname", method = RequestMethod.GET)
    @ResponseBody
    public List getNBTable(){
        return  frWjgService.getNBTable();
    }

    /**
     * 获取文件柜导出任务
     * @param request
     * @return
     */
    @RequestMapping(value = "/wjg/task", method = RequestMethod.GET)
    @ResponseBody
    public List<TUsertask> setWjgJobTask(HttpServletRequest request){
        return frWjgService.getRunWjgTask()  ;
    }


    /**
     * 获取文件柜导出任务
     * @param request
     * @return
     */
    @RequestMapping(value = "/wjg/export", method = RequestMethod.GET)
    @ResponseBody
    public OpStatus runWjgExportTask(HttpServletRequest request,String starttime ,String endtime,Integer maxSize){
        Date startTime =null ,endTime=null ;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            if(starttime!=""){
                startTime = formatter.parse(starttime) ;
            }
            if(endtime!=""){
                endTime = formatter.parse(endtime) ;
            }
            //没设置起始时间就是全部数据
            if(startTime==null){
                startTime = formatter.parse("2000-1-1 00:00:00");
            }
            if(endTime==null){
                endTime = new Date();
            }

        }   catch (Exception e){
            System.out.println("日期格式转换异常！");
            e.printStackTrace();
        }

        return frWjgService.startRunnable(request, startTime, endTime,maxSize==null?0:maxSize);
    }


    /**
     * 获取年报导出任务
     * @param request
     * @return
     */
    @RequestMapping(value = "/nb/export", method = RequestMethod.GET)
    @ResponseBody
    public OpStatus runNBExportTask(HttpServletRequest request,String starttime ,String endtime,Integer maxSize,String year){
        Date startTime =null ,endTime=null ;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            if(starttime != "" && startTime != null){
                startTime = formatter.parse(starttime) ;
            }
            if(endtime!=""){
                endTime = formatter.parse(endtime) ;
            }
            //没设置起始时间就是全部数据
            if(startTime == null){
                startTime = formatter.parse("2000-1-1 00:00:00");
            }
            if(endTime == null){
                endTime = new Date();
            }
            if(year == null){
                year = "";
            }
        }   catch (Exception e){
            System.out.println("日期格式转换异常！");
            e.printStackTrace();
        }

        return frWjgService.startNbRunnable(request, startTime, endTime,maxSize==null?0:maxSize,year);
    }


    /**
     * 获取文件柜导出任务
     * @param request
     * @return
     */
    @RequestMapping(value = "/wfsx/export", method = RequestMethod.GET)
    @ResponseBody
    public OpStatus runWFSXExportTask(HttpServletRequest request,String starttime ,String endtime,Integer maxSize){
        Date startTime =null ,endTime=null ;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        try {
            if(starttime != "" && startTime != null){
                startTime = formatter.parse(starttime) ;
            }
            if(endtime!=""){
                endTime = formatter.parse(endtime) ;
            }
            //没设置起始时间就是全部数据
            if(startTime == null){
                startTime = formatter.parse("2000-1-1 00:00:00");
            }
            if(endTime == null){
                endTime = new Date();
            }

        }   catch (Exception e){
            System.out.println("日期格式转换异常！");
            e.printStackTrace();
        }

        return frWjgService.startWFSXRunnable(request, startTime, endTime,maxSize==null?0:maxSize);
    }


    /**
     * 停止文件柜导出任务
     * @return
     */
    @RequestMapping(value = "/wjg/stopExport", method = RequestMethod.GET)
    @ResponseBody
    public OpStatus stopWjgExportTask(){
        System.out.println("停止导出!");
        return frWjgService.stopWjgTask();
    }


    /**
     * 停止年报导出任务
     * @return
     */
    @RequestMapping(value = "/nb/stopExport", method = RequestMethod.GET)
    @ResponseBody
    public OpStatus stopNBExportTask(){
        System.out.println("停止年报导出!");
        return frWjgService.stopNbTask();
    }

    /**
     * 获取文件柜导出数量
     * @return
     */
    @RequestMapping(value = "/wjg/getCounts", method = RequestMethod.GET)
    @ResponseBody
    public OpStatus getWjgCount(){
        OpStatus op = new OpStatus(true,"",null);
        String string = frWjgService.getCounts("wjg");
        //System.out.println("当前已导出 "+string+" 条!");
        op.setMsg(string);
        return op;
    }


    /**
     * 获取文件柜年报导出数量
     * @return
     */
    @RequestMapping(value = "/nb/getCounts", method = RequestMethod.GET)
    @ResponseBody
    public OpStatus getNBCount(){
        OpStatus op = new OpStatus(true,"",null);
        String string = frWjgService.getCounts("nb");
        op.setMsg(string);
        return op;
    }

    /**
     * 设置文件柜定时任务参数
     * @param request
     * @param wjgAuto
     * @param wjgJobTime
     * @return
     */
    @RequestMapping(value = "/wjg/job/set", method = RequestMethod.POST)
    @ResponseBody
    public String setWjgJobParams(HttpServletRequest request,String wjgAuto,String wjgJobTime){
        return null ;
    }


    /**
     * 得到年报年度
     * @return
     */
    @RequestMapping(value = "/nb/ancheyear", method = RequestMethod.GET)
    @ResponseBody
    public List getNBYear(HttpServletRequest request){
        return frWjgService.getNBYear();
    }



    /**
     * 检测器
     * @return
     */
    @RequestMapping(value = "/wjg/conversion", method = RequestMethod.GET)
    @ResponseBody
    public List conversion(HttpServletRequest request){
        return frGarbledConversionService.conversion();
    }

}
