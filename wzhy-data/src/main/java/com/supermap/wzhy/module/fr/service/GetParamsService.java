package com.supermap.wzhy.module.fr.service;

/**
 * Created by sun'f on 2017-09-27.
 */
public class GetParamsService {
    public static String CONDITION_TABLE = "t_condition_dictionary";
    public static String TABLEMETA = "t_micro_tablemeta";
    public static String IDENMETA = "t_micro_idenmeta";
    public static String BASEINFO_MARK = "FR_JBXX";
    public static String IDNENAME = "iden_name";
    public static String IDNECODE = "iden_code";
    public static String MITMID = "mitmid";
}
