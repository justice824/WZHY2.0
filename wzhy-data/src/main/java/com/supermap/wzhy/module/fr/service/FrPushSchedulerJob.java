package com.supermap.wzhy.module.fr.service;

import java.util.Date;

/**
 * 法人推送定时任务
 * Created by W.Qiong on 16-12-23.
 */
public class FrPushSchedulerJob {

    public void run(){
        System.out.println("定时任务执行一次"+new Date().toString());
    }
}
