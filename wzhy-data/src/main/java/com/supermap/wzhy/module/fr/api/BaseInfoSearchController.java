package com.supermap.wzhy.module.fr.api;

import com.alibaba.fastjson.JSONObject;
import com.supermap.wzhy.module.fr.service.BaseInfoSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.List;
import java.util.Map;


/**
 *  ┏┓　　　┏┓
 *┏┛┻━━━┛┻┓
 *┃　　　　　　　┃ 　
 *┃　　　━　　　┃
 *┃　┳┛　┗┳　┃
 *┃　　　　　　　┃
 *┃　　　┻　　　┃
 *┃　　　　　　　┃
 *┗━┓　　　┏━┛
 *　　┃　　　┃
 *　　┃　　　┃
 *　　┃　　　┗━━━┓
 *　　┃　　　　　　　┣┓
 *　　┃　　　　　　　┏┛
 *　　┗┓┓┏━┳┓┏┛
 *　　　┃┫┫　┃┫┫
 *　 　┗┻┛　┗┻┛
 *　　　
 * 神兽保佑
 * 代码无BUG！
 *
 *  @Version 2.0
 *  基本信息查询相关API
 * Created by sun'f on 16-12-27.
 *
 */

@Controller
@RequestMapping(value = {"/search","/service/search"})
public class BaseInfoSearchController {

    @Autowired
    BaseInfoSearchService searchService ;

    /**
     * 工商基本信息表名查询
     * @return
     */
    @RequestMapping(value = "/gs/table/name", method = RequestMethod.GET)
    @ResponseBody
    public List getGSTable(){
        return  searchService.getGSTable();
    }

    /**
     * 工商基本信息表指标查询
     * @return
     */
    @RequestMapping(value = "/gs/iden/name", method = RequestMethod.GET)
    @ResponseBody
    public List getGSIden(String mitmid){
        return  searchService.getGSIden(mitmid);
    }

    /**
     * 工商基本信息表条件字典查询 2.0
     * @return
     */
    @RequestMapping(value = "/gs/condition", method = RequestMethod.GET)
    @ResponseBody
    public List getCondition(){
        return  searchService.getCondition();
    }


    /**
     * 工商数据查询 2.0
     * @return
     */
    @RequestMapping(value = "/gs/data", method = RequestMethod.GET)
    @ResponseBody
    public JSONObject getData(String table,String iden,String where,String value,String page,String rows){
        return  searchService.getData(table,iden,where,value,page,rows);
    }


    /**
     * 工商表名查询 2.0
     * @return
     */
    @RequestMapping(value = "/gs/title", method = RequestMethod.GET)
    @ResponseBody
    public List getData(String table){
        return  searchService.getTitle(table);
    }
}
