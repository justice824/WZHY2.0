package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.entity.TDataUserpower;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 用户数据权限表dao
 *
 * @author Created by W.Qiong on 14-10-20.
 */
public interface DataUserpowerDao extends JpaRepository<TDataUserpower, Integer> {

    /**
     * 通过指定用户id和数据类型查询用户数据权限列表
     *
     * @param userid
     *            用户id
     * @param dataType
     *            数据类型（基层或者综合）
     * @return 用户数据权限列表
     */
    @Query("select t from TDataUserpower t where t.TUsers.userid=?1 and t.dataType=?2 and t.status=1 order by t.dataId")
    List<TDataUserpower> findByDataType(int userid, int dataType);

    /**
     * 通过指定用户id、数据类型和数据(一个或者多个)id查询用户数据权限列表
     *
     * @param userid
     *            用户id
     * @param dataType
     *            数据类型（基层或者综合）
     * @param dataids
     *            数据(一个或者多个)id
     * @return 用户数据权限列表
     */
    @Query("select t from TDataUserpower t where t.TUsers.userid=?1 and t.dataType=?2 and t.dataId in ?3 and t.status=1 order by t.dataId")
    List<TDataUserpower> findByDataIds(int userid, int dataType, int[] dataids);

    /**
     * 通过指定数据类型和数据(一个或者多个)id查询用户数据权限列表
     *
     * @param dataType
     *            数据类型（基层或者综合）
     * @param dataids
     *            数据(一个或者多个)id
     * @return 用户数据权限列表
     */
    @Query("select t from TDataUserpower t where  t.dataType=?1 and t.dataId in ?2 and t.status=1 order by t.dataId")
    List<TDataUserpower> findByIdsAndType(int dataType, int[] dataids);


    /**
     * 查询数据
     *
     * @param sql
     *            sql语句
     * @param args
     *            参数（？号对应的值）
     * @return 查询结果列表
     */
    public List query(String sql, Object... args);

    /**
     * 根据语句执行包括更新和创建 删除等操作
     *
     * @param sql
     *
     *
     *            原生sql
     * @param args
     *            参数（？号对应的值）
     * @return 受影响的行数
     */
    public int executeBySql(String sql, Object... args);
}
