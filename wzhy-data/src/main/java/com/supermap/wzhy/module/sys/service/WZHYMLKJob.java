package com.supermap.wzhy.module.sys.service;

import com.supermap.wzhy.module.fr.service.FrPushDataToMlkService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

/**
 * Created by Sun'fei on 17-2-21.
 * 定时任务业务处理
 */
@Service
public class WZHYMLKJob implements Job {

    @Autowired
    FrPushDataToMlkService frPushDataToMlkService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        try{
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Map map = jobExecutionContext.getMergedJobDataMap();
        String username = "";
        String tableCode = "";
        String tableName = "";
        String startTime = "";
        String endTime = "";
        boolean switchs = false;

        if(!map.isEmpty()){
           username = map.get("username").toString();
           tableCode = map.get("tableCode").toString();
           tableName = map.get("tableName").toString();

           if(map.containsKey("startTime"))startTime = map.get("startTime").toString();

           if(map.containsKey("endTime"))endTime = map.get("endTime").toString();

           switchs = (Boolean)map.get("switchs");
        }

        frPushDataToMlkService = (FrPushDataToMlkService)map.get("service");

        if(switchs){
            if(startTime == ""){
                startTime = "2000-01-01";
            }
            frPushDataToMlkService.bulidCSV(username, tableCode, tableName, startTime, endTime);

        }else{
            Date date = new Date();
            //取当前时间
            String today = sdf.format(date);
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(date);
            //把日期往前减少一天.整数往后推,负数往前移动
            calendar.add(calendar.DATE,-1);
            date = calendar.getTime();
            //取当前时间的前一天
            String yesterday = sdf.format(date);

            System.out.println("法人推送到名录库 "+map.get("tableCode")+" 定时任务开始: endtime = "+today
                    +" starttime = "+yesterday);
            frPushDataToMlkService.bulidCSVByUpdate("定时任务", tableCode, tableName, yesterday, today);
        }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
