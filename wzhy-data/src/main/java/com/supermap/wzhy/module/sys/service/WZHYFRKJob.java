package com.supermap.wzhy.module.sys.service;

import com.supermap.wzhy.module.mlk.service.MlkPushDataToFrService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

/**
 * Created by Sun'fei on 17-2-21.
 * 定时任务业务处理
 */
@Service
public class WZHYFRKJob implements Job {

    @Autowired
    MlkPushDataToFrService mlkPushDataToFrService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Map map = jobExecutionContext.getMergedJobDataMap();
        String username = "";
        String tableCode = "";
        String tableName = "";
        if(!map.isEmpty()){
            username = map.get("username").toString();
            tableCode = map.get("tableName").toString();
            tableName = map.get("tableCode").toString();
        }

        mlkPushDataToFrService = (MlkPushDataToFrService)map.get("service");

        // 推送每天的增量数据 设置时间段
        Date date = new Date();
        //取当前时间
        String today = sdf.format(date);
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        //把日期往前减少一天.整数往后推,负数往前移动
        calendar.add(calendar.DATE,-1);
        date = calendar.getTime();
        //取当前时间的前一天
        String yesterday = sdf.format(date);

        //System.out.println("2 名录库推送到法人库 "+map.get("tableCode")+" 定时任务: endtime = "+today+" starttime = "+yesterday);

        mlkPushDataToFrService.pushDataToFr(username, tableCode, tableName, yesterday, today);

    }
}
