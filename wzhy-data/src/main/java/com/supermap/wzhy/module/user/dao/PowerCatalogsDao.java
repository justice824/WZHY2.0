package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.entity.TUserpowercatalog;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * 权限分类dao
 *
 * @author Created by Administrator on 14-2-27.
 */
public interface PowerCatalogsDao extends JpaRepository<TUserpowercatalog, Integer> {


}
