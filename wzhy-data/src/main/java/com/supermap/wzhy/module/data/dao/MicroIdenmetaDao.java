package com.supermap.wzhy.module.data.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TMicroIdenmeta;
import com.supermap.wzhy.entity.TMicroTablemeta;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 基本指标配置信息dao
 *
 * @author Created by W.Qiong on 14-3-4.
 */
public interface MicroIdenmetaDao extends BaseDao<TMicroIdenmeta, Integer> {

    /**
     * 通过指定调查对象查询基层对象基本指标配置信息列表(t.status = 1)
     *
     * @param id
     *            基层调查对象(mitmid)id
     * @return 基本指标配置信息列表
     */
    @Query("select t from TMicroIdenmeta  t where t.TMicroTablemeta.mitmid=:id and t.status = 1 order by t.orderby,t.miimid")
    public List<TMicroIdenmeta> findObjectIdens(@Param("id") int id);

    /**
     * 通过指定的基层调查对象(mitmid)id和指标配置信息(miimid)id查询基层对象基本指标配置信息
     *
     * @param microid
     *            基层调查对象(mitmid)id
     * @param indicatorid
     *            指标配置信息(miimid)id
     * @return 基层对象基本指标配置信息
     */
    @Query("select t from TMicroIdenmeta t where t.TMicroTablemeta.mitmid=:microid and t.miimid=:indicatorid")
    public TMicroIdenmeta findOneIdenmeta(@Param("microid") int microid, @Param("indicatorid") int indicatorid);

    /**
     *根据多个（miimid）id查询基层对象基本指标配置信息列表
     *
     * @param ids
     *          一个或者多个miimid，多个用','逗号隔开
     * @return 基层对象基本指标配置信息列表
     */
    @Query("select  t from TMicroIdenmeta  t where t.miimid in(:ids)")
    public List<TMicroIdenmeta> findByids(@Param("ids") String ids);



    /**
     *根据多个（miimid）id查询基层对象基本指标配置信息列表
     *
     * @param
     *          一个或者多个miimid，多个用','逗号隔开
     * @return 基层对象基本指标配置信息列表
     */
    @Query("select  t from TMicroIdenmeta  t where t.TMicroTablemeta = ?1")
    public List<TMicroIdenmeta> findByMitmid(TMicroTablemeta tMicroTablemeta);

    /**
     * 根据多个（miimid）id查询基层对象基本指标配置信息列表
     * @param ids
     *          一个或者多个miimid
     * @return 基层对象基本指标配置信息列表
     */
    @Query("select  t from TMicroIdenmeta  t where t.miimid in ?1")
    public List<TMicroIdenmeta> findByids(int[] ids);

    /**
     * 检查该基层指标元数据名称是否已经存在
     * <p style="color:red;">
     * 说明：<br/>
     * 可用用来判断指标名称的唯一性时使用
     * </p>
     *
     * @param name
     *            指标名称
     * @return 基本指标配置信息列表（size>0，代表存在）
     */
    @Query("select t from TMicroIdenmeta  t where t.idenName =?1")
    public List<TMicroIdenmeta> checkNameIsExist(String name);

    /**
     * 检查指定基层调查对象(mitmid)id下指标编码是否已经存在
     *
     * <p style="color:red;">
     * 说明：<br/>
     * 可用用来判断指标编码的唯一性时使用
     * </p>
     *
     * @param mitmid
     *            基层调查对象(mitmid)id
     * @param idenCode
     *            指标编码
     * @return 基本指标配置信息列表（size>0，代表存在）
     */
    @Query("select t  from TMicroIdenmeta  t where t.TMicroTablemeta.mitmid =?1 and t.idenCode=?2")
    public List<TMicroIdenmeta> checkIsExist(int mitmid, String idenCode);

    /**
     * 找到最大的指标代码
     * <p style="color:red;">
     * 说明：<br/>
     * 指标编码B为前缀的最大值（Bxxxxx）
     * </p>
     *
     * @return 最大的指标代码
     */
    @Query("select  max(t.idenCode) from TMicroIdenmeta  t where  t.idenCode like 'B%'")
    public  String findMaxCode();

    /**
     * 获取指定的基层调查对象(mitmid)id下的最大的orderby
     *
     * @param mitmid
     *            基层调查对象(mitmid)id
     * @return orderby的最大值
     */
    @Query("select  max(t.orderby) from TMicroIdenmeta  t where t.TMicroTablemeta.mitmid =?1")
    public  Integer findMaxOrderBy(int mitmid);

    /**
     * 通过指定的基层调查对象(mitmid)id和指标配置信息(miimid)(一个或者多个)id查询基层对象基本指标配置信息
     *
     * @param microid
     *            基层调查对象(mitmid)id
     * @param indicators
     *            指标配置信息(miimid)(一个或者多个)id
     * @return 基层对象基本指标配置信息列表
     */
    @Query("select t from TMicroIdenmeta t where t.TMicroTablemeta.mitmid=:microid and t.idenCode in :indicators")
    public  List<TMicroIdenmeta> findIdenmetasByIds(@Param("microid") int microid, @Param("indicators") String[] indicators);


    /**
     * 根据idencode 和 mitmid获取基本指标配置信息
     *
     * @param mitmid
     *            基层调查对象(mitmid)id
     * @param idencode
     *            指标代码
     * @return 基本指标配置信息
     */
    @Query("select t from TMicroIdenmeta t where t.TMicroTablemeta.mitmid=?1 and t.idenCode=?2 AND ROWNUM = 1")
    public TMicroIdenmeta findByIdenCode(int mitmid, String idencode);



    @Query("select t from TMicroIdenmeta t where t.TMicroTablemeta.mitmid=?1 and t.isfjg =1 AND t.status >0")
    public List<TMicroIdenmeta> findByWJGIden(int mitmid);
}
