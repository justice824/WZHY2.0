package com.supermap.wzhy.module.fr.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TUserlogFrwjg;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Administrator on 17-2-23.
 */
public interface WjgDao extends BaseDao<TUserlogFrwjg,Integer> {
    @Query("select t from TUserlogFrwjg t")
    public List<TUserlogFrwjg> getAllFrPushLog();
}