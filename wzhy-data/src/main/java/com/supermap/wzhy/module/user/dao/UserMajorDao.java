package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TUsermajor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 专业信息（关联基层定报）
 *
 * @author Created by TJ on 14-2-27.
 */
public interface UserMajorDao extends BaseDao<TUsermajor,Integer> {

    /**
     * 根据专业id查找TUserMajor
     *
     * @param mid
     * 			专业id
     * @return 专业对象
     */
    public TUsermajor findBymajorid(String mid);

    /**
     * 找到用户所属的专业列表
     *
     * @param userid
     * 			指定用户id
     * @return 专业列表
     */
    @Query("select o.TUsermajor from TUsermajorrelation  o where o.TUsers.userid =?1")
    public List<TUsermajor>  findByUserId(int userid);


    /**
     * 按类别获取专业 1为基层定报，3为调查对象权限控制
     *
     * @param dataType
     * 			专业类型
     * @return 专业列表
     */
    @Query("select o from TUsermajor  o where  o.datatype=?1 and o.status>0")
    public List<TUsermajor>  findByDatatype(int dataType);


    /**
     * 根据专业id获取专业信息
     * @param majorid
     * @return
     */
    @Query("select o from TUsermajor o where o.majorid = ?1")
    public List<TUsermajor> findById(int majorid);
}
