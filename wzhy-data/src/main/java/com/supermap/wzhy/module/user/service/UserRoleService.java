package com.supermap.wzhy.module.user.service;

import com.supermap.wzhy.common.annotation.Permission;
import com.supermap.wzhy.common.annotation.Role;
import com.supermap.wzhy.common.service.BaseService;
import com.supermap.wzhy.util.tree.DHTMLXTree;
import com.supermap.wzhy.util.tree.DHTMLXTreeFactory;
import com.supermap.wzhy.data.ExcelUtil;
import com.supermap.wzhy.data.cons.CTree;
import com.supermap.wzhy.entity.TUserrole;
import com.supermap.wzhy.module.user.dao.DataRolePowerDao;
import com.supermap.wzhy.module.user.dao.UserRoleDao;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户角色server
 *
 * @author Created by W.Qiong on 14-2-27.
 */
@Service
public class UserRoleService extends BaseService {

    /** 用户角色dao */
    @Autowired
    UserRoleDao userRoleDao;

    /** 角色的数据权限dao */
    @Autowired
    DataRolePowerDao dataRolePowerDao;

    /**
     * 创建用户角色信息
     *
     * @param role
     *            将角色对象
     * @return 是否创建成功
     */
    public boolean create(TUserrole role) {
        if (role != null) {
            userRoleDao.save(role);
            return true;
        }
        return false;
    }

    /**
     * 删除一个角色信息
     * <p style="color:red;">
     * 说明：<br/>
     * 此方法为硬删除。
     * </p>
     *
     * @param id
     *            角色id
     * @return 是否删除成功
     */
    public boolean delete(int id) {
        TUserrole role = userRoleDao.findOne(id);
        if (role != null) {
            userRoleDao.delete(role);
            return true;
        }
        return false;
    }

    /**
     * 更新角色信息
     *
     * @param id
     *            角色id
     * @param roleEntity
     *            更新后的数据
     * @return 是否更新成功
     */
    public boolean update(int id, TUserrole roleEntity) {
        TUserrole role = userRoleDao.findOne(id);
        if (role != null) {
            userRoleDao.save(roleEntity);
            return true;
        }
        return false;
    }

    /**
     * 分页查询角色信息
     *
     * @param pageRequest
     *            分页请求对象
     * @return 分页角色信息
     */
    public Page<TUserrole> query(PageRequest pageRequest) {
        return userRoleDao.findAll(pageRequest);
    }

    /**
     * 分页查询角色信息
     *
     * @param role
     *            指定角色信息
     * @param pageRequest
     *            分页请求对象
     * @return 分页角色信息
     */
    public Page<TUserrole> query(TUserrole role, PageRequest pageRequest) {
        return userRoleDao.findAll(pageRequest);
    }

    /**
     * 通过指定父节点找到指定角色列表（子节点信息）
     *
     * @param id
     *            父节点id
     * @return 角色列表（子节点信息）
     */
    public List<TUserrole> getRolesByparid(int id) {
        return userRoleDao.findUserrolesByparid(id);
    }

    /**
     * 通过角色id取得信息
     *
     * @param id
     *            角色id
     * @return 角色信息
     */
    public TUserrole one(int id) {
        if (userRoleDao.exists(id)) {
            return userRoleDao.findOne(id);
        }
        return null;
    }

    /**
     * 根据用户id获取角色所有角色列表
     *
     * @param userid
     *            用户id
     * @return 角色列表
     */
    public List<TUserrole> getByUserid(int userid) {
        return userRoleDao.findUserrolesByuserid(userid);
    }

    /**
     * 获取指定权限id的所有角色列表
     *
     * @param powerid
     *            权限id
     * @return 角色列表
     */
    @Permission(Role.ADMIN)
    public List<TUserrole> findUserrolesByPower(int powerid) {
        return userRoleDao.findUserrolesBypowerid(powerid);
    }

    /**
     * 导入角色模板
     *
     * @return HSSFWorkbook对象
     */
    public HSSFWorkbook downloadTemplate() {
        HSSFWorkbook hssfWorkbook = new HSSFWorkbook();

        try {
            /*** 设置字体和宽度 **/
            HSSFFont font = hssfWorkbook.createFont();
            font.setFontHeightInPoints((short) 10);
            // 字体
            font.setFontName("黑体");
            // 宽度
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

            /*** 设置单元格类型样式 **/
            HSSFCellStyle cellStyle = hssfWorkbook.createCellStyle();
            cellStyle.setFont(font);
            // 水平布局：居中
            cellStyle.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            // 带边框
            cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyle.setWrapText(true);
            // 行底色
            cellStyle.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

            /*** 创建工作簿和单元数据 **/
            HSSFSheet sheet = hssfWorkbook.createSheet();
            HSSFRow head = sheet.createRow(0);
            HSSFCell nameCell = head.createCell(0);
            nameCell.setCellValue("名称");
            nameCell.setCellStyle(cellStyle);

            HSSFCell codeCell = head.createCell(1);
            codeCell.setCellValue("备注");
            codeCell.setCellStyle(cellStyle);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return hssfWorkbook;
    }

    /**
     * 将Excel数据导人数据库
     *
     * @param path
     *            excel文件路径（包含了文件名）
     * @param sheetName
     *            工作簿名
     * @param headerRows
     *            从序号headerRows行后读
     * @return 是否导入成功
     */
    public boolean importRole(String path, String sheetName, int headerRows) {

        List<List<Object>> dataList = ExcelUtil.readExcel(path, sheetName,
                headerRows);
        if (dataList != null) {
            for (List<Object> l : dataList) {
                TUserrole role = new TUserrole();
                role.setRolename(l.get(0).toString());
                role.setRolememo(l.get(1).toString());
                userRoleDao.save(role);
            }
        }
        return true;
    }

    /**
     * 获取角色树（全树）
     * <p style="color:red;">
     * 注意：<br/>
     * 此方法将会采用递归方式将树的所有节点一次性返回，
     * </p>
     *
     * @return 角色树对象（{"content":...}）
     */
    public Map<String, Object> getRoleTree() {
        Map<String, Object> re = new HashMap<String, Object>();

        DHTMLXTree roleTree = new DHTMLXTree("root", "用户角色树").noCheckbox();

        // 找到根下的一级节点
        List<TUserrole> list = userRoleDao
                .findUserrolesByparid(CTree.ROOT_VALUE);
        for (TUserrole tUserrole : list) {

            int roleId = tUserrole.getRoleid();
            String roleName = tUserrole.getRolename();

            DHTMLXTree node = new DHTMLXTree(roleId + "", roleName).open();

            // 查找子节点
            findSubNodeByid(node, roleId);
            roleTree.add(node);
        }

        re.put("content", DHTMLXTreeFactory.toTree(roleTree));

        return re;
    }

    /**
     * 将指定roleid的所有子节点向上一层
     * <p style="color:red;">
     * 说明：<br/>
     * 将父节点为roleid的所有节点的父节点更新为parid（即roleid的子节点，上移一位）
     * </p>
     * @param roleid
     * 			当前角色id
     * @param parid
     * 			当前角色的父节点id
     * @return 是否执行成功
     */
    public boolean moveNextSubItemsByroleid(int roleid, int parid) {
        String sql = "update T_Userrole t set t.parid=? where t.parid=?";
        int count = userRoleDao.executeBySql(sql, parid, roleid);
        return count > 0;
    }

    /**
     * 列表当前指定角色下的的所有权限
     *
     * <p style="color:red;">
     * 注意：<br/>
     * 1.包括子角色（角色有树形结构）的系统权限<br/>
     * 2.返回的结果重复的权限
     * </p>
     * @param roleid
     * 			角色id
     * @return 权限列表（{"sys_rolepower":...}）
     */
    public Map<String, Object> getAllRolepowerByRoleId(int roleid) {
        Map<String, Object> re = new HashMap<>();

        String sql = "select distinct t.powerid,c.power_value,c.power_name from t_rolepowerrelation t,"
                + "(select distinct * from t_userrole start with roleid =?"
                + "connect by prior roleid = parid) b,t_userpower c "
                + "where t.roleid=b.roleid and c.powerid = t.powerid";

        re.put("sys_rolepower", userRoleDao.query(sql, roleid));

        return re;
    }


    /**
     * (递归)获取指定节点下的所有子节点
     *
     * @param node
     * 			当前节点
     * @param roleId
     * 			当前节点的角色id
     */
    private void findSubNodeByid(DHTMLXTree node, int roleId) {

        List<TUserrole> list = userRoleDao.findUserrolesByparid(roleId);
        if (list != null && list.size() > 0) {
            for (TUserrole tUserrole : list) {
                DHTMLXTree subNode = new DHTMLXTree(tUserrole.getRoleid() + "",
                        tUserrole.getRolename()).open();
                // 递归
                findSubNodeByid(subNode, tUserrole.getRoleid());

                // 添加子节点
                node.add(subNode);
            }
        }
    }
}
