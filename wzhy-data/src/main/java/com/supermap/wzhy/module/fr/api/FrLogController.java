package com.supermap.wzhy.module.fr.api;

import com.supermap.wzhy.common.api.BaseController;
import com.supermap.wzhy.data.PageInfo;
import com.supermap.wzhy.entity.TUserlogPush;
import com.supermap.wzhy.module.fr.service.FrPushLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by hezhiyao on 2017/1/3.
 * 查询推送日志
 */
@Controller
@RequestMapping(value = {"/frLog","/service/frLog"})
public class FrLogController extends BaseController{
    @Autowired
    FrPushLogService frPushLogService;

    /**
     * 查询推送日志列表
     * @param request
     * @param pageInfo
     * @return
     */
    @RequestMapping(value = "/pushLog/all", method = RequestMethod.GET)
    @ResponseBody
    public List<TUserlogPush> getAll(HttpServletRequest request,PageInfo pageInfo){
        String userName = this.getSessionUser(request).getUserName();
        String sql = "SELECT t FROM TUserlogPush t WHERE t.username='" + userName + "'";
        return this.frPushLogService.getAll(sql,pageInfo);
    }

    @RequestMapping(value = "/pushLog/size", method = RequestMethod.GET)
    @ResponseBody
    public int size(HttpServletRequest request,String key) {
        String userName = this.getSessionUser(request).getUserName();
        String sql = null;
        if(key!=null && key!=""){
            sql = "FROM TUserlogPush t WHERE t.username='" + userName + "' AND " + key;
        }else{
            sql = "FROM TUserlogPush t WHERE t.username='" + userName + "'";
        }

        return this.frPushLogService.size(sql);
    }
}
