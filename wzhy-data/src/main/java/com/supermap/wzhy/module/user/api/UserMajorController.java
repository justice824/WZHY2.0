package com.supermap.wzhy.module.user.api;

import com.supermap.wzhy.common.annotation.Permission;
import com.supermap.wzhy.common.annotation.Role;
import com.supermap.wzhy.common.api.BaseController;
import com.supermap.wzhy.data.OpStatus;
import com.supermap.wzhy.data.PageInfo;
import com.supermap.wzhy.data.Sort;
import com.supermap.wzhy.entity.TUsermajor;
import com.supermap.wzhy.entity.TUsers;
import com.supermap.wzhy.module.user.service.UserMajorService;
import com.supermap.wzhy.module.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * Created by TJ on 14-2-27.
 * 用户专业
 */
@Controller
@RequestMapping(value = {"/majors","/service/majors"})
public class UserMajorController extends BaseController {
    @Autowired
    UserMajorService userMajorService;
    @Autowired
    UserService userService;

    @Permission(Role.USER)
    @RequestMapping(value="",method= RequestMethod.POST)
     public @ResponseBody TUsermajor createUserMajor(TUsermajor usermajor){
         userMajorService.create(usermajor);
         return usermajor;
     }

    @Permission(Role.USER)
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public @ResponseBody TUsermajor getUserMajor(@PathVariable("id") int id){
        TUsermajor usermajor = userMajorService.findOne(id);
        return usermajor;
    }

    @Permission(Role.USER)
    @RequestMapping(value="",method = RequestMethod.GET)
    public @ResponseBody   Page<TUsermajor> getAllUserMajor(TUsermajor usermajor,PageInfo page,Sort sort){
        return  userMajorService.findAll(getPageRequest(page));
       // return null;
    }

    @Permission(Role.USER)
    @RequestMapping(value="/{id}" ,method = RequestMethod.DELETE)
    public @ResponseBody OpStatus deleteUserMajor(@PathVariable("id") int id){
        boolean re = userMajorService.delete(id);
        return  getOpStatus(re);
    }

    @Permission(Role.USER)
    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    public @ResponseBody OpStatus updateUserMajor(@PathVariable("id") int id,TUsermajor usermajor){
        boolean re = userMajorService.update(id, usermajor);
        return getOpStatus(re);
    }

    @Permission(Role.USER)
    @RequestMapping(value = "/{majorid}/users",method = RequestMethod.GET)
    public @ResponseBody List<TUsers> getUsers(@PathVariable("majorid") int majorid){
        List<TUsers>  userses =  userService.findByUserMajorId(majorid);
        return  userses;
    }

    /**
     * 根据类型获取所有专业信息
     * @param dataType
     * @return
     */
    @RequestMapping(value = "datatype/{dataType}",method = RequestMethod.GET)
    @ResponseBody
    public List<TUsermajor> getMajrosByType(@PathVariable int dataType){
        return  userMajorService.findByDataType(dataType);
    }

}
