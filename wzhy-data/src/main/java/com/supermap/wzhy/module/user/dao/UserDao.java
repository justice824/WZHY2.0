package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.entity.TUsers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 用户相关dao
 * <p>
 * 说明：<br/>
 * 对TUser表的操作，返回的都是用户相关对象
 * </p>
 *
 * @author Created by W.Hao on 14-1-20.
 */
public interface UserDao extends JpaRepository<TUsers, Integer> {

    /**
     * 根据用户名、密码获取用户（验证登陆）
     *
     * @param username
     *            用户名（登录名）
     * @param password
     *            密码
     * @return 返回匹配的用户对象，或者null
     */
    @Query("select o from TUsers  o where o.userName = ?1 and o.password = ?2 and o.status > 0")
    public TUsers findByUserNameAndPassword(String username, String password);

    /**
     * 测试用户名获取
     *
     * @param username
     *            用户名（登录名）
     * @return 返回匹配的用户对象，或者null
     */
    @Query("select o  from TUsers o where o.userName =?1 and o.status =1 ")
    public TUsers findByUserName(String username);

    /**
     * 根据专业Major的id查询 TUser列表
     *
     * <p style="color:red;">
     * 说明：<br/>
     * 对TUsermajorrelation(用户/专业关系表)表的操作,<br/>
     * 从中获取指定专业下的所有用户
     * </p>
     *
     * @param mid
     *            专业Major的id
     * @return 用户列表
     */
    @Query(value = "select t.TUsers from TUsermajorrelation t where t.TUsermajor.majorid = ?")
    public List<TUsers> findByTuserByMajor(int mid);

    /**
     * 根据Role的id查询TUser列表
     *
     * <p style="color:red;">
     * 说明：<br/>
     * 对TUserrolerelation(用户/角色关系表)表的操作,<br/>
     * 从中获取指定角色下的所有用户
     * </p>
     *
     * @param roleid
     *            角色id
     * @return 用户列表
     */
    @Query("select  o.TUsers from TUserrolerelation  o where o.TUserrole.roleid= ?1")
    public List<TUsers> findByTUserByRole(int roleid);

    /**
     * 根据Power的id查询TUser列表
     *
     * <p style="color:red;">
     * 说明：<br/>
     * 对TUserpowerrelation(用户/权限关系表)表的操作,<br/>
     * 从中获取指定权限下的所有用户
     * </p>
     *
     * @param powerid
     *            权限id
     * @return 用户列表
     */
    @Query("select u.TUsers from TUserpowerrelation u where u.TUserpower.powerid = :id")
    public List<TUsers> findUsersByPower(@Param("id") int powerid);

    /**
     * 模糊（like）查询用户区域级别及以下的用户信息
     *
     * @param userRegion
     *            指定用户所在的级别
     * @return 分页用户列表
     */
    @Query("select o from TUsers o where o.userRegion like ?1%")
    public Page<TUsers> findByUserRegion(String userRegion, Pageable pageable);

    /**
     * 获取所有用户列表（通过用户id排序）
     *
     * @return
     */
    @Query("select o from TUsers o where o.status =1 order by o.userid")
    public List<TUsers> findAllUsers();
}
