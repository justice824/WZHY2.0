package com.supermap.wzhy.module.sys.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TParameters;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 系统参数配置
 * Created by W.Qiong on 16-12-27.
 */
public interface ParameterDao extends BaseDao<TParameters, Integer> {

    @Query("select t from TParameters t where t.key=?1")
    public List<TParameters> findByKey(String key);
}
