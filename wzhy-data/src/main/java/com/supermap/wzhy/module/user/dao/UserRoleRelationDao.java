package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TUserrolerelation;

/**
 * Created by Linhao on 2014/10/24.
 *
 * 用户/角色关系
 */
public interface UserRoleRelationDao extends BaseDao<TUserrolerelation,Integer> {


}
