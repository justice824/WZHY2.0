package com.supermap.wzhy.module.user.service;

import com.supermap.wzhy.common.service.BaseService;
import com.supermap.wzhy.entity.*;
import com.supermap.wzhy.module.user.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户系统权限server
 *
 * @author Created by W.Qiong on 14-10-21.
 */
@Service
public class SysPowerService extends BaseService {
    /** 权限dao */
    @Autowired
    PowerDao powerDao;

    /** 角色dao */
    @Autowired
    UserRoleDao userRoleDao;

    /** 用户相关dao */
    @Autowired
    UserDao userDao;

    /** 用户/权限关系dao */
    @Autowired
    UserpowerrelationDao userpowerrelationDao;

    /** 角色/权限关系dao */
    @Autowired
    RolepowerrelationDao rolepowerrelationDao;

    /**
     * 获取指定用户id、权限分类id和角色(一个或者多个)id的系统权限（去重复）
     * @param userid
     * 			用户id
     * @param pcataid
     * 			权限分类id
     * @param roleids
     * 			角色(一个或者多个)id
     *
     * @return 系统权限（去重复）{powerid：powerid}
     */
    public Map getUserSysPowers(int userid, int pcataid, int[] roleids) {
        Map<Integer, String> map = new HashMap<>();

        //获取指定权限分类id和用户id下的系统权限列表
        List<TUserpower> userpowers = powerDao.findPowerByUserAndCatalog(pcataid, userid);
        if(userpowers != null){
            for (TUserpower userpower : userpowers) {
                int powerid = userpower.getPowerid();
                if (map.containsKey(powerid)) {
                    continue;
                }
                map.put(powerid, powerid + "");
            }
        }

        //获取多个角色下的某类系统权限
        List<TUserpower> rolesPowers = null;
        if (roleids != null && roleids.length > 0) {
            rolesPowers = powerDao.findRolePowerByPar(roleids, pcataid);
        }
        if(rolesPowers != null){
            for (TUserpower rolepower : rolesPowers) {
                int powerid = rolepower.getPowerid();
                if (map.containsKey(powerid)) {
                    continue;
                }
                map.put(powerid, powerid + "");
            }
        }

        return map;
    }

    /**
     * 获取指定角色id和权限分类id下的系统权限（去重复）
     *
     * @param roleid
     * 			角色id
     * @param pcataid
     * 			权限分类id
     * @return 系统权限（去重复）{powerid：powerid}
     */
    public Map getRoleSysPowers(int roleid, int pcataid) {
        int[] roleids = new int[1];
        roleids[0] = roleid;
        Map<Integer, String> map = new HashMap<>();
        List<TUserpower> rolesPowers = powerDao.findRolePowerByPar(roleids,
                pcataid);
        if(rolesPowers != null){
            for (TUserpower rolepower : rolesPowers) {
                int powerid = rolepower.getPowerid();
                if (map.containsKey(powerid)) {
                    continue;
                }
                map.put(powerid, powerid + "");
            }
        }
        return map;
    }

    /**
     * 设置用户系统权限
     * <p>
     *  注意：<br/>
     * 	重新设置，先清除原来的权限再设置
     * </p>
     * @param userid
     * 			用户id
     * @param powerids
     * 			权限(一个或者多个)id
     * @param pcataid
     * 			权限分类id
     * @return 是否设置成功过
     */
    public boolean setUserSysPowers(int userid, int[] powerids, int pcataid) {

        List<TUserpowerrelation> hasPowerRelas = userpowerrelationDao
                .findPowerByUser(pcataid, userid);
        userpowerrelationDao.delete(hasPowerRelas);
        TUsers user = userDao.findOne(userid);
        if (null == user) {
            return false;
        }
        for (int powerid : powerids) {
            TUserpower userpower = powerDao.findOne(powerid);
            if (null != userpower) {
                TUserpowerrelation rela = new TUserpowerrelation();
                rela.setTUserpower(userpower);
                rela.setTUsers(user);
                userpowerrelationDao.save(rela);
            }
        }
        return true;
    }

    /**
     * 设置角色系统权限
     * <p>
     *  注意：<br/>
     * 	重新设置，先清除原来的权限再设置
     * </p>
     * @param roleid
     * 			角色id
     * @param powerids
     * 			权限(一个或者多个)id
     * @return 是否设置成功过
     */
    public boolean setRoleSysPowers(int roleid, int[] powerids, int pcataid) {
        List<TRolepowerrelation> hasPowerRelas = rolepowerrelationDao
                .findRolePowerByPar(roleid, pcataid);
        rolepowerrelationDao.deleteInBatch(hasPowerRelas);
        TUserrole userrole = userRoleDao.findOne(roleid);
        if (null == userrole) {
            return false;
        }

        for (int powerid : powerids) {
            TUserpower userpower = powerDao.findOne(powerid);
            if (null != userpower) {
                TRolepowerrelation rela = new TRolepowerrelation();
                rela.setTUserpower(userpower);
                rela.setTUserrole(userrole);
                rolepowerrelationDao.save(rela);
            }
        }
        return true;
    }
}
