package com.supermap.wzhy.module.fr.api;

import com.supermap.wzhy.common.api.BaseController;
import com.supermap.wzhy.data.PageInfo;
import com.supermap.wzhy.entity.TUserlogFrwjg;
import com.supermap.wzhy.entity.TUserlogPush;
import com.supermap.wzhy.module.fr.service.PushLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by hezhiyao on 2017/1/3.
 * 查询推送日志
 */
@Controller
@RequestMapping(value = {"/log","/service/frLog"})
public class LogController extends BaseController{
    @Autowired
    PushLogService pushLogService;

    /**
     * 查询推送日志列表
     * @param request
     * @param type
     * @param key
     * @param pageInfo
     * @return
     */
    @RequestMapping(value = "/pushLog/all", method = RequestMethod.POST)
    @ResponseBody
    public List<TUserlogPush> getAll(HttpServletRequest request,String type,String key, @RequestBody PageInfo pageInfo){
        String sql = null;
        if(key!=null && key!=""){
            sql = "SELECT t FROM TUserlogPush t WHERE t.type='" + type + "' " + key + " order by t.endtime DESC";
        }else{
            sql = "SELECT t FROM TUserlogPush t WHERE t.type='" + type + "' order by t.endtime DESC";
        }
        //System.out.println(sql);
        List<TUserlogPush> tUserlogPushs = this.pushLogService.getAllFrPushLogs(sql, pageInfo);
        return tUserlogPushs;
    }

    @RequestMapping(value = "/pushLog/size", method = RequestMethod.POST)
    @ResponseBody
    public int size(HttpServletRequest request,String type,String key) {
        String sql = null;
        if(key!=null && key!=""){
            sql = "FROM T_USERLOG_PUSH t WHERE t.type='" + type + "' " + key;
        }else{
            sql = "FROM T_USERLOG_PUSH t WHERE t.type='" + type+"'";
        }
        int sum = this.pushLogService.size(sql);
        return sum;
    }

    /**
     * 查询文件柜日志
     * @param request
     * @param key
     * @param pageInfo
     * @return
     */
    @RequestMapping(value = "/frWjgLog/all", method = RequestMethod.POST)
    @ResponseBody
    public List<TUserlogFrwjg> getWjgAll(HttpServletRequest request,String key, @RequestBody PageInfo pageInfo){
        String sql = null;
        if(key!=null && key!=""){
            sql = "SELECT t FROM TUserlogFrwjg t WHERE 1=1 " + key + " order by t.endtime DESC";
        }else{
            sql = "SELECT t FROM TUserlogFrwjg t order by t.endtime DESC";
        }
        //System.out.println(sql);
        List<TUserlogFrwjg> tUserlogFrwjg = this.pushLogService.getAllFrWjgLogs(sql, pageInfo);
        return tUserlogFrwjg;
    }

    @RequestMapping(value = "/frWjgLog/size", method = RequestMethod.POST)
    @ResponseBody
    public int frWjgSize(HttpServletRequest request,String key) {
        String sql = null;
        if(key!=null && key!=""){
            sql = "FROM T_USERLOG_FRWJG t WHERE 1=1 " + key;
        }else{
            sql = "FROM T_USERLOG_FRWJG t";
        }
        int sum = this.pushLogService.size(sql);
        return sum;
    }
}
