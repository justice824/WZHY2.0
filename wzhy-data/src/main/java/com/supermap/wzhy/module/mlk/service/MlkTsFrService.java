package com.supermap.wzhy.module.mlk.service;

import com.supermap.wzhy.module.mlk.dao.MlDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/12/28 0028.
 */
@Service
public class MlkTsFrService {
    @Autowired
    MlDao mlDao;

    /**
     * 根据构建的插入sql, 批量保存来源于法人库的数据
     * @param sqls
     * @return
     */
    public Map saveFromFrSql(List<String> sqls){
        //this.mlDao.save
        return this.mlDao.batchSqlExecute(sqls,sqls.size());
    }
}
