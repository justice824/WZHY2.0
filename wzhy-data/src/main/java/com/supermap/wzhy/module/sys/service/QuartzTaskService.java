package com.supermap.wzhy.module.sys.service;

import com.supermap.wzhy.data.QuartzManager;
import com.supermap.wzhy.data.SysConstant;
import com.supermap.wzhy.entity.TUsers;
import com.supermap.wzhy.module.fr.service.FrPushDataToMlkService;
import com.supermap.wzhy.module.fr.service.FrWjgService;
import com.supermap.wzhy.module.mlk.service.MlkPushDataToFrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sun'fei on 17-2-19.
 * 定时任务处理业务类
 */
@Service
public class QuartzTaskService{

    @Autowired
    FrPushDataToMlkService frPushDataToMlkService;

    @Autowired
    MlkPushDataToFrService mlkPushDataToFrService;

    @Autowired
    FrWjgService frWjgService;

    /**
     * 定时任务业务处理分类
     * @param request 主要用于得到session里的username-添加日志
     * @param mark 业务标识
     * @param time 业务定时时间
     * @param tableCode 业务表code
     * @param tableName 业务表名
     * @param jobName Job定时任务名称
     * @return 定时处理状态
     */
    public boolean task(HttpServletRequest request,String mark,String time,String min,
                        String tableCode,String tableName,String jobName){
        TUsers user = (TUsers)request.getSession().getAttribute(SysConstant.CURRENT_USER);
        String username = user.getUserName();
        Map map = new HashMap();
        map.put("username",username);

        String sw = request.getParameter("switchs");

        boolean switchs = Boolean.valueOf(sw);

        //时间设置
        //String times = "秒 分 时 天 月 周 年";
        String times = "";

        //延时任务参数
        String startTime = "";
        String endTime = "";
        String delayedDate = "";

        map.put("switchs",switchs);

        if(switchs){
            //延时任务参数
            startTime = request.getParameter("startTime");
            endTime = request.getParameter("endTime");
            delayedDate = request.getParameter("delayedDate");

            if(startTime == ""){
                startTime = "2000-01-01";
            }

            map.put("startTime",startTime);
            map.put("endTime",endTime);
            //设置延时任务名 避免冲突
            jobName = endTime+jobName;
            times = "0 "+min+" "+time+" "+delayedDate;
        }else{
            times = "0 "+min+" "+time+" * * ?";
        }

        if(mark != "wjg"){
            //quartz上下文参数
            map.put("tableCode",tableCode);
            map.put("tableName",tableName);

            if(mark == "mlk"){
                QuartzManager.addFrToMlkJob(jobName, WZHYMLKJob.class, times, map,
                        setFrPushDataToMlkServiceDao());
            }
            if(mark == "frk"){
                QuartzManager.addMLkToFrkJob(jobName, WZHYFRKJob.class, times, map,
                        setMlkPushDataToFrServiceDao());
            }
        }else{
            QuartzManager.addWjgJob(jobName, WZHYWJGJob.class, times, map,setFrWjgServiceDao());
        }
        if(!switchs){
            System.out.println("定时任务 "+jobName+" 开始 定时时间 每天 "+time+" 时 "+min+" 分 0 秒!");
        }else{
            System.out.println("延时任务 "+jobName+" 开始 ！时间 : "+time+" 时 "+min+" 分 0 秒!");
        }
        return true;
    }

    /**
     * Job注入法人库service
     * @return 注入的service
     */
    public FrPushDataToMlkService setFrPushDataToMlkServiceDao(){
        return this.frPushDataToMlkService;
    }

    /**
     * Job注入名录库service
     * @return 注入的service
     */
    public MlkPushDataToFrService setMlkPushDataToFrServiceDao(){
        return this.mlkPushDataToFrService;
    }

    /**
     * Job注入文件柜service
     * @return 注入的service
     */
    public FrWjgService setFrWjgServiceDao(){
        return this.frWjgService;
    }

}
