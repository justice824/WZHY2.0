package com.supermap.wzhy.module.data.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TMicroTablemeta;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 基层对象元数据信息dao
 *
 * @author Created by W.Qiong on 14-3-4.
 */
public interface MicroTablemetaDao extends BaseDao<TMicroTablemeta, Integer> {

    /**
     * 按条件查询各类配置对象
     *
     * @param micmeteType
     *            基层数据类型（1调查对象、2统计范围、3定报表模板、4目录树、5系统显示配置）
     * @param reportType
     *            报告期类型（11年、12季、13月）
     * @param tableCode
     *            表编码
     * @param majorid
     *            专业id
     * @return 基层对象元数据信息
     */
    @Query("select o from TMicroTablemeta o where o.micmetaType=?1 and o.reportType=?2 and o.tableCode=?3 and  o.tUsermajor.majorid=?4")
    public TMicroTablemeta findByCondition(int micmeteType, int reportType,
                                           String tableCode, int majorid);

    /**
     * 根据表名查询各类配置对象列表（采用like方式查询）
     *
     * @param tableName
     *            表名
     * @return 基层对象元数据信息列表
     */
    @Query("select o from TMicroTablemeta o where o.tableName like ?1% ")
    public List<TMicroTablemeta> findByTableName(String tableName);


    /**
     * 根据表名查询各类配置对象 结果唯一
     *
     * @param tableName
     * @return
     */
    @Query("select o  from TMicroTablemeta o where o.tableName = ?1")
    public TMicroTablemeta findOneByTableName(String tableName);


    /**
     * 根据表名查询各类配置对象 结果唯一
     *
     * @return
     */
    @Query("select o.mitmid,o.name  from TMicroTablemeta o")
    public List<String> getAllTableName();


    /**
     * 根据元数据类型查询各类配置对象列表（status >0）
     *
     * @param metaType
     *            基层数据类型（1调查对象、2统计范围、3定报表模板、4目录树、5系统显示配置）
     * @return 基层对象元数据信息列表
     */
    @Query("select o from TMicroTablemeta o where o.micmetaType = ?1 and o.status >0 order by o.orderby,o.mitmid")
    public List<TMicroTablemeta> findByMicmetaType(int metaType);

    /**
     * 查询基层数据列表（status >0）
     *
     * @param metaType 基层数据类型（1调查对象、2统计范围、3定报表模板、4目录树、5系统显示配置）
     * @param catalogId 区划类型id
     * @return 基层对象元数据信息列表
     */
    @Query("select o from TMicroTablemeta o where o.micmetaType = ?1 and o.TRegioncatalog.rcid = ?2 and o.status >0 order by o.orderby,o.mitmid")
    public List<TMicroTablemeta> findByTypeAndCatalogId(int metaType, int catalogId);

    /**
     * 查询基层数据列表（status >0）
     *
     * @param metaType 基层数据类型（1调查对象、2统计范围、3定报表模板、4目录树、5系统显示配置）
     * @param catalogId 区划类型id
     * @param moduleId 业务模块ID
     * @return
     */
    @Query("select o from TMicroTablemeta o where o.micmetaType = ?1 and o.TRegioncatalog.rcid = ?2 and o.status >0 and o.module = ?3 order by o.orderby,o.mitmid")
    public List<TMicroTablemeta> findByTypeAndCatalogId(int metaType, int catalogId, String moduleId);

    @Query("select o from TMicroTablemeta  o where o.micmetaType = ?1 and o.reportType = ?2 and o.tUsermajor.majorid = ?3 and o.status > 0 ")
    List<TMicroTablemeta> findByTypeAndMajor(int metaType, int reportType, int major);


    @Query("select o from TMicroTablemeta  o where o.micmetaType = ?1 and o.reportType = ?2 and o.status > 0 ")
    List<TMicroTablemeta> findByType(int metaType, int reportType);


    @Query("select o from TMicroTablemeta  o where o.micmetaType = ?1 and o.TRegioncatalog.rcid = ?2 and o.reportType = ?3 and o.tUsermajor.majorid = ?4 and o.status > 0 ")
    List<TMicroTablemeta> findByTypeAndMajorAndCatalog(int metaType, int catalogid, int reportType, int major);


    @Query("select o from TMicroTablemeta  o where o.micmetaType = ?1 and o.TRegioncatalog.rcid = ?2 and o.reportType = ?3 and o.status > 0 ")
    List<TMicroTablemeta> findByTypeAndCatalog(int metaType, int catalogid, int reportType);


    /**
     * 通过元数据（一个或者多个）id获取对应调查对象列表
     *
     * @param mitmids
     * 			元数据（一个或者多个）id
     * @return 基层对象元数据信息列表
     */
    @Query("select o from TMicroTablemeta o where o.mitmid in ?1 and o.status >0 order by o.orderby,o.mitmid")
    public List<TMicroTablemeta> findByMitmids(int[] mitmids);


    /**
     * 通过表号查询
     * @param tableCode
     * @return
     */
    @Query("select o from TMicroTablemeta o where  o.tableCode=?1")
    public TMicroTablemeta findByTableCode(String tableCode);


    /**
     * 通过mitmid查询
     * @param mitmid
     * @return
     */
    @Query("select o from TMicroTablemeta o where  o.mitmid=?1")
    public TMicroTablemeta findByMitmid(int mitmid);


    /**
     * 通过状态和表号获取
     * @param tableCode
     * @param status
     * @return
     */
    @Query("select o from TMicroTablemeta o where  o.tableCode=?1 and o.status>?2")
    public TMicroTablemeta findByTableCodeStatus(String tableCode, int status);


    /**
     * 返回多张作用表
     * @param tableNames
     * @return
     */
    @Query("select o  from TMicroTablemeta o where o.tableName in ?1")
    public List<TMicroTablemeta> findBatchTableName(List<String> tableNames);
}
