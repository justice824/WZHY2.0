package com.supermap.wzhy.module.context;

import org.hibernate.dialect.Dialect;
import org.hibernate.type.StandardBasicTypes;

import java.sql.Types;

/**
 * Created by sun'fei on 17-2-16.
 * 自定义Informix数据库方言
 */
public class MyDialect extends Dialect {
    //构造方法
    public MyDialect(){
        super();
        //registerHibernateType(Types.CHAR,StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.TIMESTAMP, StandardBasicTypes.STRING.getName());
        registerHibernateType(Types.DECIMAL, StandardBasicTypes.STRING.getName());
        //CHAR
        registerHibernateType(1, StandardBasicTypes.STRING.getName());
        registerHibernateType(1,1, StandardBasicTypes.STRING.getName());
        registerHibernateType(1,255, StandardBasicTypes.STRING.getName());
    }
}
