package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TRolepowerrelation;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 角色/权限关系dao
 *
 * @author Created by ruanruping on 2014/10/14.
 */
public interface RolepowerrelationDao extends BaseDao<TRolepowerrelation,Integer> {

    /**
     * 取得指定角色id和权限分类id下的角色/权限关系列表
     * @param roleid
     * 			角色id
     * @param pcataid
     * 			权限分类id
     * @return 角色/权限关系列表
     */
    @Query("select t from TRolepowerrelation t where t.TUserrole.roleid=?1 and t.TUserpower.TUserpowercatalog.pcataid=?2" +
            " order by t.TUserpower.powerid")
    List<TRolepowerrelation> findRolePowerByPar(int roleid, int pcataid);

}
