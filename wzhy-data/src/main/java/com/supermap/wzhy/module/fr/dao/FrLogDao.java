package com.supermap.wzhy.module.fr.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TUserlogPush;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by hezhiyao on 2017/1/3.
 */
public interface FrLogDao extends BaseDao<TUserlogPush,BigDecimal> {
    @Query("select t from TUserlogPush t")
    public List<TUserlogPush> getAllFrPushLog();
}
