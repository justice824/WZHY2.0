package com.supermap.wzhy.module.sys.service;

import com.supermap.wzhy.module.fr.service.FrWjgService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by Sun'fei on 17-2-22.
 */
@Service
public class WZHYWJGJob implements Job {

    @Autowired
    FrWjgService frWjgService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Map map = jobExecutionContext.getMergedJobDataMap();
            frWjgService = (FrWjgService)map.get("service");

            boolean switchs = (Boolean)map.get("switchs");

            Date date = new Date();
            //取当前时间
            String today = sdf.format(date);

            if(switchs){
                String start = "";
                String end = map.get("endTime").toString();
                if(map.containsKey("startTime")){
                    start = map.get("startTime").toString();
                }else{
                    start = "2000-01-01";
                }
                Date start_time = sdf.parse(start);
                Date end_time = sdf.parse(end);

                System.out.println("文件柜延时任务: 导出 "+start+" —— "+end+" 数据");
                frWjgService.startRunnable(null,start_time,end_time,0);
            }else{
                System.out.println("文件柜定时任务: 导出 "+today+" 当天数据");
                frWjgService.startRunnable(null,sdf.parse(today),sdf.parse(today),0);
            }
        }catch (ParseException e){
            System.out.println("文件柜定时任务异常");
            e.printStackTrace();
        }
    }
}
