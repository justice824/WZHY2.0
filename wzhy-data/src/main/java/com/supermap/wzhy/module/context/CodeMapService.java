package com.supermap.wzhy.module.context;

import javax.naming.NamingException;
import javax.naming.Reference;
import javax.naming.Referenceable;
import javax.naming.StringRefAddr;
import java.util.HashMap;
import java.util.Map;


public class CodeMapService implements Referenceable{
	private Map info;
	@Override
	public Reference getReference() throws NamingException {
		  Reference ref=new Reference(getClass().getName(),CodeMapFactory.class.getName(),null);
		  ref.add(new MapRefAddr("codeMap",this.info));
		  return ref;
	}

	public Map getInfo() {
		return info;
	}

	public void setInfo(Map info) {
		this.info = info;
	}
}
