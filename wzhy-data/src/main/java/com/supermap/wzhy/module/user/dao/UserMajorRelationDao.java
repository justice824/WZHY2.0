package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TUsermajorrelation;

/**
 * Created by TJ on 14-3-2.
 * 用户专业对应
 */
public interface UserMajorRelationDao extends BaseDao<TUsermajorrelation,Integer> {

//    @Query("delete from TUsermajorrelation t where t.TUsers.userid =?1" )
//    public int deleteByUserId(int userid);

}
