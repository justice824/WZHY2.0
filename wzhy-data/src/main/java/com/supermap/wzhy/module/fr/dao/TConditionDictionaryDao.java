package com.supermap.wzhy.module.fr.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TConditionDictionary;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by sun'fei on 2017-09-21.
 */
public interface TConditionDictionaryDao extends BaseDao<TConditionDictionary, Integer> {

    /**
     * 根据ID查询字典
     *
     * @param id
     * @return
     */
    @Query("select o  from TConditionDictionary o where o.id = ?1")
    TConditionDictionary findOneByID(int id);


    @Query("select o.id,o.name  from TConditionDictionary o")
    List getCondition();


}
