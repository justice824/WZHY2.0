package com.supermap.wzhy.module.user.service;

import com.supermap.wzhy.entity.*;
import com.supermap.wzhy.module.user.dao.PowerCatalogsDao;
import com.supermap.wzhy.module.user.dao.PowerDao;
import com.supermap.wzhy.module.user.dao.UserDao;
import com.supermap.wzhy.module.user.dao.UserRoleDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by Administrator on 14-2-27.
 */
@Service
public class PowerCatalogsService {
    @Autowired
    private PowerCatalogsDao powerCatalogsDao;
    @Autowired
    PowerDao powerDao;
    @Autowired
    UserRoleDao userRoleDao;
    @Autowired
    UserDao userDao;

    @Autowired
    PowerService powerService;


    public boolean save(TUserpowercatalog userpowercatalog) {
        if (powerCatalogsDao.save(userpowercatalog) != null) {
            return true;
        } else return false;
    }

    public void delete(int pcataid) {
        powerCatalogsDao.delete(pcataid);
    }

    public TUserpowercatalog find(int pcataid) {
        return powerCatalogsDao.findOne(pcataid);
    }

    public Page<TUserpowercatalog> findAll(PageRequest pageRequest) {
        return powerCatalogsDao.findAll(pageRequest);
    }

    public boolean update(int powercatalogid, TUserpowercatalog userpowercatalog) {
        if (powerCatalogsDao.getOne(powercatalogid) != null) {
            TUserpowercatalog us = powerCatalogsDao.save(userpowercatalog);
            if (us != null) {
                return true;
            } else return false;
        } else
            return false;
    }

    /**
     * 按照ID删除（不（强制）关联删除）
     * 删除的时候若其分类下的所有权限不被使用，才可以删除
     * @param powercatalogid
     * @return
     */
    public Map<String,Object> deletePowercatalogByidForNoForce(int powercatalogid) {
        Map<String,Object> re = null;

        boolean isCandelete = true; //默认可以删除

        TUserpowercatalog tUserpowercatalog = find(powercatalogid);
        if(tUserpowercatalog !=null){
            //取得该分类下的所有权限
            List<TUserpower> userpowerList = powerDao.findPowerByCatalog(powercatalogid);
            if (userpowerList != null && userpowerList.size() > 0){
                for (int i=0,size=userpowerList.size(); i<size; i++) {
                    TUserpower tUserpower = userpowerList.get(i);
                    if(tUserpower == null)
                        continue;

                    //判断该权限是否被角色使用
                    re = powerService.checkPowerIsUsedByAnyRole(tUserpower);
                    if(re != null){  //该角色已被角色使用
                        isCandelete = false;    //标记不可删除
                    }else{
                        //判断该权限是否被用户使用
                        re = powerService.checkPowerIsUsedByAnyUser(tUserpower);
                        if(re != null){ //该角色已被用户使用
                            isCandelete = false;   //标记不可删除
                        }
                    }//end if(re != null) else

                    if(isCandelete){
                        //取得当前节点的所有子节点(递归)
                        List<TUserpower> allChildren = powerService.getAllChildrenPoweridBypowerid(
                                tUserpower.getTUserpowercatalog().getPcataid(),tUserpower.getPowerid());

                        if(allChildren != null && allChildren.size() > 0){
                            for (TUserpower child : allChildren){
                                if(child == null)
                                    continue;

                                //判断该权限是否被角色使用
                                re = powerService.checkPowerIsUsedByAnyRole(child);
                                if(re != null){  //该角色已被角色使用
                                    isCandelete = false;    //标记不可删除
                                    break;  //退出循环 for (TUserpower child : allChildren)
                                }else{
                                    //判断该权限是否被用户使用
                                    re = powerService.checkPowerIsUsedByAnyUser(child);
                                    if(re != null){ //该角色已被用户使用
                                        isCandelete = false;   //标记不可删除
                                        break;  //退出循环 for (TUserpower child : allChildren)
                                    }
                                }//end if(re != null) else
                            }//end for (TUserpower child : allChildren)
                        } //end if(allChildren != null && allChildren.size() > 0)
                    }//end if(isCandelete)
                }//end for (int i=0,size=userpowerList.size();i<size;i++)

                if(re != null){
                    String msg = re.get("msg").toString();
                    msg = "权限分类["+tUserpowercatalog.getName()+"]下的"+msg;
                    re.put("msg",msg);
                }

            } //end if (userpowerList != null && userpowerList.size() > 0)
        }else{
            isCandelete = false;

            re = new HashMap<String,Object>();  //初始化返回对象
            re.put("status",true);
            re.put("msg","删除权限分类失败！该权限分类不存在！");

        } //end if(tUserpowercatalog !=null) else

        if(isCandelete){
            re = new HashMap<String,Object>();  //初始化返回对象
            re.put("status",true);

            powerCatalogsDao.delete(powercatalogid);   //删除

            re.put("msg","删除权限分类成功！");
        }

        return re;
    }

}
