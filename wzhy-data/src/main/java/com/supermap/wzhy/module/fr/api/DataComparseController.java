package com.supermap.wzhy.module.fr.api;

import com.supermap.wzhy.common.api.BaseController;
import com.supermap.wzhy.data.OpStatus;
import com.supermap.wzhy.module.fr.service.DataComparseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * Created by S'f on 17-3-29.
 */
@Controller
@RequestMapping(value = "/dataComparse")
public class DataComparseController extends BaseController{

    @Autowired
    DataComparseService dataComparseService;

    /**
     *
     * @return
     */
    @RequestMapping(value = "/gs/getData")
    @ResponseBody
    public List getGSData(){
        List list = dataComparseService.getGSData();
        return list;
    }


    @RequestMapping(value = "/gs/refalsh")
    @ResponseBody
    public boolean refalshGSTable(){
        return dataComparseService.refalshGSTable();
    }



    @RequestMapping(value = "/ml/getData")
    @ResponseBody
    public List getMLData(){
        List list = dataComparseService.getMLData();
        return list;
    }


    @RequestMapping(value = "/ml/refalsh")
    @ResponseBody
    public boolean refalshMLTable(){
        return dataComparseService.refalshMLTable();
    }


    /**
     * 下载Excel
     */
    @RequestMapping(value = "/download", method = RequestMethod.GET)
    @ResponseBody
    public OpStatus downloadTemplates(HttpServletRequest request){
        String excel_head = request.getParameter("excel_head");
        String etp = request.getParameter("etp");
        String title = request.getParameter("title");
        dataComparseService.downloadExcel(request,title,excel_head,etp);
        OpStatus op = new OpStatus();
        op.setStatus(true);
        op.setMsg("下载模板成功！");
        return  op;
    }
}
