package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TUserrole;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 用户角色dao
 *
 * @author Created by W.Qiong on 14-2-27.
 */
public interface UserRoleDao extends BaseDao<TUserrole, Integer> {
    /**
     * 根据用户id获取角色列表
     * <p style="color:red;">
     * 说明：<br/>
     * 对TUserrolerelation(用户/角色关系表)表的操作,<br/>
     * 从中获取指定专业下的所有用户
     * </p>
     *
     * @param userid
     *            用户id
     * @return 角色列表
     */
    @Query("select o.TUserrole from TUserrolerelation o where o.TUsers.userid = ?1")
    public List<TUserrole> findUserrolesByuserid(int userid);

    /**
     * 获取指定权限id的所有角色列表
     * <p style="color:red;">
     * 说明：<br/>
     * 对TUserrolerelation(角色/权限关系表)表的操作,<br/>
     * 从中获取指定专业下的所有用户
     * </p>
     *
     * @param powerid
     *            权限id
     * @return 角色列表
     */
    @Query("select  t.TUserrole from TRolepowerrelation t where  t.TUserpower.powerid = :id")
    public List<TUserrole> findUserrolesBypowerid(@Param("id") int powerid);

    /**
     * 通过指定父节点找到指定角色列表（子节点信息）
     *
     * @param parid
     *            父节点id
     * @return 角色列表（子节点信息）
     */
    @Query("select t from TUserrole t where t.parid = :id")
    public List<TUserrole> findUserrolesByparid(@Param("id") int parid);

    /**
     * 根据权限id获取用户角色
     *
     * @param powerid
     * @return
     * @Query("select t.TUserrole from TRolepowerrelation t where
     *                t.TUserpower.parid = :id") public List<TUserrole>
     *                findUserrolesByPower(@Param("id")int powerid);
     */
}