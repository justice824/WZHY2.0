package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.entity.TDataRolepower;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 *
 * 角色数据权限dao
 *
 * @author Created by W.Qiong on 14-10-20.
 */
public interface DataRolePowerDao extends JpaRepository<TDataRolepower, Integer> {

    /**
     * 查询指定角色（一个或者多个）id和数据类型的数据权限列表
     * @param roleid
     * 			角色（一个或者多个）id
     * @param dataType
     * 			数据类型（综合或者基层）
     * @return 数据权限列表
     */
    @Query("select t from TDataRolepower t where t.TUserrole.roleid in ?1 and t.dataType=?2 and t.status=1 order by t.dataId")
    List<TDataRolepower> findByDataType(int[] roleid, int dataType);

    /**
     * 查询指定角色id、数据类型和的数据权限列表
     * @param roleid
     * 			角色id
     * @param dataType
     * 			数据类型（综合或者基层）
     * @param dataids
     * 			data(一个或者多个)Id
     * @return 数据权限列表
     */
    @Query("select t from TDataRolepower t where t.TUserrole.roleid=?1 and t.dataType=?2 and t.dataId in ?3 and t.status=1 order by t.dataId")
    List<TDataRolepower> findByDataIds(int roleid, int dataType, int[] dataids);
}
