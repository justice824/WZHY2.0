package com.supermap.wzhy.module.context;

import java.util.Hashtable;
import java.util.Map;

import javax.naming.Context;
import javax.naming.Name;
import javax.naming.Reference;
import javax.naming.spi.ObjectFactory;

/**
 * 还原服务，映射工厂
 */
public class CodeMapFactory implements ObjectFactory {
	@Override
	public Object getObjectInstance(Object obj, Name arg1, Context arg2,
			Hashtable<?, ?> arg3) throws Exception {
		if (obj instanceof Reference) {
			Reference ref = (Reference) obj;
			Map codeMapInfo = (Map) ref.get("codeMap").getContent();
			CodeMapService service = new CodeMapService();
			service.setInfo(codeMapInfo);
			return service;
		}
		return null;
	}

}
