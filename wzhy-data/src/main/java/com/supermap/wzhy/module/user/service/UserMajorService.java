package com.supermap.wzhy.module.user.service;

import com.supermap.wzhy.common.service.BaseService;
import com.supermap.wzhy.data.cons.CMajorDataType;
//import com.supermap.wzhy.entity.TMajortraderelation;
import com.supermap.wzhy.entity.TMicroIdent;
import com.supermap.wzhy.entity.TMicroIdenvl;
import com.supermap.wzhy.entity.TUsermajor;
//import com.supermap.wzhy.module.data.dao.MicroIdentDao;
//import com.supermap.wzhy.module.data.dao.MicroIdenvlDao;
import com.supermap.wzhy.module.user.dao.UserDao;
import com.supermap.wzhy.module.user.dao.UserMajorDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户所属专业server
 *
 * @author Created by TJ on 14-2-27.
 */
@Service
public class UserMajorService extends BaseService {
    /** 用户所属专业dao */
    @Autowired
    UserMajorDao userMajorDao;

    /** 用户dao */
    @Autowired
    UserDao userDao;


    /**
     * 创建用户专业记录
     *
     * @param entity
     *            要创建的用户对象
     * @return 返回创建后的记录
     */
    public TUsermajor create(TUsermajor entity) {
        return userMajorDao.saveAndFlush(entity);
    }

    /**
     * 删除一条记录
     * <p>
     * 说明：<br/>
     * 此处删除为硬删除
     * </p>
     *
     * @param mid
     *            专业id
     * @return 是否删除成功
     */
    public boolean delete(int mid) {
        TUsermajor usermajor = userMajorDao.findOne(mid);
        if (usermajor != null) {
            userMajorDao.delete(usermajor);
            return true;
        } else
            return false;
    }

    /**
     * 更新一条记录
     *
     * @param id
     *            专业id
     * @param entity
     *            要更新的用户对象
     * @return 是否更新成功
     */
    public boolean update(int id, TUsermajor entity) {
        TUsermajor usermajor = userMajorDao.findOne(id);
        if (usermajor != null) {
            userMajorDao.save(entity);
            return true;
        } else
            return false;

    }

    /**
     * 通过主键找到一条记录
     *
     * @param id
     *            专业id
     * @return 专业记录
     */
    public TUsermajor findOne(int id) {
        return userMajorDao.findOne(id);
    }


    /**
     * 根据类型获取专业
     * @param dataType
     * @return
     */
    public List<TUsermajor> findByDataType(int dataType){
        return userMajorDao.findByDatatype(dataType);
    }

    /**
     * 分页查找所有的专业记录
     *
     * @param pageRequest
     *            分页对象
     * @return 专业记录分页列表
     */
    public Page<TUsermajor> findAll(PageRequest pageRequest) {
        return userMajorDao.findAll(pageRequest);
    }

    /**
     * 找到用户所属的专业
     *
     * @param userid
     *            用户id
     * @return 专业列表
     */
    public List<TUsermajor> getByUserid(int userid) {
        return userMajorDao.findByUserId(userid);
    }

}



