package com.supermap.wzhy.module.fr.service;

import com.supermap.wzhy.data.PageInfo;
import com.supermap.wzhy.entity.TUserlogFrwjg;
import com.supermap.wzhy.entity.TUserlogPush;
import com.supermap.wzhy.module.fr.dao.FrDao;
import com.supermap.wzhy.module.fr.dao.LogDao;
import com.supermap.wzhy.module.fr.dao.WjgDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hezhiyao on 2017/1/3.
 */
@Service
public class PushLogService {
    @Autowired
    FrDao frDao;
    @Autowired
    LogDao logDao;
    @Autowired
    WjgDao wjgDao;
    public List<TUserlogPush> getAllFrPushLogs(String sql,PageInfo pageInfo){
        List<TUserlogPush> tUserlogPushs = this.logDao.queryEntityByPage(sql, TUserlogPush.class, pageInfo);
        return tUserlogPushs;
    }

    public int size(String sql){
        return this.logDao.findCountBySql(sql);
    }

    //文件柜日志
    public List<TUserlogFrwjg> getAllFrWjgLogs(String sql,PageInfo pageInfo){
        List<TUserlogFrwjg> tUserlogFrwjg = this.logDao.queryEntityByPage(sql, TUserlogFrwjg.class, pageInfo);
        return tUserlogFrwjg;
    }


    public boolean addPushDataLog(TUserlogPush log){
        try {
            logDao.save(log);
        }catch (Exception e){
            System.out.println("保存日志异常");
            return false;
        }
        return true;
    }

    public boolean addWjgLog(TUserlogFrwjg wjg){
        try {
            wjgDao.save(wjg);
        }catch (Exception e){
            System.out.println("保存日志异常");
            return false;
        }
        return true;
    }
}
