package com.supermap.wzhy.module.mlk.api;

import com.supermap.wzhy.common.api.BaseController;
import com.supermap.wzhy.data.OpStatus;
import com.supermap.wzhy.data.SysConstant;
import com.supermap.wzhy.entity.TUsers;
import com.supermap.wzhy.module.fr.service.FrPushDataToMlkService;
import com.supermap.wzhy.module.mlk.service.MlkPushDataToFrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 17-2-7.
 */
@Controller
@RequestMapping("/MlkPushDataToFr")
public class MlkPushDataToFrController extends BaseController{

    @Autowired
    FrPushDataToMlkService frService;

    @Autowired
    MlkPushDataToFrService mlService;



    /**
     * 获取名录库数据库表名()
     * @return
     */
    @RequestMapping(value = "/mlkList",method = RequestMethod.GET)
    @ResponseBody
    public List getMlkList(){
        //数据库标识前缀-ML
        String args = "ML_QYXX";
        return frService.getTableNameList(args);
    }


    /**
     * 推送数据
     * @param request
     * @return
     */
    @RequestMapping(value = "/pushData")
    @ResponseBody
    public OpStatus pushData(HttpServletRequest request){
        OpStatus op = new OpStatus();
        TUsers user = (TUsers)request.getSession().getAttribute(SysConstant.CURRENT_USER);
        String username = user.getUserName();

        String tableCode = request.getParameter("tableCode");
        String tableName = request.getParameter("tableName");
        String startTime = request.getParameter("startTime");
        String endTime = request.getParameter("endTime");
        boolean boo = mlService.pushDataToFr(username,tableCode,tableName,startTime,endTime);
        op.setStatus(boo);
        if(boo){
            op.setMsg("导入名录库数据成功！");
        }
        return op;
    }


    /**
     * 删除数据
     * @param request
     * @return
     */
    @RequestMapping(value = "/delData")
    @ResponseBody
    public OpStatus delFrData(HttpServletRequest request){
        return mlService.delMlkData(request.getParameter("date"));
    }

}
