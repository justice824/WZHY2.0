package com.supermap.wzhy.module.sys.api;

import com.supermap.wzhy.data.OpStatus;
import com.supermap.wzhy.module.sys.service.QuartzTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;

/**
 * Created by Sun’fei on 17-2-19.
 */
@Controller
@RequestMapping(value = "/quartz")
public class QuartzTaskController {

    @Autowired
    QuartzTaskService quartzTaskService;

    /**
     * 定时任务-推送法人库数据
     * @param request
     * @return
     */
    @RequestMapping(value = "/frk")
    @ResponseBody
    public OpStatus pushFrk(HttpServletRequest request) throws ParseException {
        OpStatus op = new OpStatus();

        String mark = "frk";
        String jobName = request.getParameter("jobName");
        String time = request.getParameter("time");
        String min = request.getParameter("min");
        String tableName = request.getParameter("tableName");
        String tableCode = request.getParameter("tableCode");
        boolean boo = quartzTaskService.task(request,mark,time,min,tableCode,tableName,jobName);
        op.setStatus(boo);
        return op;
    }


    /**
     * 定时任务-推送名录库数据
     * @param request
     * @return
     */
    @RequestMapping(value = "/mlk")
    @ResponseBody
    public OpStatus pushMlk(HttpServletRequest request) throws ParseException {
        OpStatus op = new OpStatus();

        String mark = "mlk";
        //定时任务参数
        String jobName = request.getParameter("jobName");
        String time = request.getParameter("time");
        String min = request.getParameter("min");
        String tableName = request.getParameter("tableName");
        String tableCode = request.getParameter("tableCode");

        boolean boo = quartzTaskService.task(request,mark,time,min,tableCode,tableName,jobName);
        op.setStatus(boo);
        return op;
    }


    /**
     * 定时任务-推送文件柜数据
     * @param request
     * @return
     */
    @RequestMapping(value = "/wjg")
    @ResponseBody
    public OpStatus pushWjg(HttpServletRequest request) throws ParseException {
        OpStatus op = new OpStatus();

        String mark = "wjg";
        String jobName = request.getParameter("jobName");
        String time = request.getParameter("time");
        String min = request.getParameter("min");
        String tableName = request.getParameter("tableName");
        String tableCode = request.getParameter("tableCode");
        boolean boo = quartzTaskService.task(request,mark,time,min,tableCode,tableName,jobName);
        op.setStatus(boo);
        return op;
    }
}
