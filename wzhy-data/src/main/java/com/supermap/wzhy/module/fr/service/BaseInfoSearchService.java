package com.supermap.wzhy.module.fr.service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.supermap.wzhy.entity.*;
import com.supermap.wzhy.module.fr.dao.FrDao;
import com.supermap.wzhy.module.fr.dao.TConditionDictionaryDao;
import com.supermap.wzhy.module.data.dao.MicroIdenmetaDao;
import com.supermap.wzhy.module.data.dao.MicroTablemetaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Version 2.0
 * Created by sun'f on 2017-09-19.
 * 基本信息查询服务类
 * 看不懂的可以访问
 * http://blog.csdn.net/sun_falls/article/details/78082367
 * 需要扩展的请在注释里注 @expand
 */
@Service
public class BaseInfoSearchService {

    @Autowired
    FrDao frDao ;
    @Autowired
    MicroTablemetaDao microTablemetaDao ;
    @Autowired
    MicroIdenmetaDao microIdenmetaDao ;
    @Autowired
    TConditionDictionaryDao dictionaryDao;

    /**
     * 私有参数
     * CONDITION_TABLE : 数据词典表
     * TABLEMETA ：元数据表
     * IDENMETA　：指标数据表
     * BASEINFO_MARK :　基本信息在元数据表标识
     * IDNENAME　：　指标数据表字段指标名称
     * IDNECODE　：　指标数据表字段指标代码
     * MITMID　：元数据表在指标数据表外键
     */
    private String IDENMETA = GetParamsService.IDENMETA;
    private String IDNENAME = GetParamsService.IDNENAME;
    private String IDNECODE = GetParamsService.IDNECODE;
    private String MITMID = GetParamsService.MITMID;


    /**
     * 得到基本信息表名 2.0
     * @return 表名集合
     */
    public List getGSTable(){
        return  microTablemetaDao.getAllTableName();
    }


    /**
     * 得到基本信息表指标 2.0
     * @return 指标集合
     */
    public List getGSIden(String mitmid){
        String table_sql = "select " + IDNECODE + "," + IDNENAME + " from "
                + IDENMETA + " where " + MITMID + " = ?";
        List<String> list;
        if (mitmid == null || mitmid == "")
            list = microTablemetaDao.query(table_sql,"1");//基本表mitmid从1开始
        else
            list = microTablemetaDao.query(table_sql,mitmid);
        return  list;
    }


    /**
     * 得到条件字典值
     * @return 表名集合
     */
    public List getCondition(){
        List<String> list = dictionaryDao.getCondition();
        if (list.isEmpty()) System.out.println("数据字典为空！请先配置数据字典！");
        return  list;
    }


    /**
     * 模块化查询数据表表格字段名称
     * @param table 数据表唯一标识 mitmid
     * @return List<List<JSON>>
     */
    public List getTitle(String table){
        List tm_list = new ArrayList<>();
        if (table == ""){
            //无参数默认查询第一张表字段名称
            table = "1";
        }
        List<Object []> list = getIdenListByMit(table);
        if (list.isEmpty()){
            return null;
        }else {
            //自定义JSON格式
            List column = new ArrayList<>();
            /**
             * 将列名转换为JSONObject格式
             */
            for (Object [] arr :list){
                Columns columns = new Columns();
                //arr[0] 为IDENCODE 指标表数据库列名
                columns.setField(arr[0].toString());
                //arr[1] 为IDNENAME 指标表显示列名(一般为中文)
                columns.setTitle(arr[1].toString());
                // columns.setWidth(width);
                columns.setAlign("center");

                String jsons = JSON.toJSONString(columns);
                JSONObject object = JSON.parseObject(jsons);

                column.add(object);
            }
            tm_list.add(column);
        }
        return tm_list;
    }


    /**
     * 模块化查询数据
     * 此函数返回 EasyUI 要求的 datagrid 数据对象格式
     * @param table 元数据表唯一标识
     * @param iden  条件指标名称（单个）
     * @param where 查询条件，从数据字典获取(如：=，<,>)
     * @param value 查询阈值
     * @param page  第几页 pageNumber
     * @param rows  页行数 pageSize
     * @return datagrid 数据对象 List<JSON>
     */
    public JSONObject getData(String table, String iden, String where,
                              String value, String page, String rows){
        List result = new ArrayList();
        JSONObject jsonObjects;
        Map se_map = new HashMap();

        //计算分页
        int pageNumber = Integer.valueOf(page);
        int size = Integer.valueOf(rows);
        int first = (pageNumber - 1) * size;

        //默认查询第一个表
        if (table == ""){
            table = "1";
        }

        //查询数据准备对象
        TMicroTablemeta tm = getTableByMitmid(table);
        //查询数据库表名
        String tableCode = tm.getTableName();
        //查询数据库列名
        String iden_code = getIdenByMit(table,0);
        //列名转换为数组
        String [] iden_code_array = iden_code.split(",");
        //数据查询sql
        String data_sql = getDataSql(first,size,tableCode,iden_code);
        //数据查询总数sql
        String count_sql = getDataCountSql(tableCode);

        List<Object []> list;
        //计算查询条件 where
        //条件不为空
        if (value.length() != 0){
            //阈值不为空
            if (where.length() != 0 || where != null){
                int condition = Integer.valueOf(where);
                TConditionDictionary dictionary = getMarkByDictionary(condition);
                data_sql += " where " + iden +" "+ dictionary.getMark() + "'" + value + "'";
                count_sql += " where " + iden +" "+ dictionary.getMark() + "'" + value + "'";
            } else {
                //阈值为空
                data_sql += " where " + iden + " is null";
                count_sql += " where " + iden + " is null";
            }
        }

        //计算count值
        List<String> count_list = frDao.query(count_sql);
        int count = 0;
        if (count_list.size() > 0){
            String count_str = count_list.get(0).toString();
            //System.out.println(count_str);
            count = Integer.valueOf(count_str);
        }else System.out.println("查询总数为空 : " + count_sql);

        //计算data数据
        System.out.println(getNowDate()+" 动态查询数据 ：" + data_sql);
        list = frDao.query(data_sql);
        if (list.isEmpty()){
            System.out.println(getNowDate()+" 查询数据为空！　:  " + data_sql);
            return null;
        }else {
            Map map = new HashMap();
            //遍历查询结果 list
            //将结果转换成JSON对象存放在list中
            for (Object [] arr : list){
                //遍历查询结果字段
                int k;
                for (k = 0; k < arr.length;k++){
                    map.put(iden_code_array[k],arr[k]);
                }
                String json = JSON.toJSONString(map);
                JSONObject jsonObject = JSON.parseObject(json);
                result.add(jsonObject);
            }
            //设置数据总数
            se_map.put("total",count);
            //设置数据集
            se_map.put("rows",result);
        }
        //转换为输出JSON对象
        jsonObjects = JSON.parseObject(JSON.toJSONString(se_map));
        return  jsonObjects;
    }


    /**
     * 工具函数1-1 ： 获取字段名称拼接sql
     * @return
     */
    public String getTitleSql (){
        return "select "+IDNECODE+","+IDNENAME+" from "+IDENMETA+" where "+MITMID+" = ?";
    }


    /**
     * 工具函数1-2 ： 获取数据 拼接sql(分页)
     * @param first 从第几个数据开始
     * @param size  页数大小
     * @param tableCode     表名
     * @param iden_code     字段名
     * @return sql
     */
    public String getDataSql (int first,int size,String tableCode,String iden_code){
        return "select skip "+first+" first "+size+" "+iden_code+" from " + tableCode;
    }


    /**
     * 工具函数1-3 ： 获取数据总数 拼接sql(分页)
     * @param tableCode     数据库表名
     * @return sql          汇总sql
     */
    public String getDataCountSql (String tableCode){
        return "select count(*) from " + tableCode;
    }


    /**
     * 工具函数2-1 : 根据唯一标识得到元数据表对象 (一对一)
     * @param table 唯一标识
     * @return 工商表数据对象（元数据对象）
     */
    public TMicroTablemeta getTableByMitmid(String table){
        TMicroTablemeta tm = microTablemetaDao.findByMitmid(Integer.valueOf(table));
        return tm;
    }

    /**
     * 工具函数2-2 : 根据唯一标识得到指标描述对象 (一对多)
     * @param table 唯一标识
     * @return 指标集合
     */
    public List getIdenListByMit(String table){
        TMicroTablemeta tm = microTablemetaDao.findByMitmid(Integer.valueOf(table));
        String sql = getTitleSql();
        List list = microIdenmetaDao.query(sql,tm.getMitmid());
        if (list.isEmpty()) System.out.println("数据为空 :\n" + sql);
        return list;
    }

    /**
     * 工具函数2-3 : 根据唯一标识得到指标数据字符串
     * @param table 唯一标识
     * @param type 控制器（type=1 指标中文名,type=0指标名称）
     * @return 指标字符串（用于拼接sql）
     */
    public String getIdenByMit(String table,int type){
        String result = "";
        TMicroTablemeta tm = microTablemetaDao.findByMitmid(Integer.valueOf(table));
        String sql = getTitleSql();
        List<Object []> list = microIdenmetaDao.query(sql,tm.getMitmid());
        if (list.isEmpty()) System.out.println("数据为空 :\n" + sql);
        else {
            for (Object [] arr:list){
                result += arr[type].toString()+",";
            }
            result = result.substring(0,result.length()-1);
        }
        return result;
    }


    /**
     * 工具函数3 : 根据唯一标识得到数据字典对象
     * @param id 唯一标识
     * @return  数据字典对象
     */
    public TConditionDictionary getMarkByDictionary(int id){
        return dictionaryDao.findOneByID(id);
    }


    /**
     * 这个还要我写注释？
     * @return
     */
    public String getNowDate(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

}
