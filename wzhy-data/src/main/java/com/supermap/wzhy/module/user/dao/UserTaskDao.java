package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.common.dao.BaseDao;
import com.supermap.wzhy.entity.TUsertask;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 用户任务表
 * Created by W.Qiong on 16-12-27.
 */
public interface UserTaskDao  extends BaseDao<TUsertask, Integer> {

    /**
     * 查找正在运行的任务
     * @param taskType
     * @return
     */
    @Query("select t from TUsertask t where t.taskType=?1 and t.finished=0")
    List<TUsertask> findRunningTask(String taskType);
}
