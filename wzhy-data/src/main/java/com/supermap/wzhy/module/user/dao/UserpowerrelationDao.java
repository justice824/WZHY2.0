package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.entity.TUserpowerrelation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 用户/权限关系dao
 *
 * @author created by W.Qiong on 14-10-21.
 */
public interface UserpowerrelationDao extends JpaRepository<TUserpowerrelation, Integer> {

    /**
     * 获取指定权限分类id和用户id下的用户/权限关系列表
     *
     * @param pcataid
     *            权限分类id
     * @param userid
     *            用户id
     * @return 用户/权限关系列表
     */
    @Query("select t from TUserpowerrelation t where t.TUserpower.TUserpowercatalog.pcataid=?1 and  t.TUsers.userid=?2 order by t.TUserpower.powerid ")
    List<TUserpowerrelation> findPowerByUser(int pcataid, int userid);

}
