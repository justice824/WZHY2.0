package com.supermap.wzhy.module.user.api;

import com.supermap.wzhy.common.annotation.Permission;
import com.supermap.wzhy.common.annotation.Role;
import com.supermap.wzhy.common.api.BaseController;
import com.supermap.wzhy.data.OpStatus;
import com.supermap.wzhy.data.PageInfo;
import com.supermap.wzhy.entity.TUserpower;
import com.supermap.wzhy.entity.TUserpowercatalog;
import com.supermap.wzhy.module.user.service.PowerCatalogsService;
import com.supermap.wzhy.module.user.service.PowerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 14-2-27.
 * 权限分类（以后可合并到权限信息表）
 */
@Controller
@RequestMapping(value = {"/powercatalogs","/service/powercatalogs"})
public class PowerCatalogsController extends BaseController {
    @Autowired
    PowerCatalogsService powerCatalogsService;
    @Autowired
    PowerService powerService;

    /**
     * 创建
     *
     * @param userpowercatalog
     * @return
     */
    @Permission(Role.ADMIN)
    @RequestMapping(value = "", method = RequestMethod.POST)
    @ResponseBody
    public OpStatus createPowercatalog(TUserpowercatalog userpowercatalog) {
        if (powerCatalogsService.save(userpowercatalog)) {
            return getOpStatus(true);
        } else
            return getOpStatus(false);
    }

    /**
     * 按照ID删除(强制删除，将关联表中的数据也删除)
     * 删除的时候需要将关联表中的数据也删除
     * 总共删除UserPowerCatalog，powerRoleRelation，PowerUserRelation三个表中的数据
     *
     * @param powercatalogid
     * @return
     */

    @Transactional
    @Permission(Role.ADMIN)
    @RequestMapping(value = "/{powercatalogid}", method = RequestMethod.DELETE)
    @ResponseBody
    public OpStatus deletePowercatalog(@PathVariable("powercatalogid") int powercatalogid) {
        if (powerCatalogsService.find(powercatalogid) != null) {
            powerCatalogsService.delete(powercatalogid);
            return getOpStatus(true);
        } else
            return getOpStatus(false);

    }

    /**
     * 按照ID删除（不（强制）关联删除）
     * 删除的时候若其分类下的所有权限不被使用，才可以删除
     *
     * @param powercatalogid
     * @return
     */

    @Transactional
    @Permission(Role.ADMIN)
    @RequestMapping(value = "/{powercatalogid}/noforedelete", method = RequestMethod.DELETE)
    @ResponseBody
    public Map<String,Object> deletePowercatalogByidForNoForce(@PathVariable("powercatalogid") int powercatalogid) {
        return  powerCatalogsService.deletePowercatalogByidForNoForce(powercatalogid);
    }

    /**
     * 按照ID查找
     *
     * @param powercatalogid
     * @return
     */
    @Permission(Role.ADMIN)
    @RequestMapping(value = "/{powercatalogid}", method = RequestMethod.GET)
    @ResponseBody
    public TUserpowercatalog getPowercatalog(@PathVariable("powercatalogid") int powercatalogid) {
        TUserpowercatalog userpowercatalog = powerCatalogsService.find(powercatalogid);
        return userpowercatalog == null ?  new TUserpowercatalog() : userpowercatalog;
    }

    /**
     * 查找全部
     *
     * @return
     */
    @Transactional
    @RequestMapping(value = "", method = RequestMethod.GET)
    @ResponseBody
    @Permission(Role.ADMIN)
    public Page<TUserpowercatalog> getAllUserpowercatalogs(PageInfo pageInfo) {
        Page<TUserpowercatalog> userpowercatalogs = powerCatalogsService.findAll(getPageRequest(pageInfo));
        return userpowercatalogs;
    }

    /**
     * 更新
     *
     * @param powercatalogid
     * @param userpowercatalog
     * @return
     */
    @RequestMapping(value = "/{powercatalogid}", method = RequestMethod.PUT)
    @ResponseBody
    @Permission(Role.ADMIN)
    public OpStatus updatePowercatalog(@PathVariable("powercatalogid") int powercatalogid, TUserpowercatalog userpowercatalog) {
        if (powerCatalogsService.update(powercatalogid, userpowercatalog)) {
            return getOpStatus(true);
        } else
            return getOpStatus(false);
    }

    /**
     * 根据powercatalogID找所有Powers
     *
     * @param powercatalogid
     * @return
     */
    @RequestMapping(value = "/{powercatalogid}/powers", method = RequestMethod.GET)
    @ResponseBody
    @Permission(Role.ADMIN)
    public List<TUserpower> getPowersByPowercatalog(@PathVariable("powercatalogid") int powercatalogid) {
        return powerService.findPowersByCatalog(powercatalogid);
    }

}
