package com.supermap.wzhy.module.user.dao;

import com.supermap.wzhy.entity.TUserpower;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * 权限dao
 *
 * @author Created by W.Hao on 14-2-27.
 */
public interface PowerDao extends JpaRepository<TUserpower, Integer> {

    /**
     * 根据用户角色id，找到角色下权限列表信息
     *
     * <p style="color:red;">
     * 说明：<br/>
     * 对TRolepowerrelation(角色/权限关系表)表的操作,<br/>
     * 从中获取指定角色下的所有权限
     * </p>
     * @param roleid
     * 			角色id
     * @return 权限列表信息
     */
    @Query("select t.TUserpower from TRolepowerrelation t where t.TUserrole.roleid=:id")
    List<TUserpower> findUserpowerListByRoleid(@Param("id") int roleid);

    /**
     * 根据用户id，找到用户下的权限列表信息
     * <p style="color:red;">
     * 说明：<br/>
     * 对TUserpowerrelation(用户/权限关系表)表的操作,<br/>
     * 根据用户id，找到用户下的权限列表信息
     * </p>
     * @param userid
     * 			用户id
     * @return 权限列表信息
     */
    @Query("select t.TUserpower from TUserpowerrelation t where t.TUsers.userid =:id")
    List<TUserpower> findUserpowerListByUserid(@Param("id") int userid);

    /**
     * 获取指定权限分类id和用户id下的系统权限列表
     * <p style="color:red;">
     * 说明：<br/>
     * 对TUserpowerrelation(用户/权限关系表)表的操作,<br/>
     * 从中获取指定权限分类id和用户id下的系统权限列表
     * </p>
     * @param pcataid
     * 			权限分类id
     * @param userid
     * 			用户id
     * @return 权限列表信息
     */
    @Query("select t.TUserpower from TUserpowerrelation t where t.TUserpower.TUserpowercatalog.pcataid=?1 and t.TUserpower.status>0 and t.TUsers.userid=?2 order by t.TUserpower.powerid ")
    List<TUserpower> findPowerByUserAndCatalog(int pcataid, int userid);

    /**
     * 根据用户角色(一个或者多个)id，找到权限列表信息(去重复)
     * <p style="color:red;">
     * 说明：<br/>
     * 对TRolepowerrelation(角色/权限关系表)表的操作,<br/>
     * 从中指定用户角色(一个或者多个)id，找到权限列表信息(去重复)
     * </p>
     * @param roleids
     * 			角色(一个或者多个)id
     * @return 权限列表信息
     */
    @Query("select distinct t.TUserpower from TRolepowerrelation t where t.TUserrole.roleid in ?1 and t.TUserpower.status>0 ")
    List<TUserpower> findUserpowerListByRoleids(int[] roleids);

    /**
     * 获取多个角色下的某类系统权限
     * <p style="color:red;">
     * 说明：<br/>
     * 对TRolepowerrelation(角色/权限关系表)表的操作,<br/>
     * 从中指定用户角色(一个或者多个)id和权限分类id，找到权限列表信息(去重复)
     * </p>
     * @param roleids
     * 			角色(一个或者多个)id
     * @param pcataid
     * 			权限分类id
     * @return 权限列表信息
     */
    @Query("select t.TUserpower from TRolepowerrelation t where t.TUserrole.roleid in ?1 and t.TUserpower.TUserpowercatalog.pcataid=?2 order by t.TUserpower.powerid")
    List<TUserpower> findRolePowerByPar(int[] roleids, int pcataid);


    /**
     * 根据权限分类id，找到权限信息
     *
     * @param pcataid
     * 			权限分类id
     *
     * @return 权限列表信息
     */
    @Query("select t from TUserpower t where t.TUserpowercatalog.pcataid=:id and t.status >0")
    List<TUserpower> findPowerByCatalog(@Param("id") int pcataid);

    /**
     * 根据权限分类id和权限父id，找到用户权限信息
     *
     * @param pcataid
     * 			权限分类id
     * @param parid
     * 		  	权限父id
     * @return 权限列表信息
     */
    @Query("select t from TUserpower t where t.TUserpowercatalog.pcataid=?1 and t.parid=?2 and t.status>0")
    List<TUserpower> findByParid(int pcataid, int parid);
}
