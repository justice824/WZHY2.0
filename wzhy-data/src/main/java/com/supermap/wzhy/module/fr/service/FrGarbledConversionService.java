package com.supermap.wzhy.module.fr.service;

import com.supermap.wzhy.data.ConfigHelper;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.springframework.stereotype.Service;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 检测器服务类(检测XML中特殊字符乱码)
 * Created by Sunfei on 17-01-19.
 */
@Service
public class FrGarbledConversionService {

    /**
     * 检测器
     * @return
     */
    public List conversion(){
        String path = ConfigHelper.get("jcpath");
        System.out.println("检测文件路径 : "+path);
        File file = new File(path);
        String [] str = file.list();
        int count = 0;
        List list = new ArrayList();
        //返回值集合头部
        list.add("错误文件列表");
        //返回值
        for(String name:str){
            SAXReader reader = new SAXReader();
            try {
                reader.read(new File(path + "\\" + name));
            }catch (DocumentException e){
                System.out.println("错误文件: "+name);
                list.add(name);
                count++;
            }
        }
        System.out.println("错误文件率 : "+count+"/"+str.length);
        DecimalFormat df = new DecimalFormat("0.00%");
        double dou = Double.valueOf(count)/Double.valueOf(str.length);
        //返回值集合尾部
        list.add(count+"/"+str.length+" 错误文件率 :  "+df.format(dou));
        return  list;
    }
}