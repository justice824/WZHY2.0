package com.supermap.wzhy.module.user.api;

import com.supermap.wzhy.common.api.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * 用户权限API
 *
 * @author Created by W.Hao on 14-1-21.
 */
@Controller
@RequestMapping(value = {"/service"})
public class PermissionController extends BaseController{

}
