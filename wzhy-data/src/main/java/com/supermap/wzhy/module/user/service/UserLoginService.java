package com.supermap.wzhy.module.user.service;

import com.supermap.wzhy.data.MessagePrintUtil;
import com.supermap.wzhy.entity.TUsers;
import com.supermap.wzhy.module.user.dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NonUniqueResultException;

/**
 * 用户登录server
 *
 * @author Created by W.Hao on 14-1-20.
 */
@Service
public class UserLoginService {

    /** 用户登录dao */
    @Autowired
    private UserDao userDao;

    /**
     * 根据用户名和密码检查用户是否存在
     *
     * @param username
     *            用户名（登录名）
     * @param password
     *            密码
     * @return 用户对象,若未找到匹配的用户则返回null
     */
    public TUsers checkUser(String username, String password) {
        TUsers user = null;
        try {
            user = userDao.findByUserNameAndPassword(username, password);
        }catch (NonUniqueResultException  ue){
            MessagePrintUtil.printException("存在多个用户名和密码相同的用户");
        }catch (Exception e) {
            MessagePrintUtil.printException("用户密码输入错误、或连接数据库失败!");
            e.printStackTrace();
        }
        return user;
    }
}

