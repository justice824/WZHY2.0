package com.supermap.wzhy.module.fr.service;

import com.supermap.wzhy.data.PageInfo;
import com.supermap.wzhy.entity.TUserlogPush;
import com.supermap.wzhy.module.fr.dao.FrDao;
import com.supermap.wzhy.module.fr.dao.FrLogDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by hezhiyao on 2017/1/3.
 */
@Service
public class FrPushLogService {
    @Autowired
    FrDao frDao;
    @Autowired
    FrLogDao frLogDao;

    public List<TUserlogPush> getAll(String sql,PageInfo pageInfo){
        return this.frLogDao.queryByPage(sql,pageInfo);
    }

    public int size(String sql){
        return this.frLogDao.findCountBySql(sql);
    }
}
