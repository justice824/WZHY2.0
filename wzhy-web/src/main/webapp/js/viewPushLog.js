$(function(){
    var ml = "2";

    getMlLogList(ml,"");

    //动态赋值时间段
    var dateTemp = SGIS.Util.template('<li fromDate="{{key}}"><a href="#">{{value}}</a></li>');
    var dateBox = $(".time-period");
    getDatePeriod(dateTemp, dateBox);
    //下拉列表填值
    $(".time-period li").on("click",function(){
        var val = $(this).find("a").html();
        var key = $(this).attr("fromDate");
        $(this).parents(".input-group").find("input").val(val);
        $(this).parents(".input-group").find("input").attr("fromDate",key);
    });
    //查询日志（根据用户名和期限）
    $("#searchLog").on('click',function(){
        var key = "";
        if($("#searchUser").val() != null && $("#searchUser").val() != ""){
            key += "AND t.username like '%" + $("#searchUser").val() + "%'";
        }
        //select t.*, t.rowid from T_USERLOG_PUSH t where t.starttime Between to_date('2010/5/4 10:00:00','yyyy/mm/dd hh24:mi:ss')
        //And to_date('2017/1/6 15:00:00','yyyy/mm/dd hh24:mi:ss')
        if($("#searchDeadline").val() != null && $("#searchDeadline").val() != ""){
            var fromDate = $("#searchDeadline").attr("fromDate");
            fromDate = fromDate.replace("/","-");
            fromDate = fromDate.replace("/","-");
            var date = new Date();
            var curDate = date.getFullYear() + "-" + (parseInt(date.getMonth())+1) + "-" + date.getDate();
            if(fromDate != "0"){
                key += "AND t.endtime >= to_date('" + fromDate + "','%Y-%m-%d') And t.endtime <= to_date('" + curDate + "','%Y-%m-%d')";
            }
        }
        getMlLogList(ml,key);
    });
    $('#searchUser').bind('keydown', function (event) {
        if (event.keyCode == "13") {
            $("#searchLog").trigger('click');
        }
    });
    $('#searchDeadline').bind('keydown', function (event) {
        if (event.keyCode == "13") {
            $("#searchLog").trigger('click');
        }
    });

    function getDatePeriod(temple, box){
        var mydate = new Date();
        var datePeriod = Data.config(mydate);
        for(var i=0; i<datePeriod.length; i++){
            $(temple(datePeriod[i])).appendTo(box);
        }
    }

    function getMlLogList(type,key){
        getMlPushLog($.PageInfo(1, 10),type,key);
        getMlPushLogPages(type,key);
    }

    //查询推送日志列表
    function getMlPushLog (pageInfo,type,key){
        $("#mlData").html("");
        var mlBox = $("#mlData");
        var mlDataTemp = SGIS.Util.template('<tr>'+
            '<td>{{username}}</td>'+
            '<td>{{starttime}}</td>'+
            '<td>{{endtime}}</td>'+
            '<td>{{successrecord}}</td>'+
            '<td>{{tablename}}</td>'+
            '</tr>');
        SGIS.API.post('log/pushLog/all').data({"type":type,"key":key}).data(JSON.stringify(pageInfo)).json(function (re) {
            if(re){
                for(var i=0; i<re.length; i++){
                    $(mlDataTemp(re[i])).appendTo(mlBox);
                }
            }
        });
    }
    function getMlPushLogPages (type,key){
        SGIS.API.post('log/pushLog/size').data({"type":type,"key":key}).text(function (size) {
            var paging = $.Paging("mlPushLog-paging");
            paging.render({
                count: size,
                style: 2,
                pageSize: 10/*,
                 showPageNum: 15*/
            });
            var pageClick = function (pageNumb) {
                getMlPushLog($.PageInfo(pageNumb, 10),type,key);
            };
            paging.addClick(pageClick);
        });
    }

});

