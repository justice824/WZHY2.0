$(function(){
    var fr = "1";//数据推送
    var wjg = "2";//文件柜
    var switchType = "1"; //tab标签切换

    getFrLogList(fr,"");
    //动态赋值时间段
    var dateTemp = SGIS.Util.template('<li fromDate="{{key}}"><a href="#">{{value}}</a></li>');
    var dateBox = $(".time-period");
    getDatePeriod(dateTemp, dateBox);
    //下拉列表填值
    $(".time-period li").on("click",function(){
        var val = $(this).find("a").html();
        var key = $(this).attr("fromDate");
        $(this).parents(".input-group").find("input").val(val);
        $(this).parents(".input-group").find("input").attr("fromDate",key);
    });
    /*-------------------------------------------------------点击查询事件-----------------------------------------------------*/
    //查询日志（根据用户名和期限）
    $(".searchLog").on('click',function(){
        var key = "";
        var $_this = $(this).parents(".tab-pane");
        if($_this.find(".userData").val() != null && $_this.find(".userData").val() != ""){
            key += "AND t.username like '%" + $_this.find(".userData").val() + "%'";
        }
        //select t.*, t.rowid from T_USERLOG_PUSH t where t.starttime Between to_date('2010/5/4 10:00:00','yyyy/mm/dd hh24:mi:ss')
        //And to_date('2017/1/6 15:00:00','yyyy/mm/dd hh24:mi:ss')
        if($_this.find(".searchTime").val() != null && $_this.find(".searchTime").val() != ""){
            var fromDate = $_this.find(".searchTime").attr("fromDate");
            fromDate = fromDate.replace("/","-");
            fromDate = fromDate.replace("/","-");
            var date = new Date();
            var curDate = date.getFullYear() + "-" + (parseInt(date.getMonth())+1) + "-" + date.getDate();
            if(fromDate != "0"){
                key += "AND t.endtime >= to_date('" + fromDate + "','%Y-%m-%d') And t.endtime <= to_date('" + curDate + "','%Y-%m-%d')";
            }
        }
        if(switchType == "1"){
            getFrLogList(fr,key);
        }else if(switchType == "2"){
            getFrWjgLogList(key);
        }
    });

    $("#myTab li").on("click",function(){
        var index = $(this).index()+1;
        switchType = index;
        if(switchType == "1"){
            $("#frData").html("");
            getFrLogList(fr,"");
        }else if(switchType == "2"){
            $("#frWjgData").html("");
            getFrWjgLogList("");
        }
    });
    /*-------------------------------------------------------关联键盘回车事件-----------------------------------------------------*/
    $('.userData').bind('keydown', function (event) {
        var $_this = $(this).parents(".tab-pane");
        if (event.keyCode == "13") {
            $_this.find(".searchLog").trigger('click');
        }
    });
    $('.searchTime').bind('keydown', function (event) {
        var $_this = $(this).parents(".tab-pane");
        if (event.keyCode == "13") {
            $_this.find(".searchLog").trigger('click');
        }
    });

    /*-------------------------------------------------------查询列表函数-----------------------------------------------------*/
    function getDatePeriod(temple, box){
        var mydate = new Date();
        var datePeriod = Data.config(mydate);
        for(var i=0; i<datePeriod.length; i++){
            $(temple(datePeriod[i])).appendTo(box);
        }
    }
    //查询数据推送日志
    function getFrLogList(type,key){
        getFrPushLog($.PageInfo(1, 10),type,key);
        getFrPushLogPages(type,key);
    }
    //查询文件柜日志
    function getFrWjgLogList(key){
        getFrWjgLog($.PageInfo(1, 10),key);
        getFrWjgLogPages(key);
    }
    //查询数据推送日志列表
    function getFrPushLog (pageInfo,type,key){
        $("#frData").html("");
        var frBox = $("#frData");
        var frDataTemp = SGIS.Util.template('<tr>'+
            '<td>{{username}}</td>'+
            '<td>{{starttime}}</td>'+
            '<td>{{endtime}}</td>'+
            '<td>{{successrecord}}</td>'+
            '<td>{{tablename}}</td>'+
            '</tr>');
        SGIS.API.post('log/pushLog/all').data({"type":type,"key":key}).data(JSON.stringify(pageInfo)).json(function (re) {
            if(re){
                for(var i=0; i<re.length; i++){
                    $(frDataTemp(re[i])).appendTo(frBox);
                }
            }
        });
    }
    function getFrPushLogPages (type,key){
        //alert(type);
        SGIS.API.post('log/pushLog/size').data({"type":type,"key":key}).text(function (size) {
            var paging = $.Paging("frPushLog-paging");
            paging.render({
                count: size,
                style: 2,
                pageSize: 10/*,
                 showPageNum: 15*/
            });
            var pageClick = function (pageNumb) {
                getFrPushLog($.PageInfo(pageNumb, 10),type,key);
            };
            paging.addClick(pageClick);
        });
    }
    //查询文件柜日志列表
    function getFrWjgLog (pageInfo,key){
        $("#frWjgData").html("");
        var frWjgBox = $("#frWjgData");
        var frWjgDataTemp = SGIS.Util.template('<tr>'+
            '<td>{{username}}</td>'+
            '<td>{{starttime}}</td>'+
            '<td>{{endtime}}</td>'+
            '<td>{{unitrecord}}</td>'+
            '</tr>');
        SGIS.API.post('log/frWjgLog/all').data({"key":key}).data(JSON.stringify(pageInfo)).json(function (re) {
            if(re){
                for(var i=0; i<re.length; i++){
                    $(frWjgDataTemp(re[i])).appendTo(frWjgBox);
                }
            }
        });
    }
    function getFrWjgLogPages (key){
        SGIS.API.post('log/frWjgLog/size').data({"key":key}).text(function (size) {
            var paging = $.Paging("frWjgLog-paging");
            paging.render({
                count: size,
                style: 2,
                pageSize: 10/*,
                 showPageNum: 15*/
            });
            var pageClick = function (pageNumb) {
                getFrWjgLog($.PageInfo(pageNumb, 10),key);
            };
            paging.addClick(pageClick);
        });
    }

});

