var _manager = 1, _mlk = 2, _frk = 3;
var nav = {3:"frk",2:"mlk",1:"manager"};
$(function(){
    var documentHeight = $(document).height(); //浏览器当前窗口文档的高度
    $("#page-wrapper").css("height",documentHeight-100+"px");
    //判断是否登录
    isLogin();
    //切换开关
    if($("input").hasClass("switch-btn")){
        $(".switch-btn").bootstrapSwitch();
    }
    //注销（退出登陆）
    $("#logout").bind("click",function(){
        logout();
    })


});

function isLogin(){
    SGIS.API.get("oAuth/isLogin").json(function(re){
        if(re.status){
            var username = re.user.userName;
            $("i.fa-user").after('  '+username+"  ");
            //根据角色，分配权限
            var role = re.user.sys_role;
            if(role != _manager){
                $.each(nav,function(key,val){
                    if(key == role){
                        $("#" + val).show();
                    }else{
                        $("#" + val).hide();
                    }
                });
            }
        }else{
            SGIS.UI.alert("请先登录！");
            window.location.href = "../web/login.html";
        }
    })
}
function logout(){
    SGIS.API.get("oAuth/logout").json(function(re){
        if(re == "ok"){
            location.href =SGIS.Util.getBaseUrl() +  "/web/login.html";   //退出
        }
    })
}

/*----------------------------------------------------自动填充表单---------------------------------------------------*/
/*
 *obj是一个对象
 *如：{key1:value1,key2:value2...}
 */
function loadData(obj,form){
    var key,value,tagName,type,arr;
    for(x in obj){
        key = x;
        value = obj[x];

        form.find("[name='"+key+"'],[name='"+key+"[]']").each(function(){
            tagName = $(this)[0].tagName;
            type = $(this).attr('type');
            if(tagName=='INPUT'){
                if(type=='radio'){
                    $(this).attr('checked',$(this).val()==value);
                }else if(type=='checkbox'){
                    arr = value.split(',');
                    for(var i =0;i<arr.length;i++){
                        if($(this).val()==arr[i]){
                            $(this).attr('checked',true);
                            break;
                        }
                    }
                }else{
                    $(this).val(value);
                }
            }else if(tagName=='SELECT' || tagName=='TEXTAREA'){
                $(this).val(value);
            }

        });
    }
}
/*
 *将form中的值转换为键值对
 * 如：{Name:'摘取天上星',position:'IT技术'}
 *ps:注意将同名的放在一个数组里
 */
function getFormJson(form) {
    var o = {};
    var a = form.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
}

