var Data = Data || {};
//设定时间段
Data.config = function(date){
    var fr_data_val1 = "当天";
    var key1 = Data.addDay( 0, date);
    var fr_data_val2 = "最近一周";
    var key2 = Data.addDay( 7, date);
    var fr_data_val3 = "最近一个月";
    var key3 = Data.addDay( 30, date);
    var fr_data_val4 = "最近一年";
    var key4 = Data.addDay( 365, date);
    var fr_data_val5 = "所有";
    var key5 = "0";
    var date = [
        {
            key:key1,
            value:fr_data_val1
        },
        {
            key:key2,
            value:fr_data_val2

        },
        {
            key:key3,
            value:fr_data_val3
        },
        {
            key:key4,
            value:fr_data_val4
        },
        {
            key:key5,
            value:fr_data_val5
        }
    ]
    return date;
}

Data.showData = function(interval){
    var mydate = new Date();
    Data.showData(interval,mydate.toLocaleString());
}

//现有时间加上天数之后的时间
Data.addDay =  function(dayNumber, date) {
    date = date ? date : new Date();
    var ms = dayNumber * (1000 * 60 * 60 * 24)
    var newDate = new Date(date.getTime() - ms);
    var showDate = newDate.getFullYear() + "/" + (parseInt(newDate.getMonth())+1) + "/" + newDate.getDate();
    return showDate;
}