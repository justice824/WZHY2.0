/**
 * 导入excel
 */
define(function(require,exports,module){

    var File = function(container,iframe,forms,callback,supportFormat){
        this.file = $("#"+container);
        this.onload = callback||null;
        this.format = supportFormat||"xls,xlsx";

        this.iframe = "#"+iframe;
        this.forms = "#"+forms;
        this.originName = "";
        this.uploadName = "";

        this.check();
        this.upload();
    }

    File.prototype = {

        upload:function(){
            var that = this;
            var form = $(that.forms);
            form.attr("action",SGIS.API.getURL("common/upload"));
            SGIS.UI.addLoadingBar("正在上传......");
            form.submit();
            SGIS.UI.clearLoadingBar();

            $(that.iframe).one("load",function(){
                SGIS.UI.clearLoadingBar();
                var c = $(this).contents().find("body");
                var re = $.trim(c.text());
                if(re){
                    re = eval("("+re+")");
                    that._uploadFileName = re.fileName;
                    re.originName = that.originName;
                    that.onload && that.onload(re);
                }
                c.empty();
            })

        },
        check:function(){
            var path = this.file.val();
            if(!path){
                return false;
            }
            var info = getFileInfo(path);
            this.originName = info.fileName;
            var extendName = info.extendName.toUpperCase();
            var split = this.format.split(",");
            for(var i = 0;i < split.length;i++){
                if(extendName == split[i].toUpperCase()){
                    return true;
                }
            }

            function getFileInfo(path){
                if(!path){
                    return null;
                }
                var extendName = path.substring(path.lastIndexOf(".")+1);
                var fileName = path.substring(path.lastIndexOf("\\")+1);
                var json = {
                    extendName:extendName,
                    fileName:fileName
                }
                return json;
            }
        },
        getFileName:function(){
            return this.uploadName;
        }

    }

    Object.defineProperty(File.prototype,"constructor",{
        enumerable:false,
        constructor:File
    })

    return File;
});

var uploadFile = "";
function callback_sx(re){
    var address = $("#import-sxqy").val();
    if(!address){
        SGIS.UI.alert("未选择文件");
    }else{
        uploadFile = re.fileName;
        SGIS.API.get("mlk/fillAndImportExcel/importExcel")
            .data({fileName:re.fileName,sheet:"sxqy"})
            .json(function(d){
            if(d){
                SGIS.UI.alert(d.msg);
            }
        })
    }
}

function callback_wf(re){
    var address = $("#import-wfcf").val();
    if(!address){
        SGIS.UI.alert("未选择文件");
    }else{
        uploadFile = re.fileName;
        SGIS.API.get("mlk/fillAndImportExcel/importExcel")
            .data({fileName:re.fileName,sheet:"wfcf"})
            .json(function(d){
                if(d){
                    SGIS.UI.alert(d.msg);
                }
            })
    }
}