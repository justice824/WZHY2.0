$(function(){
    init();
});

var init = function(){
    //初始化数据推送列表
    $("body").ready(function(){
        var html = "";
        SGIS.API.get("MlkPushDataToFr/mlkList").json(function(re){
            for(var i = 0 ;i < re.length;i++){
                var tbody_tr =
                    " <tr>"+
                        "<td class='text-indent'>" +
                        "<input name='check-box' type='checkbox' checked='true'>" +
                        "<input name='table-name' type='hidden' value='"+re[i][3]+"'>" + //法人库表名 re[i][3]
                        "<input name='type' type='hidden' value='"+re[i][24]+"'>" + //法人库类型 re[i][24]
                        "</td>"+
                        "<td>"+re[i][6]+"</td>"+ //法人库表名称 re[i][6]
                        "</tr>";
                html+=tbody_tr;
            }
            $("#push-data-table").html(html);
        });
    });

    $("#importDate").val(getNowFormatDate());

    setdate();

    checkAll();
    startPushData();
    setOnTimePush();
    delFrData();
	//isLogin();
};

/**
 * 全选反选
 */
function checkAll(){
    $("#check-all").bind("click",function(){
        $("input[name='check-box']").each(function(){
            if($(this).prop("checked")){
                $(this).prop("checked",false);
            }else{
                $(this).prop("checked",true);
            }
        });

    });
}


/**
 * 设置定时推送
 */
function setOnTimePush(){

    var job_name = "PUSHTOFRKJOB";

    $("#start-quartz").bind("click",function(){quartz()});

    function quartz(){
        var switchs = $("#switchs").bootstrapSwitch("state");
        // var date = $("#importDate").val();
        var hour = $("#hour").val();
        var min = $("#min").val();
        var table = getPushTableName();
        var  code = getPushTableCode();
        //alert(code);
        if(table == ""){
            SGIS.UI.alert("请选择定时数据表!");
        }else if(switchs == false){
            SGIS.UI.alert("请打开定时开关!");
        }else if(hour == ""){
            SGIS.UI.alert("请选择精准时间!");
        }else if(hour < 0 || hour > 24){
            SGIS.UI.alert("请选择正确的精准时间!");
        }else{
            if(switchs == true){
                SGIS.API.get("quartz/frk")
                    .data({
                        jobName:job_name,
                        time:hour,
                        min:min,
                        tableName:table,
                        tableCode:code
                    }).json(function(re){
                        SGIS.UI.alert(re.msg);
                    })
            }
        }

    }

}

function delFrData(){
    $("#del-but").bind("click",function(){
        var delDate = $("#del-date").val();
        //alert(delDate);
        SGIS.API.get("MlkPushDataToFr/delData")
            .data({
                date:delDate
            })
            .json(function(re){
                SGIS.UI.alert(re.msg);
            })
    })


}


function getPushTableCode(){
    var count = 0;
    var tableName = "";
    $("input[name='check-box']").each(function(){
        var checked = $(this).prop("checked");
        if(checked == true){
            count = count + 1;
            tableName += $(this).parent().next().html().toString()+",";
        }
    });
    if(count > 0){
        tableName = tableName.substring(0,tableName.length-1);
    }else{
        tableName = "";
    }
    return tableName;
}

function getPushTableName(){
    var count = 0;
    var tableName = "";
    $("input[name='check-box']").each(function(){
        var checked = $(this).prop("checked");
        if(checked == true){
            count = count + 1;
            tableName += $(this).next().val().toString()+",";
        }
    });
    if(count > 0){
        tableName = tableName.substring(0,tableName.length-1);
    }else{
        tableName = "";
    }
    return tableName;
}

/**
 * 开始推送
 */
function startPushData(){
    $("#start-push").bind("click",function(){validata()});
    //验证是否非法提交
    function validata(){
        var count = 0;
        var table = "";
        var tableName = "";
        var start_time = $("#start-time").val();
        var end_time = $("#end-time").val();
        $("input[name='check-box']").each(function(){
            var checked = $(this).prop("checked");
            if(checked == true){
                count = count + 1;
                table += $(this).next().val().toString()+",";
                tableName += $(this).parent().next().html().toString()+",";
            }
        });
        if(count == 0){
            SGIS.UI.alert("请选择推送数据表！");
        }else{
            table = table.substring(0,table.length-1);
            tableName = tableName.substring(0,tableName.length-1);
            //alert(table+" : "+start_time+" > "+end_time);
            SGIS.API.get("MlkPushDataToFr/pushData")
                .data({
                    tableCode:table,
                    tableName:tableName,
                    startTime:start_time,
                    endTime:end_time
                })
                .json(function(re){
                    SGIS.UI.alert(re.msg);
                })
        }
        return false;
    }
}

function setdate(){
    var today = getNowFormatDate();
    var yesteday = getyesteday();

    $("#start-time").val(yesteday);
    $("#end-time").val(today);
    //$("#del-date").val(today);
}

function getNowFormatDate() {
    var date = new Date();
    var seperator = "-";
    var month = date.getMonth() + 1;
    if(month < 9){
        month = "0" + month;
    }
    var strDate = date.getDate();
    if(strDate < 9 && strDate > 0){
        strDate = "0" + strDate;
    }
    return date.getFullYear() + seperator + month + seperator + strDate;
}

function getyesteday() {
    var date = new Date();
    date.setTime(date.getTime() - 24*60*60*1000);
    var seperator = "-";
    var month = date.getMonth() + 1;
    if(month < 9){
        month = "0" + month;
    }
    var strDate = date.getDate();
    if(strDate < 9 && strDate > 0){
        strDate = "0" + strDate;
    }
    return date.getFullYear() + seperator + month + seperator + strDate;
}