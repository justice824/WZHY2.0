<%@ page language="java" pageEncoding="UTF-8"%>
<%
    String _path = request.getContextPath() + "/";
    String _basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
    String BASE = "/web/lib/";
%>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="renderer" content="webkit"> <%-- 兼容360双核浏览器 --%>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="UTF-8">
<!-- Javascript&CSS Loader -->
<script type="text/javascript" src="<%=_basePath + BASE %>boot/SGIS.Loader.js"></script>
<script type="text/javascript" src="<%=_basePath + BASE %>page/SGIS.Base.js"></script>
<script type="text/javascript" src="<%=_basePath + BASE %>sea/sea.js"></script>
<script type="text/javascript">
	SGIS.Loader.basePath = "<%=_basePath + BASE %>web/";
    SGIS.URL.SEAJS_BASE = "<%=_basePath + _path %>lib/";
    seajs.config({
        base: SGIS.URL.SEAJS_BASE      // sea.js用到的基础路径
        ,map:[
            ['.json', '.json?t=' + new Date().getTime()]
            ,['.js', '.js?t=' + new Date().getTime()],
            ['.css', '.css?t=' + new Date().getTime()]
        ]
    });



</script>
<!-- Javascript&CSS Loader -->
