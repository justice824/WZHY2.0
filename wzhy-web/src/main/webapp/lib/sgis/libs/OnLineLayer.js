/**
 * Created by jinn on 2015/6/8.
 */

/**
 * 通用在线地图服务类
 * Class: SuperMap.Layer.OnLineLayer
 * Inherits from: <SuperMap.Layer.CanvasLayer>
 */
SuperMap.Layer.OnLineLayer = SuperMap.Class(SuperMap.CanvasLayer,{

    name:"OnLineLayer",
    url:"http://10.16.157.196:8080/servicemanager/wmtsproxy/1187?service=WMTS&request=GetTile&version=1.0.0&layer=1187&style=default&format=image/PNG&TileMatrixSet=CustomCRS4326Scale_1187&TileMatrix=${TileMatrix}&TileRow=${TileRow}&TileCol=${TileCol}&key=123456789456123456",
   

   zOffset:2,
	

   initialize: function(_name,_url,_option) {
        this.name = _name || name;
		
        var options = _option || {
            projection: "EPSG:4326",
			visibility: true
           ,numZoomLevels: 21
        };	
		
        this.url = _url || url;
		
		this.url = this.proxyUrl(this.url);
        SuperMap.CanvasLayer.prototype.initialize.apply(this,[this.name,this.url,{},options] );
    },

    /**
     * Method: clone
     */
    clone: function(obj) {
        if (obj == null) {
            obj = new SuperMap.Layer.OnLineLayer(
                this.name);
        }
        obj = SuperMap.CanvasLayer.prototype.clone.apply(this, [obj]);
        return obj;
    },

    /**
     * APIMethod: destroy
     * 释放资源。
     */
    destroy: function () {
        var me = this;
        me.name = null;
        me.url = null;
        SuperMap.CanvasLayer.prototype.destroy.apply(me, arguments);
    },

	
   
    /**
     * Method: getTileUrl
     * 获取瓦片的URL。
     *
     * Parameters:
     * xyz - {Object} 一组键值对，表示瓦片X, Y, Z方向上的索引。
     *
     * Returns
     * {String} 瓦片的 URL 。
     */
    getTileUrl: function (xyz) {
	    var temp = arguments;
        var me = this,  url;
        url = me.url;
		var x,y,z ;
		x = xyz.x;
		y = xyz.y;
		//z = xyz.z-me.zOffset;	
        z = xyz.z;
            
        url= SuperMap.String.format(url, {     
            z:z,		
            y:y,
            x:x       
        });
 	//console.log(url);		
        return  url;
    },
	
	
    setMap: function(map) {
        SuperMap.CanvasLayer.prototype.setMap.apply(this, [map]);
        var proCode = null;
        var proj = this.projection||map.projection;
		
		//this.setLayerParam(proj);
    },
	
	
  
  
     setLayerParam:function(projection){      
      
        if(projection=="EPSG:4326"){     
                                         
            var minX = 107.397733654 ;
            var minY = 33.6308004457 ;
            var maxX= 109.9996266854 ;
            var maxY= 35.0219435341 ;         

           var resolutions = [		
                     0.002993214599038876292214947230816689,				
				     0.001496607299519438146107473615408344,
					 0.0007483036497597190730537368077041719,
					 0.0002993214599038876292214947230816689,
					 0.0001496607299519438146107473615408344,
					 0.00007483036497597190730537368077041719,
					 0.00002993214599038876292214947230816689,
					 0.00001496607299519438146107473615408344,
					 0.000005986429198077752584429894461633377,
					 0.000002993214599038876292214947230816689					 
				];
       //     for(var i=resStart;i<=resLen;i++){
         //       resolutions.push(1.40625/2/Math.pow(2,i));
          //  }
		  
		

         //   this.units = "degree";
        //    this.projection = new SuperMap.Projection("EPSG:4326");

            this.maxExtent=new SuperMap.Bounds(
                minX, minY, maxX, maxY
            );
          //  this.tileOrigin = new SuperMap.LonLat(107.397733654,35.0219435341);  //矩阵原点
			 this.tileOrigin = new SuperMap.LonLat(107.57528,34.8209);  //矩阵原点   (X+ 右    Y+ 上)
            this.resolutions = resolutions;
			
			this.isBaseLayer = true;
        }
       
       
    },
	
	/*
	 getURL: function(c) {
		var b = this,
			a;
		c = b.adjustBounds(c);
		a = b.getXYZ(c);
		return b.getTileUrl(a)
	},
	getXYZ: function(b) {
		var f = this,
			h, g, e, c = f.map,
			d = c.getResolution(),
			a = f.getTileOrigin(),
			i = f.tileSize;
		h = Math.round((b.left - a.lon) / (d * i.w));
		g = Math.round((a.lat - b.top) / (d * i.h));
		e = c.getZoom();
		return {
			x: h,
			y: g,
			z: e
		}
	},
	
	*/
	
	
	 /**
     * 将地址转化为代理服务地址
     * @param url
     * @returns {string}
     */
    proxyUrl : function (url) {
        var purl = this.getBaseUrl() +  "/web/proxy/tile?url=";
        var _param = url;  //this.url;
        _param = this.replaceChar(_param);
        return purl + _param;
    },


    getBaseUrl : function(){
        var loc = window.location;
        if (loc.origin){
            return loc.origin;
        }
        return loc.protocol+"//"+loc.host;
    },

    replaceChar:function (url) {
        url = url.replace(/&/g,"$");
        url= url.replace(/\?/g,"!");
        return url;
    },
	
    CLASS_NAME: "SuperMap.Layer.OnLineLayer"
});
