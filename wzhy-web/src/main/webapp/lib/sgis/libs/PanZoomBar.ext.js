SuperMap.Control.PanZoomBarExt = SuperMap.Class(SuperMap.Control, {
    slideFactor: 50,
    slideRatio: null,
    buttons: null,
    zoomStopWidth: 13,
    zoomStopHeight: 11,
    slider: null,
    szTemp_zoombar: new SuperMap.Size(13, 160), //sliderbar大小
    szTemp_zoombar_center: new SuperMap.Size(1, 160),
    sliderEvents: null,
    divEvents: null,
    divEventLevel: null,
    forceFixedZoomLevel: false,
    mouseDragStart: null,
    deltaY: null,
    mapLevel: [],
    divLevel: null,
    zoomStart: null,
    showSlider: false,
    showCompass: true,
    levelsDesc: null,
    initialize: function(l) {
        SuperMap.Control.prototype.initialize.apply(this, arguments);
        var d = false,
            k = false,
            j = new SuperMap.Size(17, 17), //内侧罗盘全幅显示zoommaxextent的大小
            g = new SuperMap.Size(27, 27), //缩小放大按钮大小
            c = new SuperMap.Size(11, 11),  //缩放条的大小
            a = new SuperMap.Size(63, 62), //外侧罗盘size of Compass
            h, f, i, e;
        if (l) {
            this.showSlider = typeof l.showSlider !== "undefined" ? l.showSlider : true;
            this.showCompass = typeof l.showCompass !== "undefined" ? l.showCompass : true
        }
        SuperMap.Control.PanZoomBarExt.prototype.draw = function(n) {
            SuperMap.Control.prototype.draw.apply(this, arguments);
            n = n || new SuperMap.Pixel(4, 4);
            n.y += a.h / 2;
            this.buttons = [];
            i = this.centered;
            i = new SuperMap.Pixel(n.x + a.w / 2, n.y); //罗盘的中心位置
            var o;
            if (this.showCompass) {
                this._addButton("pan", "zoom-maxextent-mini.png", i.add(-a.w / 2 + j.w / 2, -a.h / 2 + j.h / 2), a);
                this._addButton("panup", "", i.add(-a.w / 6 + j.w / 2, -a.h / 2 + j.h / 2), a);
                this._addButton("panleft", "", i.add(-a.w / 2 + j.w / 2, -a.h / 6 + j.h / 2), a);
                this._addButton("panright", "", i.add(a.w / 6 + j.w / 2, -a.h / 6 + j.h / 2), a);
                this._addButton("pandown", "", i.add(-a.w / 6 + j.w / 2, a.w / 6 + j.w / 2), a);
                this._addButton("zoommaxextent", "zoom-maxextent-mini.png", i, j);
                o = a.w / 2
            } else {
                o = -a.w / 2
            }
            this._addButton("zoomin", "controlSkinBlue/zoom-plus-mini.png", i.add(-g.w / 2 + j.w / 2, o + j.h / 2 + 10), g);
            if (this.showSlider) {
                var m = this.szTemp_zoombar.h;
                this._addButton("zoomout", "controlSkinBlue/zoom-minus-mini.png", i.add(-g.w / 2 + j.w / 2, o + j.h / 2 + 10 + g.h + m), g);
                this._addZoomBar(i.add(j.w / 2 - this.zoomStopWidth / 2, o + j.h / 2 + 10 + g.h))
            } else {
                this._addButton("zoomout", "controlSkinBlue/zoom-minus-mini.png", i.add(-g.w / 2 + j.w / 2, o + j.h / 2 + 8 + g.h), g)
            }
            return this.div
        };
        SuperMap.Control.PanZoomBarExt.prototype._addButton = function(t, n, q, s) {
            h = SuperMap.Util.getImagesLocation() + n; //图片位置

            if (t == "panup" || t == "panleft" || t == "pandown" || t == "panright") {
                var p = SuperMap.Util.createDiv(this.id + "_" + t, q, "", "", "absolute");
                p.style.width = s.w / 3 + "px";
                p.style.height = s.w / 3 + "px";
                 p.style.cursor = "pointer";
                if ( !! (window.attachEvent && navigator.userAgent.indexOf("Opera") === -1)) {
                    p.style.backgroundColor = "#4c4c4c";
                    p.style.filter = "alpha(opacity=0)";
                    p.style.opacity = 0
                }
            } else {
                if (t == "pan") {
                    var p = SuperMap.Util.createAlphaImageDiv(this.id + "_" + t, q, s, h, "absolute")
                } else {
                    var p = SuperMap.Util.createAlphaImageDiv(this.id + "_" + t, q, s, h, "absolute");
                    p.style.cursor = "pointer"
                }
            }
            this.div.appendChild(p);
            SuperMap.Event.observe(p, "mousedown", SuperMap.Function.bindAsEventListener(this.buttonDown, p));
            SuperMap.Event.observe(p, "dblclick", SuperMap.Function.bindAsEventListener(this.doubleClick, p));
            SuperMap.Event.observe(p, "click", SuperMap.Function.bindAsEventListener(this.doubleClick, p));
            SuperMap.Event.observe(p, "mouseover", SuperMap.Function.bindAsEventListener(this.btnMouseOver, p));
            SuperMap.Event.observe(p, "mouseout", SuperMap.Function.bindAsEventListener(this.btnMouseOut, p));
            SuperMap.Event.observe(p, "mouseup", SuperMap.Function.bindAsEventListener(this.passEventToSlider, this));
            p.action = t;
            p.id = this.id;
            p.map = this.map;
            if (!this.slideRatio) {
                var o = this.slideFactor;
                var m = function() {
                    return o
                }
            } else {
                var r = this.slideRatio;
                var m = function(u) {
                    return this.map.getSize()[u] * r
                }
            }
            p.getSlideFactor = m;
            this.buttons.push(p);
            return p
        };
        SuperMap.Control.PanZoomBarExt.prototype._addZoomBar = function(t) {
            if (this.map.getNumZoomLevels()) {
//                this.zoomStopHeight = 109 / (this.map.getNumZoomLevels() - 1)
                this.zoomStopHeight = (this.szTemp_zoombar.h-10) / (this.map.getNumZoomLevels() - 1)
            }
            h = SuperMap.Util.getImagesLocation();
            var p = null;
            var o = this.id + "_" + this.map.id;
            var x = this.map.getNumZoomLevels() - 1 - this.map.getZoom();
            p = SuperMap.Util.createAlphaImageDiv(o, t.add(this.zoomStopWidth / 2 - 5.5, x * this.zoomStopHeight + 1), c, h + "slider.png", "absolute");
            p.style.cursor = "pointer";
            this.slider = p;
            this.sliderEvents = new SuperMap.Events(this, p, null, true, {
                includeXY: true
            });
            this.sliderEvents.on({
                touchstart: this.zoomBarDown,
                touchmove: this.zoomBarDrag,
                touchend: this.zoomBarUp,
                mousedown: this.zoomBarDown,
                mousemove: this.zoomBarDrag,
                mouseup: this.zoomBarUp,
                dblclick: this.doubleClick,
                click: this.doubleClick
            });
            var w = new SuperMap.Size(c.w, 3);
            var y = null;
            if (SuperMap.Util.alphaHack()) {
                var o = this.id + "_" + this.map.id;
                y = SuperMap.Util.createAlphaImageDiv(o, t, new SuperMap.Size(w.w, this.zoomStopHeight), h + "zoombar_glide.png", "absolute", null, "crop");
                y.style.height = w.h + "px"
            } else {
                y = SuperMap.Util.createDiv("SuperMap_Control_PanZoomBar_Zoombar" + this.map.id, t.add(1, 117), w, h + "zoombar_glide.png")
            }
            y.style.cursor = "pointer";
            this.div_zoombar_three = y;
            this.divEvents = new SuperMap.Events(this, y, null, true, {
                includeXY: true
            });
            this.divEvents.on({
                touchmove: this.passEventToSlider,
                mousedown: this.divClick,
                mousemove: this.passEventToSlider,
                dblclick: this.doubleClick,
                click: this.doubleClick
            });
            this.div.appendChild(y);
            var r = null;
            if (SuperMap.Util.alphaHack()) {
                var o = this.id + "_" + this.map.id;
                r = SuperMap.Util.createAlphaImageDiv(o, t, new SuperMap.Size(this.szTemp_zoombar.w, this.zoomStopHeight), h + "zoombar_center.png", "absolute", null, "crop");
                r.style.height = this.szTemp_zoombar.h + "px"
            } else {
                r = SuperMap.Util.createDiv("SuperMap_Control_PanZoomBar_Zoombar" + this.map.id, t.add(this.szTemp_zoombar.w / 2 - this.szTemp_zoombar_center.w / 2, 0), this.szTemp_zoombar_center, h + "zoombar_center.png")
            }
            r.style.cursor = "pointer";
            this.div_zoombar_one = r;
            this.div.appendChild(r);
            var v = null;
            if (SuperMap.Util.alphaHack()) {
                var o = this.id + "_" + this.map.id;
                v = SuperMap.Util.createAlphaImageDiv(o, t, new SuperMap.Size(this.szTemp_zoombar.w, this.zoomStopHeight), h + "zoombar.png", "absolute", null, "crop");
                v.style.height = this.szTemp_zoombar.h + "px"
            } else {
                v = SuperMap.Util.createDiv("SuperMap_Control_PanZoomBar_Zoombar" + this.map.id, t, this.szTemp_zoombar, h + "zoombar.png")
            }
            v.style.cursor = "pointer";
            this.div_zoombar = v;
            this.divEvents = new SuperMap.Events(this, v, null, true, {
                includeXY: true
            });
            if (SuperMap.Browser.device == "pc") {
                this.divEvents.on({
                    touchmove: this.passEventToSlider,
                    mousedown: this.divClick,
                    mousemove: this.passEventToSlider,
                    mouseup: this.passEventToSlider,
                    dblclick: this.doubleClick,
                    click: this.doubleClick,
                    mouseover: this.mouseOverLevel
                })
            } else {
                this.divEvents.on({
                    touchmove: this.passEventToSlider,
                    mousedown: this.divClick,
                    mousemove: this.passEventToSlider,
                    mouseup: this.passEventToSlider,
                    dblclick: this.doubleClick,
                    click: this.doubleClick
                })
            }
            this.div.appendChild(v);
            if (this.map.getNumZoomLevels()) {
                var n = false;
                this.divLevel = SuperMap.Util.createDiv("", t.add(this.zoomStopWidth, 0), new SuperMap.Size(this.zoomStopWidth + 30, 120));
                centered_roll_level = t.add(-t.x, -t.y);
                if ( !! (window.attachEvent && navigator.userAgent.indexOf("Opera") === -1)) {
                    var m = "url('" + h + "blank.gif')";
                    this.divLevel.style.background = m
                }
                if (SuperMap.Browser.device == "pc") {
                    this.divEventLevel = new SuperMap.Events(this, this.divLevel, null, true, {
                        includeXY: true
                    });
                    this.divEventLevel.on({
                        mouseout: this.mouseOutLevel,
                        mouseover: this.mouseOverLevel,
                        mouseup: this.passEventToSlider
                    })
                }
                var u = new SuperMap.Size(38, 21); //行政区划标签大小
                if (l && l.levelsDesc) {
                    this.levelsDesc = l.levelsDesc;
                    var z = null;
                    for (var q = 0, s = this.levelsDesc.levels.length; q < s; q++) {
                        this._buttonLabel(this.divLevel, z, centered_roll_level, this.levelsDesc.imageSources[q], this.levelsDesc.levels[q], u)
                    }
                }
            }
            this.startTop = parseInt(v.style.top);
            this.div.appendChild(p);
            this.moveZoomBar();
            this.map.events.register("zoomend", this, this.moveZoomBar)
        }
    },
    getDoms: function() {
        var a = {};
        a.container = this.div;
        if (this.showCompass) {
            a.pan = this.buttons[0];
            a.panup = this.buttons[1];
            a.panleft = this.buttons[2];
            a.panright = this.buttons[3];
            a.pandown = this.buttons[4];
            a.zoommaxextent = this.buttons[5];
            a.zoomIn = this.buttons[6];
            a.zoomOut = this.buttons[7]
        } else {
            a.zoomIn = this.buttons[0];
            a.zoomOut = this.buttons[1]
        }
        if (this.showSlider) {
            a.zoombarOne = this.div_zoombar_one;
            a.zoomBar = this.div_zoombar;
            a.zoombarThree = this.div_zoombar_three;
            a.slider = this.slider
        }
        return a
    },
    destroy: function() {
        this._removeZoomBar();
        this.map.events.un({
            changebaselayer: this.redraw,
            scope: this
        });
        this.removeButtons();
        this.buttons = null;
        this.position = null;
        SuperMap.Control.prototype.destroy.apply(this, arguments);
        delete this.mouseDragStart;
        delete this.zoomStart
    },
    setMap: function(a) {
        SuperMap.Control.prototype.setMap.apply(this, arguments);
        this.map.events.register("changebaselayer", this, this.redraw)
    },
    redraw: function() {
        if (this.div != null) {
            this.removeButtons();
            this._removeZoomBar();
            if (this.divLevel && this.divLevel.parentNode) {
                this.divLevel.parentNode.removeChild(this.divLevel);
                this.divLevel = null
            }
            this.mapLevel = []
        }
        this.draw()
    },
    _removeZoomBar: function() {
        if (this.showSlider) {
            this.sliderEvents.un({
                touchmove: this.zoomBarDrag,
                mousedown: this.zoomBarDown,
                mousemove: this.zoomBarDrag,
                mouseup: this.zoomBarUp,
                dblclick: this.doubleClick,
                click: this.doubleClick
            });
            this.sliderEvents.destroy();
            this.divEvents.un({
                touchmove: this.passEventToSlider,
                mousedown: this.divClick,
                mousemove: this.passEventToSlider,
                dblclick: this.doubleClick,
                click: this.doubleClick,
                mouseover: this.mouseOverLevel,
                mouseup: this.passEventToSlider
            });
            this.divEvents.destroy();
            this.div.removeChild(this.slider);
            this.div.removeChild(this.div_zoombar_three);
            this.div.removeChild(this.div_zoombar_one);
            this.div.removeChild(this.div_zoombar);
            this.slider = null;
            this.div_zoombar_three = null;
            this.div_zoombar_one = null;
            this.div_zoombar = null
        }
        this.map.events.unregister("zoomend", this, this.moveZoomBar)
    },
    passEventToSlider: function(a) {
        if (this.showSlider) {
            this.sliderEvents.handleBrowserEvent(a)
        }
    },
    divClick: function(a) {
        if (!SuperMap.Event.isLeftClick(a)) {
            return
        }
        var d = a.xy.y / this.zoomStopHeight;
        if (this.forceFixedZoomLevel || !this.map.fractionalZoom) {
            d = Math.floor(d)
        }
        var c = (this.map.getNumZoomLevels() - 1) - d;
        c = Math.min(Math.max(c, 0), this.map.getNumZoomLevels() - 1);
        this.map.zoomTo(c);
        SuperMap.Event.stop(a)
    },
    zoomBarDown: function(a) {
        if (!SuperMap.Event.isLeftClick(a) && !SuperMap.Event.isSingleTouch(a)) {
            return
        }
        this.map.events.on({
            touchmove: this.passEventToSlider,
            mousemove: this.passEventToSlider,
            mouseup: this.passEventToSlider,
            scope: this
        });
        this.mouseDragStart = a.xy.clone();
        this.zoomStart = a.xy.clone();
        this.div_zoombar.offsets = null;
        SuperMap.Event.stop(a)
    },
    zoomBarDrag: function(d) {
        if (this.mouseDragStart != null) {
            var a = this.mouseDragStart.y - d.xy.y;
            var f = SuperMap.Util.pagePosition(this.div_zoombar);
            if ((d.clientY - f[1]) > 5.5 && (d.clientY - f[1]) < parseInt(this.div_zoombar.style.height) - 5.5) {
                var e = parseInt(this.slider.style.top) - a;
                this.slider.style.top = e + "px";
                this.div_zoombar_three.style.top = e + 6 + "px";
                var c = parseInt(this.div_zoombar_three.style.height) + a;
                this.div_zoombar_three.style.height = c + "px";
                this.mouseDragStart = d.xy.clone()
            }
            this.deltaY = this.zoomStart.y - d.xy.y;
            SuperMap.Event.stop(d)
        }
    },
    zoomBarUp: function(a) {
        if (!SuperMap.Event.isLeftClick(a) && a.type !== "touchend") {
            return
        }
        if (this.mouseDragStart) {
            this.div.style.cursor = "";
            this.map.events.un({
                touchmove: this.passEventToSlider,
                mouseup: this.passEventToSlider,
                mousemove: this.passEventToSlider,
                scope: this
            });
            var c = this.map.zoom;
            if (!this.forceFixedZoomLevel && this.map.fractionalZoom) {
                c += this.deltaY / this.zoomStopHeight;
                c = Math.min(Math.max(c, 0), this.map.getNumZoomLevels() - 1)
            } else {
                c += this.deltaY / this.zoomStopHeight;
                c = Math.max(Math.round(c), 0)
            }
            if (c > this.map.getNumZoomLevels() - 1) {
                c = this.map.getNumZoomLevels() - 1
            }
            this.map.zoomTo(c);
            this.mouseDragStart = null;
            this.zoomStart = null;
            this.deltaY = 0;
            SuperMap.Event.stop(a)
        }
    },
    moveZoomBar: function() {
        var a = (this.map.getNumZoomLevels() - 1 - this.map.getZoom()) * this.zoomStopHeight + this.startTop + 1;
        if (isNaN(a)) {
            a = 0
        }
        this.slider.style.top = a + "px";
        this.div_zoombar_three.style.top = a + 6 + "px";
        this.div_zoombar_three.style.height = this.map.getZoom() * this.zoomStopHeight + 4.5 + "px"
    },
    _buttonLabel: function(d, g, h, e, a, i, f) {
        var m = new Array();
        var l = e.lastIndexOf("/"),
            k = e.lastIndexOf(".");
        var j = e.substring(l + 1, k);
        if (SuperMap.Util.alphaHack()) {
            var c = this.id + "_" + this.map.id;
            g = SuperMap.Util.createAlphaImageDiv(c, h.add(0, this.zoomStopHeight * a - 10), i, e, "absolute", null, "crop")
        } else {
            g = SuperMap.Util.createDiv("SuperMap_Control_PanZoomBar_Zoombar" + j, h.add(0, this.zoomStopHeight * a - 3), i, e)
        }
        g.style.backgroundRepeat = "no-repeat";
        g.style.left = "10px";
        m[0] = "SuperMap_Control_PanZoomBar_Zoombar" + j;
        m[1] = a;
        this.mapLevel.push(m);
        g.style.cursor = "pointer";
        g.style.display = "none";
        g.className = "sm_" + j;
        this.div_zoom_name = g;
        this.divEvents = new SuperMap.Events(this, g, null, true, {
            includeXY: true
        });
        this.divEvents.on({
            dblclick: this.doubleClickLevel,
            click: this.doubleClickLevel
        });
        d.appendChild(g);
        this.div.appendChild(d)
    },
    doubleClickLevel: function(c) {
        var f, h, g;
        var e = c ? c : window.evt;
        if (e.srcElement) {
            h = e.srcElement
        } else {
            h = e.target
        }
        g = h.id;
        for (var d = 0, a = this.mapLevel.length; d < a; d++) {
            if (this.mapLevel[d][0] == g) {
                f = this.map.getNumZoomLevels() - 1 - this.mapLevel[d][1]
            }
        }
        this.map.zoomTo(f);
        SuperMap.Event.stop(c)
    },
    mouseOverLevel: function(c) {
        for (var d = 0, a = this.mapLevel.length; d < a; d++) {
            if (this.mapLevel[d]) {
                document.getElementById(this.mapLevel[d][0]).style.display = "block"
            }
        }
    },
    mouseOutLevel: function() {
        for (var c = 0, a = this.mapLevel.length; c < a; c++) {
            if (this.mapLevel[c]) {
                document.getElementById(this.mapLevel[c][0]).style.display = "none"
            }
        }
    },
    btnMouseOver: function() {
        var a = SuperMap.Util.getImagesLocation();
        switch (this.action) {
            case "panup":
                document.getElementById(this.id + "_pan_innerImage").src = a + "north-mini.png";
                break;
            case "pandown":
                document.getElementById(this.id + "_pan_innerImage").src = a + "south-mini.png";
                break;
            case "panleft":
                document.getElementById(this.id + "_pan_innerImage").src = a + "west-mini.png";
                break;
            case "panright":
                document.getElementById(this.id + "_pan_innerImage").src = a + "east-mini.png";
                break
        }
    },
    btnMouseOut: function() {
        var a = SuperMap.Util.getImagesLocation();
        switch (this.action) {
            case "panup":
            case "pandown":
            case "panleft":
            case "panright":
                document.getElementById(this.id + "_pan_innerImage").src = a + "zoom-maxextent-mini.png";
                break
        }
    },
    getLinkStyle: function(a, c) {
        if (a.currentStyle) {
            return a.currentStyle[c]
        } else {
            if (window.getComputedStyle) {
                props = c.replace(/([A-Z])/g, "-$1");
                props = c.toLowerCase();
                return document.defaultView.getComputedStyle(a, null)[props]
            }
        }
        return null
    },
    _removeButton: function(a) {
        SuperMap.Event.stopObservingElement(a);
        a.map = null;
        a.getSlideFactor = null;
        this.div.removeChild(a);
        SuperMap.Util.removeItem(this.buttons, a)
    },
    removeButtons: function() {
        for (var a = this.buttons.length - 1; a >= 0; --a) {
            this._removeButton(this.buttons[a])
        }
    },
    doubleClick: function(a) {
        SuperMap.Event.stop(a);
        return false
    },
    buttonDown: function(a) {
        if (!SuperMap.Event.isLeftClick(a)) {
            return
        }
        switch (this.action) {
            case "panup":
                this.map.pan(0, -this.getSlideFactor("h"));
                break;
            case "pandown":
                this.map.pan(0, this.getSlideFactor("h"));
                break;
            case "panleft":
                this.map.pan(-this.getSlideFactor("w"), 0);
                break;
            case "panright":
                this.map.pan(this.getSlideFactor("w"), 0);
                break;
            case "zoomin":
                this.map.zoomIn();
                break;
            case "zoomout":
                this.map.zoomOut();
                break;
            case "zoommaxextent":
                this.map.zoomToMaxExtent();
                break
        }
        SuperMap.Event.stop(a)
    },
    CLASS_NAME: "SuperMap.Control.PanZoomBarExt"
});