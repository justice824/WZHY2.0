
/**
 * @requires SuperMap/Util.js
 * @requires SuperMap/Layer/CanvasLayer.js
 */

/**
 * Class: SuperMap.Layer.MapABC
 *    此图层可以访问 MapABC 的地图服务。
 *
 * Inherits from:
 *  - <SuperMap.Layer.CanvasLayer>
 */
SuperMap.Layer.MapABC = SuperMap.Class(SuperMap.CanvasLayer, {

    /**
     * APIProperty: name
     * {String}图层名称，默认为“MapABC”，防止初始化时未设置图层名
     *
     */
    name: "base:MapABC",

    /**
     * Property: url
     * {String}默认的MapABC的服务器地址
     */
    url: "http://emap${a}.mapabc.com/mapabc/maptile?v=w2.61&&x=${x}&y=${y}&z=${z}",    //互联网矢量图
//  url:"http://10.6.133.130/maptile/maptile?x=${x}&y=${y}&z=${z}",                    //国家局矢量图


//    url:"http://si.mapabc.com/appmaptile?x=${x}&y=${y}&z=${z}&lang=zh_cn&size=1&scale=1&style=6",  //互联网影像图

    /**
     * Constructor: SuperMap.Layer.MapABC
     * 创建MapABC图层，可以浏览MapABC地图
     * Example:
     * (code)
     *
     * var layer = new SuperMap.Layer.MapABC("MyName");
     * //将Layer图层加载到Map对象上
     * map.addLayer(layer);
     * //出图，map.setCenter函数显示地图
     * //MapABC图层默认为墨卡托投影，所以定位需要转换
     * map.setCenter(
     *  new SuperMap.LonLat(110,39.5 ).transform(
     *  new SuperMap.Projection("EPSG:4326"),
     *  map.getProjectionObject()
     *  ), 4
     *  );
     *                                    
     * (end)
     *
     *
     * Parameters:
     * name - {String} 图层名称
     * _selfUr- {String} 可支持影像地址
     */
    initialize: function(name,_selfUrl) {
        this.name = name || "base:MapABC";
        //设置为墨卡托投影
        var options = {
            projection: "EPSG:900913",
            numZoomLevels: 18
        };
        this.url = this.proxyUrl(_selfUrl||this.url);
        SuperMap.CanvasLayer.prototype.initialize.apply(this,[this.name,this.url,{},options] );
    },

    /**
     * Method: clone
     */
    clone: function(obj) {
        if (obj == null) {
            obj = new SuperMap.Layer.MapABC(
                this.name);
        }
        obj = SuperMap.CanvasLayer.prototype.clone.apply(this, [obj]);
        return obj;
    },

    /**
     * APIMethod: destroy
     * 解构MapABC类，释放资源。
     */
    destroy: function () {
        var me = this;
        me.name = null;
        me.url = null;
        SuperMap.CanvasLayer.prototype.destroy.apply(me, arguments);
    },
    /**
     * Method: getTileUrl
     * 获取瓦片的URL。
     *
     * Parameters:
     * xyz - {Object} 一组键值对，表示瓦片X, Y, Z方向上的索引。
     *
     * Returns
     * {String} 瓦片的 URL 。
     */
    getTileUrl: function (xyz) {
        var me = this,  url;
        url = me.url;
        url= SuperMap.String.format(url, {
            a: xyz.x%4,
            x: xyz.x,
            y: xyz.y,
            z: xyz.z
        });
      return  url;
    },


    /**
     * 将地址转化为代理服务地址
     * @param url
     * @returns {string}
     */
    proxyUrl : function (url) {
        var purl = this.getBaseUrl() +  "/web/proxy/tile?url=";
        var _param = url;  //this.url;
        _param = this.replaceChar(_param);
        return purl + _param;
    },


    getBaseUrl : function(){
        var loc = window.location;
        if (loc.origin){
            return loc.origin;
        }
        return loc.protocol+"//"+loc.host;
    },

    replaceChar:function (url) {
        url = url.replace(/&/g,"$");
        url= url.replace(/\?/g,"!");
        return url;
    },

    CLASS_NAME: "SuperMap.Layer.MapABC"
});
