
/**
 * @requires SuperMap/Util.js
 * @requires SuperMap/Layer/CanvasLayer.js
 */

/**
 * Class: SuperMap.Layer.MapABC
 *    此图层可以访问 MapABC 的地图服务。
 *
 * Inherits from:
 *  - <SuperMap.Layer.CanvasLayer>
 */
SuperMap.Layer.BjMap = SuperMap.Class(SuperMap.CanvasLayer, {


    name: "base:BjMap",

    url: "http://www.beijingmap.egov.cn/service/RSImage/wms?service=WMS&request=GetMap&width=256&height=256&format=JPEG&layers=59&styles=&bgcolor=0xFFFFFF&transparent=False&version=1.0.0&SRS=EPSG:0&bbox=${minx}${miny}${maxx}${maxy}&username=stjj01&password=tongjiju01",

    initialize: function(name) {
        this.name = name || "base:BjMap";
        //设置为墨卡托投影
        var options = {
//            projection: "EPSG:900913",
//            numZoomLevels: 3
        };

        this.url = this.getBaseUrl() + "/web/proxy/bjmap?bounds=";              // 本地代理
        SuperMap.CanvasLayer.prototype.initialize.apply(this,[this.name,this.url,{},options] );
    },

    getBaseUrl : function(){
        var loc = window.location;
        if (loc.origin){
            return loc.origin;
        }
        return loc.protocol+"//"+loc.host;
    },


    /**
     * Method: clone
     */
    clone: function(obj) {
        if (obj == null) {
            obj = new SuperMap.Layer.BjMap(
                this.name);
        }
        obj = SuperMap.CanvasLayer.prototype.clone.apply(this, [obj]);
        return obj;
    },

    /**
     * APIMethod: destroy
     * 解构MapABC类，释放资源。
     */
    destroy: function () {
        var me = this;
        me.name = null;
        me.url = null;
        SuperMap.CanvasLayer.prototype.destroy.apply(me, arguments);
    },

    getTileUrl: function (bounds) {
        var me = this,  url;
//        url = me.url;
//        url= SuperMap.String.format(url, {
//            minx: bounds.left + ",",
//            miny: bounds.bottom + ",",
//            maxx: bounds.top+ ",",
//            maxy: bounds.right
//        });
        var bdparam = bounds.left+ "," + bounds.bottom + "," + bounds.right + "," + bounds.top;
//        url = "http://localhost:8080/web/proxy/bjmap?bounds=" + bdparam;
        return this.url + bdparam;
    },

    getURL: function(c) {
        var b = this;
        c = b.adjustBounds(c);
        return b.getTileUrl(c);
    },


    CLASS_NAME: "SuperMap.Layer.BjMap"
});
