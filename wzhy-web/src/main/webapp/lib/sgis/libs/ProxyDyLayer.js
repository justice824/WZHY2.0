/**
 * Created by Augustine on 2014/11/23.
 */

/**
 * TiledDynamicRESTLayer图层的代理图层
 *
 */
SuperMap.Layer.ProxyDyLayer = SuperMap.Class(SuperMap.Layer.TiledDynamicRESTLayer,{


    getTileUrl: function(a) {
        var b = this,
            e, c = b.tileSize,
            d = b.scales[a.z];
        e = {
            width: c.w,
            height: c.h,
            x: a.x,
            y: a.y,
            scale: d,
            redirect: false,
            _token: b.token
        };
        if(!b.params.cacheEnabled) {
            e.t = new Date().getTime()
        }
        if(typeof b.params.layersID !== "undefined" && typeof e.layersID == "undefined") {
            if(b.params.layersID && b.params.layersID.length > 0) {
                e.layersID = b.params.layersID
            }
        }
        if(b.prjStr1) {
            e.prjCoordSys = b.prjStr1
        }
        var url = b.getFullRequestString(e);
        var temp = this.replaceChar(url);
        url =  this.getBaseUrl() + "/web/proxy/tile?url="  + temp; //本地后台切片代理（解决跨域，能切屏获取）
        return url;
    },

    getBaseUrl : function(){
        var loc = window.location;
        if (loc.origin){
            return loc.origin;
        }
        return loc.protocol+"//"+loc.host;
    },

    replaceChar:function (url) {
        url = url.replace(/&/g,"$");
        url= url.replace(/\?/g,"!");
        return url;
    },


    CLASS_NAME:"SuperMap.Layer.ProxyDyLayer"
});
