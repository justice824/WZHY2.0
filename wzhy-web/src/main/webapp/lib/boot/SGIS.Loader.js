/**
 * Created by Augustine on 14-7-11.
 */

/*
 *负责管理 加载所有前端 资源、文件等
 *
 */

if(typeof(SGIS) === "undefined"){
    SGIS = {Loader:{}};
}else{
    if(typeof(SGIS.Loader) === "undefined") SGIS.Loader = {};
}
var baseApp = "/web/";

SGIS.Loader = {

    baseApp:baseApp,

    baseUrl:function(){
        var loc = window.location;
        if (loc.origin){
            return loc.origin + baseApp;
        }
        return loc.protocol+"//"+loc.host +baseApp ;
    },

    config:{
      dev:true
    },

    _pm:[
        {name:'sgis',type:'core',dev:true,path:['lib/sgis/SGIS.Base.js']},

        {name:'bootstrap',type:'core',dev:true,path:['lib/bootstrap/js/bootstrap.js','lib/bootstrap/css/bootstrap.css']},
        {name:'bootstrap',type:'core',dev:false,path:['lib/bootstrap/js/bootstrap.min.js','lib/bootstrap/css/bootstrap.min.css']},

        {name:'semantic',type:'core',dev:true,path:['lib/semantic/javascripts/semantic.js','lib/semantic/css/semantic.css']},
        {name:'semantic',type:'core',dev:false,path:['lib/semantic/javascripts/semantic.min.js','lib/semantic/css/semantic.min.css']},

        {name:'jquery',type:'core',dev:true,path:['lib/jquery/jquery-2.1.1.js']},
        {name:'jquery',type:'core',dev:false,path:['lib/jquery/jquery-2.1.1.min.js']},

        {name:'mustache',type:'core',dev:true,path:['lib/mustache/mustache.js']},

        {name:'seajs',type:'core',dev:true,path:['lib/sea/sea.js']},
        {name:'seajs',type:'core',dev:false,path:['lib/sea/sea-debug.js']},

       /* {name:'echarts',type:'core',dev:true,path:['base/echart/echarts-original.js','base/echart/echarts-original-map.js','base/echart/echarts-original-plain.js','base/echart/echarts-original-plain-map.js']},
        {name:'echarts',type:'core',dev:false,path:['base/echart/echarts.js','base/echart/echarts-map.js','base/echart/echarts-plain.js','base/echart/echarts-plain-map.js']},

        {name:'echarts-o',type:'core',dev:true,path:['base/echarts-old/echarts-plain.js']},   //,'base/echarts-old/echarts-plain-map.js'

        {name:'underscore',type:'core',dev:true,path:['base/underscore/underscore.js']},
        {name:'underscore',type:'core',dev:false,path:['base/underscore/underscore.min.js']},*/

        {name:'sgisbase',type:'core',dev:true,path:['lib/page/SGIS.Base.js']},
        {name:'sgisbase',type:'core',dev:false,path:['lib/page/SGIS.Base.js']},

        {name:'iclient',type:'core',dev:true,path:['lib/sgis/libs/SuperMap.Include.js']},
        {name:'iclient',type:'core',dev:false,path:['lib/sgis/libs/SuperMap.Include.js']},

        {name:'iclient',type:'core',dev:true,path:['lib/sgis/libs/Tianditu.js']},
        {name:'tianditu',type:'core',dev:false,path:['lib/sgis/libs/Tianditu.js']},

        {name:'mapabc',type:'core',dev:true,path:['lib/sgis/libs/Mapabc.js']},
        {name:'mapabc',type:'core',dev:false,path:['lib/sgis/libs/Mapabc.js']},

        {name:'bigautocomplete',type:'core',dev:true,path:['lib/bigautocomplete/jquery.bigautocomplete.js','lib/bigautocomplete/jquery.bigautocomplete.css']},
        {name:'bigautocomplete',type:'core',dev:false,path:['lib/bigautocomplete/libs/Mapabc.js','lib/bigautocomplete/jquery.bigautocomplete.css']},

        /*{name:'events',type:'core',dev:true,path:['base/event/events.js']},*/

        {name:'dhtmlxgrid',type:'core',dev:true,path:['lib/dhtmlx/dhtmlxcommon.js','lib/dhtmlx/dhtmlxgrid.js','lib/dhtmlx/dhtmlxgridcell.js',
            'lib/dhtmlx/ext/dhtmlxgrid_drag.js','lib/dhtmlx/dhtmlxgrid.css','lib/dhtmlx/skins/dhtmlxgrid_dhx_skyblue.css','lib/dhtmlx/skins/dhtmlxgrid_dhx_terrace.css']} ,
        {name:'dhtmlxtree',type:'core',dev:true,path:['lib/dhtmlx/dhtmlxcommon.js','lib/dhtmlx/dhtmlxtree.js','lib/dhtmlx/dhtmlxtree.css']},
        {name:'md5',type:'core',dev:true,path:['lib/md5/md5.js']} ,

        {name:'cookietool',type:'core',dev:true,path:['lib/commons/cookietools.js']},

        {name:'ProxyDyLayer',type:'core',dev:true,path:['lib/sgis/libs/ProxyDyLayer.js']},
        {name:'OnLineLayer',type:'core',dev:false,path:['lib/sgis/libs/OnLineLayer.js']}

    ],
    /**
     * 加载脚本文件
     * @param name
     * @param path
     * @private
     */
    _IncludeScript: function (path) {
        var script = '<' + 'script type="text/javascript" src="' + this.baseUrl() + path + '"  ' + '><' + '/script>';
        document.writeln(script);
    },
    /**
     * 加载样式
     * @param {} name 文件名称
     * @param {} path 文件路径
     */
    _IncludeStyle: function (path) {
        var style = '<' + 'link type="text/css" rel="stylesheet" href="' + this.baseUrl() + path + '"' + ' />';
        document.writeln(style);
    },

    getItemByName:function(arg){
        var name = arg.name;
        var type = arg.type;
        var dev = arg.dev;

        var item = {};
        for(var i=0,size=this._pm.length; i<size; i++){
            var it = this._pm[i];
            var nm = it.name;
            var ty = it.type;
            var de = it.dev;
            if(name==nm && type==ty && de==dev ){
                item = it;
                break;
            }else if(name==nm && type==ty){
                item = it;
                break;
            }
        }
        return item;
    } ,

    boot: function (args) {
        var hasGandT = 0; //是否同时拥有 grid和tree
        for (var i = 0; i < args.length; i++) {
            var item = {};
            item.name = args[i];
            item.type = "core";
            item.dev = this.config.dev;
            var paths = this.getItemByName(item).path;

            if(item.name.indexOf("dhtmlxgrid")!=-1 || item.name.indexOf("dhtmlxtree")!=-1 ){
                hasGandT ++;
            }
            for (var j = 0; j < paths.length; j++) {
                var path = paths[j];
                if(hasGandT>1 && path.indexOf("dhtmlxcommon")!=-1) continue;
                if (path.indexOf(".css") != -1) {
                   this._IncludeStyle(path);//加载CSS
                } else {
                   this._IncludeScript(path);//加载JS
                }
            }
        }
    }
};





