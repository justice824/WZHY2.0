/**
 * Created by duanxiaofei on 2016/2/3.
 */
;
(function($,window,document,undefined){

    function PageInfo(pageNumber,pageSize){
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }
    PageInfo.prototype ={
        getPageNumber:function(){
            return this.pageNumber;
        },
        getPageSize:function(){
            return this.pageSize;
        }
    }

    $.PageInfo=function(pageNumber,pageSize){
        return new PageInfo(pageNumber,pageSize);
    };

})(jQuery,window,document);