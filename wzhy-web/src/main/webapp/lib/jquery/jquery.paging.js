/**
 * 鼠标移动，mousemove 事件
 * DOM 元素动态定位，window对象的resize和scroll 事件
 * Created by duanxiaofei on 2016/2/3.
 */
;
(function ($, window, document, undefined) {
    function Paging(container) {
        this.div = typeof container == "string" ? $("#" + container) : container;
        this.pageSize = 10;
        this.pageCount = 0;
        this.recordCount = 0;
        this.currPage = 1;
        this.click = null;
        this.style ="1";
        this.showPageNum = 7;

        //提供几种模板
        this.templates = {
            simple: '1',
            complex :"2"
        };
        this.init = function (pageData) {
            var that = this;
            this.pageSize = pageData.pageSize || 10;
            this.recordCount = pageData.count || 0;
            this.pageCount = this.recordCount % this.pageSize == 0 ? this.recordCount / this.pageSize : parseInt(this.recordCount / this.pageSize) + 1;
            this.currPage = this.pageCount > 0 ? 1 : 0;
            this.style =pageData.style!=null&&pageData.style!=undefined?pageData.style:1;
            this.showPageNum = pageData.showPageNum >0?pageData.showPageNum:7;//可以调整显示的页码数

            if(this.style==1){
                var pageHtml = '<div class="ui menu small fluid twelve item borderless">';
                pageHtml += '<a class="first icon item"><i class="fast backward icon"></i>第1页</a>';
                pageHtml += '<a class="pre icon item"><i class="backward icon"></i>前1页</a>';
                pageHtml += '<a class="text item" style="width: 200px;">' + this.currPage + "/" + this.pageCount + '页' +
                    '<span style="font-weight: bold;">(共'+
                    this.recordCount+')</span></a>';
                pageHtml += '<a  class="next icon item"><i  class="forward icon"></i>下1页</a>';
                pageHtml += '<a  class="last icon item"><i  class="fast forward icon"></i>最后1页</a>';
                this.div.html("");
                $(pageHtml).appendTo(this.div);

                var prePageHTML = $(".pre", this.div);
                var nextPageHTML = $(".next", this.div);
                var firstPageHTML = $(".first", this.div);
                var lastPageHTML = $(".last", this.div);
                prePageHTML.click(function () {
                    //已经是第一页了
                    if(that.currPage <2){
                        return ;
                    }
                    if (that.currPage > 1) {
                        that.currPage--;
                    }
                    afterClick(that);
                });
                nextPageHTML.click(function () {
                    //已经是最后一页了
                    if(that.currPage == that.pageCount){
                        return ;
                    }
                    if (that.currPage < that.pageCount) {
                        that.currPage++;
                    }
                    afterClick(that);
                });
                firstPageHTML.click(function () {
                    if(that.currPage <2){
                        return ;
                    }
                    if (that.pageCount > 0) {
                        that.currPage = 1;
                        afterClick(that);
                    }
                });
                lastPageHTML.click(function () {
                    //已经是最后一页了
                    if(that.currPage == that.pageCount){
                        return ;
                    }
                    if (that.pageCount > 0) {
                        that.currPage = that.pageCount;
                        afterClick(that);
                    }

                });
            }else if(this.style == 2){
                var pageHtml = '<div class="ui borderless menu small purple inverted">';
                pageHtml +='<a class="first item"> <i class="icon double angle left "></i>第一页</a>';
                pageHtml +='<a class="pre item"><i class="icon angle left "></i>前一页</a>';
                var textHtml ='<div class="pagecount text item">共'+this.pageCount+'页</div>';
                textHtml += '<div class="recordcount text item">共'+this.recordCount+'条记录</div>';
                var jumpHtml ='<div class="ui item"> <div class="ui action input small"><input type="text" placeholder="页号" style="width: 65px;"> <a class="ui button jump">跳转</a> </div> </div>';
//                jumpHtml += '<div style="left: -10px;" class="ui item jump mini button">跳转</div>';

                var showPageNum = that.showPageNum;

                if(that.pageCount <1){
                }else {
                    var len = showPageNum<=that.pageCount?showPageNum:that.pageCount;
                    for(var pageNum =1;pageNum <len+1;pageNum++){
                        pageHtml +=' <a class="item page">'+pageNum+'</a>';
                    }
                }
                pageHtml +='<a class="next item">下一页<i class=" icon  angle right "></i></a>';
                pageHtml +='<a class="last item">最后一页<i class="icon double angle right "></i></a>';
                pageHtml +=(jumpHtml+textHtml);
                this.div.html("");
                $(pageHtml).appendTo(this.div);

                $("a.page", this.div).removeClass("active");
                $("a.page", this.div).eq(getPageIndex(that,this.currPage,showPageNum)).addClass("active");

                $("a.item", this.div).click(function () {
                    var operateType = 0;// 0 1 2 3 4 page first pre next last
                    if ($(this).hasClass("first")) {
                        if (that.currPage < 2) {
                            return;
                        }
                        if (that.pageCount > 0) {
                            that.currPage = 1;
                        }
                        operateType = 1;

                    } else if ($(this).hasClass("pre")) {
                        if (that.currPage < 2) {
                            return;
                        }
                        if (that.currPage > 1) {
                            that.currPage--;
                        }
                        operateType = 2;

                    } else if ($(this).hasClass("next")) {
                        if (that.currPage == that.pageCount) {
                            return;
                        }
                        if (that.currPage < that.pageCount) {
                            that.currPage++;
                        }
                        operateType = 3;

                    } else if ($(this).hasClass("last")) {
                        if (that.currPage == that.pageCount) {
                            return;
                        }
                        if (that.pageCount > 0) {
                            that.currPage = that.pageCount;
                        }
                        operateType = 4;

                    } else if ($(this).hasClass("nopage")) {
                        return;
                    } else {
                        if ($(this).text() == "" || $(this).text() == null) {
                            return;
                        }
                        that.currPage = parseInt($(this).text());
                    }
                    afterClick(that, operateType);

                });

                //跳转查询事件
                $(".jump.button",this.div).click(function(){
                    var page = $("input",that.div).val();
                    if(page == null || page ==""){ return ;}
                    var regex = new RegExp('^[0-9]+$');
                    if(regex.test(page)){
                        if(page <= that.pageCount&&page>0){
                            that.currPage =parseInt(page) ;
                            afterClick(that,-1);
                        }else {
                            alert("输入的页码过大或过小");
                        }
                    }else{
                        alert("请输入正确的页码");
                    }

                });
            }
            this.checkStatus();

        };
        function afterClick(that,operateType) {
            function changePageText(b,start){
                $(".page", b.div).each(function(i,obj){
                    var pageNum = start + i;
                    if(pageNum <= b.pageCount){
                        $(this).removeAttr("disabled");
                        $(this).removeClass("disabled");
                        $(this).text(start+i);
                    }else{
                        $(this).attr("disabled","disabled");
                        $(this).addClass("disabled");
                        $(this).text("");

                    }
                })
            }
            var o = that;
            if(o.style ==1){

                var textHTML = $(".text", o.div);
                var html  = o.currPage + "/" + o.pageCount+'页<span style="font-weight: bold;">(共'+
                    o.recordCount+')</span></a>';
                textHTML.html(html);

            }else if(o.style ==2){

                var showPageNum = that.showPageNum ;
                if(operateType == 0){

                }else if(operateType ==1 ){//第一页
                    changePageText(o, o.currPage);

                }else if(operateType ==2 ){//前一页
                    if(o.currPage% showPageNum == 0){
                        var times = Math.floor(o.currPage/showPageNum);
                        var start = (times-1)*showPageNum +1;
                        changePageText(o,start);

                    }else{

                    }

                }else if(operateType ==3 ){//后一页
                    if(o.currPage% showPageNum == 1){
                        changePageText(o, o.currPage);
                    }else{

                    }

                }else if(operateType ==4 ){//最后一页
                    var times =  Math.floor(o.currPage/showPageNum);
                    var start = -1;
                    if(o.currPage%showPageNum == 0){
                        start = showPageNum*(times - 1)+1;
                    }else{
                        start = showPageNum*times+1;
                    }
                    changePageText(o,start);

                }else if(operateType ==-1){//跳转
                    var  times = Math.floor(o.currPage/showPageNum);
                    if(o.currPage%showPageNum == 0){
                        times = times -1;
                    }
                    var start = times* showPageNum +1;
                    changePageText(o,start);
                }
                var index = getPageIndex(o,o.currPage,showPageNum);//选中显示页序号
                //console.log(index);
                $("a.page", o.div).removeClass("active");
                $("a.page", o.div).eq(index).addClass("active");

            }
            o.checkStatus();
            o.click && o.click(o.currPage);
        };
        function getPageIndex(that,currPage,showPageNum){
            var index = currPage%showPageNum;
            if(index == 0){
                index = that.showPageNum;
            }
            return  index-1 ;
        };
    }

    Paging.prototype = {

        checkStatus:function(){
            var o = this;
            var prePageHTML = $(".pre", o.div);
            var nextPageHTML = $(".next", o.div);
            var firstPageHTML = $(".first", o.div);
            var lastPageHTML = $(".last", o.div);
            firstPageHTML.removeClass("disabled");
            prePageHTML.removeClass("disabled");
            nextPageHTML.removeClass("disabled");
            lastPageHTML.removeClass("disabled");

            firstPageHTML.removeAttr("disabled");
            prePageHTML.removeAttr("disabled");
            nextPageHTML.removeAttr("disabled");
            lastPageHTML.removeAttr("disabled");
            if (o.currPage < 2) {
                firstPageHTML.addClass("disabled");
                prePageHTML.addClass("disabled");

                firstPageHTML.attr("disabled","disabled");
                prePageHTML.attr("disabled","disabled");
            }
            if (o.currPage == o.pageCount) {
                nextPageHTML.addClass("disabled");
                lastPageHTML.addClass("disabled");

                nextPageHTML.attr("disabled","disabled");
                lastPageHTML.attr("disabled","disabled");
            }
        },

        //添加分页跳转查询事件
        addClick: function (callback) {
            this.click = callback;
        },

        render: function (pageData) {
            this.init(pageData);
        },

        //更新分页栏显示
        update: function (pageData) {
            var that = this;
            this.pageSize = pageData.pageSize || 10;
            this.recordCount = pageData.count || 0;
            this.pageCount = this.recordCount % this.pageSize == 0 ? this.recordCount / this.pageSize : parseInt(this.recordCount / this.pageSize) + 1;
            this.currPage = pageData.pageNumber != null ? pageData.pageNumber : this.pageCount > 0 && this.currPage == 0 ? 1 : this.currPage;
            this.checkStatus();
            if(that.style == 1){
                var textHTML = $(".text", that.div);
                var html  = this.currPage + "/" + this.pageCount+'页<span style="font-weight: bold;">(共'+
                    this.recordCount+')</span></a>';
                textHTML.html(html);
            }else if(that.style == 2){
                var pagecountHtml = $(".pagecount", that.div);
                var recordcountHtml = $(".recordcount", that.div);
                pagecountHtml.html("共"+that.pageCount+"页");
                recordcountHtml.html("共"+that.recordCount+"条记录");
            }
        },

        dispose: function () {
            this.div.html("");
            delete this;
        },
        getCurrPage :function(){
            return this.currPage ;
        },
        getPageSize :function(){
            return this.pageSize ;
        }
    }


    $.Paging = function (container) {
        return new Paging(container);
    };

})(jQuery, window, document);