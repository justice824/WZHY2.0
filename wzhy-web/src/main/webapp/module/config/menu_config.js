/**
 * annotation
 * Created by sun'fei on 2017-08-11 in WZHY_V2.0 system.
 * this's menu config file
 * use getMenuList() function to get menu List
 * use getMenuName() function to get menu Name
 */
var menu_name = "basic";
var _menus = {
    basic : [
        {
            "menuid" : "101",
            "icon" : "icon-sys",
            "menuname" : "多证数据查询",
            "menus" : [ {
                "menuid" : "102",
                "menuname" : "工商信息查询",
                "icon" : "icon-searchs",
                "url" : "search/base_info.html"
            }, {
                "menuid" : "103",
                "menuname" : "税务信息查询",
                "icon" : "icon-searchs",
                "url" : "search/tax_info.html"
            }, {
                "menuid" : "105",
                "menuname" : "社保信息查询",
                "icon" : "icon-searchs",
                "url" : "search/social_info.html"
            }, {
                "menuid" : "107",
                "menuname" : "统计信息查询",
                "icon" : "icon-searchs",
                "url" : "search/statistics_info.html"
            }]
        },


        {
        "menuid" : "20",
        "icon" : "icon-sys",
        "menuname" : "多证数据转换",
        "menus" : [ {
            "menuid" : "211",
            "menuname" : "工商基本信息导出",
            "icon" : "icon-conversion",
            "url" : "conversion/base_info_export.html"
        }, {
            "menuid" : "212",
            "menuname" : "工商年报信息导出",
            "icon" : "icon-conversion",
            "url" : "#"
        }, {
            "menuid" : "213",
            "menuname" : "社会保障信息导出",
            "icon" : "icon-conversion",
            "url" : "#"
        }, {
            "menuid" : "214",
            "menuname" : "税务登记信息导出",
            "icon" : "icon-conversion",
            "url" : "#"
        }]
    } ,


        {
        "menuid" : "30",
        "icon" : "icon-sys",
        "menuname" : "数据共享交换",
        "menus" : [ {
            "menuid" : "311",
            "menuname" : "工商数据推送",
            "icon" : "icon-switching",
            "url" : "#"
        }, {
            "menuid" : "312",
            "menuname" : "名录数据推送",
            "icon" : "icon-switching",
            "url" : "#"
        }, {
            "menuid" : "313",
            "menuname" : "在线填报",
            "icon" : "icon-switching",
            "url" : "#"
        }, {
            "menuid" : "314",
            "menuname" : "数据比对",
            "icon" : "icon-switching",
            "url" : "#"
        }]
    },

        {
            "menuid" : "10",
            "icon" : "icon-sys",
            "menuname" : "数据指标配置",
            "menus" : [ {
                "menuid" : "111",
                "menuname" : "工商基本信息配置",
                "icon" : "icon-config",
                "url" : "#"
            }, {
                "menuid" : "113",
                "menuname" : "工商年报数据配置",
                "icon" : "icon-config",
                "url" : "#"
            }, {
                "menuid" : "115",
                "menuname" : "违法处罚数据配置",
                "icon" : "icon-config",
                "url" : "#"
            }, {
                "menuid" : "117",
                "menuname" : "文件柜数据配置",
                "icon" : "icon-config",
                "url" : "#"
            }]
        },

        {
        "menuid" : "40",
        "icon" : "icon-sys",
        "menuname" : "日志查询管理",
        "menus" : [ {
            "menuid" : "411",
            "menuname" : "工商基本信息日志",
            "icon" : "icon-log",
            "url" : "#"
        }, {
            "menuid" : "412",
            "menuname" : "工商年报信息日志",
            "icon" : "icon-log",
            "url" : "#"
        }, {
            "menuid" : "413",
            "menuname" : "工商违法失信日志",
            "icon" : "icon-log",
            "url" : "#"
        }]
    },

        {
        "menuid" : "50",
        "icon" : "icon-sys",
        "menuname" : "用户权限管理",
        "menus" : [ {
            "menuid" : "511",
            "menuname" : "用户管理",
            "icon" : "icon-man",
            "url" : "#"
        }, {
            "menuid" : "512",
            "menuname" : "权限管理",
            "icon" : "icon-man",
            "url" : "#"
        }]
    }


  ]
};

/**
 * 系统菜单配置数据集合接口
 * @returns {{basic: [*,*,*,*,*]}}
 */
function getMenuList() {
    return _menus;
}

/**
 * 系统菜单配置菜单名称接口
 * @returns {string}
 */
function getMenuName() {
    return menu_name;
}
