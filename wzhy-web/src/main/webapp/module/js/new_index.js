/**
 * 新首页 new_index.js
 * Created by sun'fei on 2017-08-10.
 * this's init function
 */
$(function () {
    //menu 接口
    var _menus = getMenuList();
    //标签关闭函数
    tabClose();
    //标签关闭事件
    tabCloseEven();
    //菜单初始化
    $("#menu").accordion({
        animate : false
    });
    //菜单中文名称
    var firstMenuName = getMenuName();
    addNav(_menus[firstMenuName]);
    //初始化左侧导航
    InitLeftMenu();

    loginOut();

    //定时函数
    $("body").load(oneSecond());
})

function oneSecond() {
    setInterval(function () {
        $(".header-label").html(getNowFormatDate());
    },1000);
}


function addNav(data) {
    $.each(data, function(i, sm) {
        var menulist = "";
        menulist += '<ul>';
        $.each(sm.menus, function(j, o) {
            menulist += '<li><div><a ref="' + o.menuid + '" href="#" rel="'
                + o.url + '" ><span class="icon ' + o.icon
                + '" >&nbsp;&nbsp;</span><span class="nav">' + o.menuname
                + '</span></a></div></li> ';
        });
        menulist += '</ul>';

        $('#menu').accordion('add', {
            title : sm.menuname,
            content : menulist,
            iconCls : 'icon ' + sm.icon
        });

    });

    var pp = $('#menu').accordion('panels');
    var t = pp[0].panel('options').title;
    $('#menu').accordion('select', t);

}

// 初始化左侧
function InitLeftMenu() {
    hoverMenuItem();
    $('#menu li a').on('click', function() {
        var tabTitle = $(this).children('.nav').text();

        var url = $(this).attr("rel");
        var menuid = $(this).attr("ref");
        var icon = getIcon(menuid, icon);

        addTab(tabTitle, url, icon);
        $('#menu li div').removeClass("selected");
        $(this).parent().addClass("selected");
    });

}

/**
 * 菜单项鼠标 Hover事件
 */
function hoverMenuItem() {
    $(".easyui-accordion").find('a').hover(function() {
        $(this).parent().addClass("hover");
    }, function() {
        $(this).parent().removeClass("hover");
    });
}

// 获取左侧导航的图标
function getIcon(menuid) {
    var icon = 'icon ';
    $.each(_menus, function(i, n) {
        $.each(n, function(j, o) {
            $.each(o.menus, function(k, m){
                if (m.menuid == menuid) {
                    icon += m.icon;
                    return false;
                }
            });
        });
    });
    return icon;
}

function addTab(subtitle, url, icon) {
    if (!$('#tabs').tabs('exists', subtitle)) {
        $('#tabs').tabs('add', {
            title : subtitle,
            content : createFrame(url),
            closable : true,
            icon : icon
        });
    } else {
        $('#tabs').tabs('select', subtitle);
        $('#mm-tabupdate').click();
    }
    tabClose();
}

function createFrame(url) {
    var s = '<iframe scrolling="auto" frameborder="0"  src="' + url + '" ' +
        'style="width:99.5%;height:99.5%;overflow-y: hidden;overflow-x: hidden">' +
        '</iframe>';
    return s;
}

function tabClose() {
    /* 双击关闭TAB选项卡 */
    $(".tabs-inner").dblclick(function() {
        var subtitle = $(this).children(".tabs-closable").text();
        $('#tabs').tabs('close', subtitle);
    });
    /* 为选项卡绑定右键 */
    $(".tabs-inner").bind('contextmenu', function(e) {
        $('#mm').menu('show', {
            left : e.pageX,
            top : e.pageY
        });

        var subtitle = $(this).children(".tabs-closable").text();

        $('#mm').data("currtab", subtitle);
        $('#tabs').tabs('select', subtitle);
        return false;
    });
}
// 绑定右键菜单事件
function tabCloseEven() {
    // 刷新
    $('#mm-tabupdate').click(function() {
        var currTab = $('#tabs').tabs('getSelected');
        var url = $(currTab.panel('options').content).attr('src');
        $('#tabs').tabs('update', {
            tab : currTab,
            options : {
                content : createFrame(url)
            }
        });
    });
    // 关闭当前
    $('#mm-tabclose').click(function() {
        var currtab_title = $('#mm').data("currtab");
        $('#tabs').tabs('close', currtab_title);
    });
    // 全部关闭
    $('#mm-tabcloseall').click(function() {
        $('.tabs-inner span').each(function(i, n) {
            var t = $(n).text();
            $('#tabs').tabs('close', t);
        });
    });
    // 关闭除当前之外的TAB
    $('#mm-tabcloseother').click(function() {
        $('#mm-tabcloseright').click();
        $('#mm-tabcloseleft').click();
    });
    // 关闭当前右侧的TAB
    $('#mm-tabcloseright').click(function() {
        var nextall = $('.tabs-selected').nextAll();
        if (nextall.length == 0) {
            msgShow('系统提示','后边没有啦~~','error');
            return false;
        }
        nextall.each(function(i, n) {
            var t = $('a:eq(0) span', $(n)).text();
            $('#tabs').tabs('close', t);
        });
        return false;
    });
    // 关闭当前左侧的TAB
    $('#mm-tabcloseleft').click(function() {
        var prevall = $('.tabs-selected').prevAll();
        if (prevall.length == 0) {
            msgShow('系统提示','到头了，前边没有啦~~','error');
            return false;
        }
        prevall.each(function(i, n) {
            var t = $('a:eq(0) span', $(n)).text();
            $('#tabs').tabs('close', t);
        });
        return false;
    });

    // 退出
    $("#mm-exit").click(function() {
        $('#mm').menu('hide');
    });
}


function loginOut() {
    var hostport = document.location.host;
    $("#loginOut").bind("click",function () {
        location.replace("http://"+hostport + "/web");
    })
}

/**
 * 获取当前时间
 * yyyy-MM-dd hh24:mm:ss
 * @returns {string}
 */
function getNowFormatDate() {
    var date = new Date();
    var s1 = "-";
    var s2 = ":";
    var month = date.getMonth() + 1;
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds =date.getSeconds();
    var strDate = date.getDate();

    var week = " 周" + "日一二三四五六".charAt(date.getDay());

    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }

    if (hours >= 0 && hours <= 9) {
        hours = "0" + hours;
    }
    if (minutes >= 0 && minutes <= 9) {
        minutes = "0" + minutes;
    }
    if (seconds >= 0 && seconds <= 9) {
        seconds = "0" + seconds;
    }
    var currentDate = date.getFullYear() + s1 + month + s1 + strDate
        + " " + hours + s2 + minutes + s2 + seconds  + " " + week ;
    return currentDate;
}

// 弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
function msgShow(title, msgString, msgType) {
    $.messager.alert(title, msgString, msgType);
}


/*
 function addTab(title, url){
 if ($('#tabs').tabs('exists', title)){
 $('#tabs').tabs('select', title);
 } else {
 var content = '<iframe scrolling="auto" frameborder="0"  src="'+url+'" style="width:100%;height:100%;"></iframe>';
 $('#tabs').tabs('add',{
 title:title,
 content:content,
 closable:true
 });
 }
 }*/


/*function Clearnav() {
 var pp = $('#menu').accordion('panels');

 $.each(pp, function(i, n) {
 if (n) {
 var t = n.panel('options').title;
 $('#menu').accordion('remove', t);
 }
 });

 pp = $('#menu').accordion('getSelected');
 if (pp) {
 var title = pp.panel('options').title;
 $('#menu').accordion('remove', title);
 }
 }*/