/**
 *  基本信息查询js
 *  @fn 函数
 *  -->:下级函数或调用函数
 *  @module:模块化函数
 *  @init:初始化函数
 *  @bind:绑定事件
 */
$(function () {
    initSearchCondition();

})
/**
 * @init
 * 初始化查询条件
 */
function initSearchCondition() {
    getBaseInfoTable();

    setSelectDataValue("data-table-select","data-table");
    setSelectDataValue("data-iden-select","data-iden");
    setSelectDataValue("data-where-select","data-where");

    /**
     * @bind
     * 指标鼠标悬浮即加载数据
     */
    $("#data-iden-select").bind("mouseover",function () {
        getBaseInfoIden();
        //参数赋值
        $(this).val($("#data-iden").val());
    });
    /**
     * @bind
     * 条件鼠标悬浮即加载
     */
    $("#data-where-select").bind("mouseover",function () {
        getCondition();
        //参数赋值
        $(this).val($("#data-where").val());
    });


    searchTable();
}

/**
 * @f1 初始化工商数据表格 --> @f2
 */
function getBaseInfoTable() {
    SGIS.API.get("search/gs/table/name").json(function(re){
       instance("data-table-select",re);
    });
}


/**
 * @module
 * @f2 初始化数据表选项
 * @param id select ID 初始化
 * @param re 后台查询结果
 */
function instance(id,re){
    var len = re.length;
    var html = "";
    for(var i=0;i<len;i++){
        var tr = "<option class='data-select' value='"+re[i][0]+"'>"+re[i][1]+"</option>>";
        html += tr;
    }
    $("#"+id).html(html);

}

/**
 * @module
 * @f3 选择下拉框赋值给页面中的数据表表单
 * @param select_id select选择框
 * @param input_id  input表单
 */
function setSelectDataValue(select_id,input_id) {
    $("#"+select_id).bind("click","."+input_id,function () {
        $("#"+input_id).val($(this).val());
        $(this).val($("#"+input_id).val());
    })
}


/**
 * @f4 初始化工商数据指标 --> @f2
 */
function getBaseInfoIden() {
    var mitmid = $("#data-table").val();
    SGIS.API.get("search/gs/iden/name").data({mitmid:mitmid}).json(function(re){
        instance("data-iden-select",re);
    });
}


/**
 * @f5 初始化条件字典
 */
function getCondition() {
    SGIS.API.get("search/gs/condition").json(function(re){
        instance("data-where-select",re);
    });
}

/**
 * @f6 查询事件
 */
function searchTable() {
    $("#search").bind("click",function () {
       init_table();
    })
}



/**
 * Columns格式
 * EasyUI 列名格式实例 后台传输JSON格式按照此标准
 * 后台传输标准为 List<List<JSON>>格式，将查询结果转化为JSONObject对象，
 * 通过List存放JSONObject对象，切记外面再封装一层List
 *
var columns = [
    [
        { field: "HouseNo", title: "企业唯一标识", width: 80, align: "center" },
        { field: "HouseDoorplate", title: "注册号", width: 80, align: "center" },
        { field: "RentRange", title: "社会统一信用代码", width: 80, align: "center" },
        { field: "ContractNo", title: "详细地址", width: 80, align: "center" }
    ]
];
*/

/**
 * 加载EasyUI表格
 * 事件触发先加载列名,异步加载数据
 */
var init_table = function () {
    var table = $("#data-table").val();
    var iden = $("#data-iden").val();
    var where = $("#data-where").val();
    var value = $("#data-value").val();
    //通过ajax请求生成的新的datagrid的列名
    $.ajax({
        url:"/data/search/gs/title",
        type:"GET",
        dataType:'json',
        data:{table:table},
        rownumbers: true, //行号
        pagination: true, //分页控件
        pageSize: 20,
        pageList: [10, 20, 30, 50, 100, 150, 200, 300, 500],
        success:function(data){
            //获取表头数据成功后，使用easyUi的datagrid去生成表格
            $('#table').datagrid({
                url: "/data/search/gs/data",
                method:"GET",
                contentType: "application/json",
                columns:data,//外层ajax请求的表头json
                queryParams:{table:table,iden:iden,where:where,value:value},
                rownumbers: true, //行号
                pagination: true, //分页控件
                pageSize: 20,
                pageList: [10, 20, 30, 50, 100, 150, 200, 300, 500],
                striped:true,
                loadMsg:"正在努力加载数据,表格渲染中...",
                onLoadSuccess: function (data) {
                    //console.log(data);
                },
                onLoadError:function () {
                    $(this).datagrid('loadData', { total: 0, rows: [] });
                    msgShow("warning","查询数据为空!",'warning');
                },
            });
        },
        error:function(e){
            msgShow("error","请求数据出错!",'error');
        }
    });
}


// 弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
function msgShow(title, msgString, msgType) {
    $.messager.alert(title, msgString, msgType);
}
