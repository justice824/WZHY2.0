$(function(){

    $("#gs-btn").bind("click",function(){
        $("#etps").html("");
        $("#etps").html("gs");
    })

    $("#ml-btn").bind("click",function(){
        $("#etps").html("");
        $("#etps").html("ml");
    })


    $("body").ready(
        refalshGS(),
        refalshML()
    );


    /**
     * 刷新汇总表
     */
    $("#refalsh").bind("click",function(){
        SGIS.UI.alert("正在更新中......请稍候!");

        SGIS.API.get("dataComparse/ml/refalsh")
            .json(function(re){
                if(re){
                    $("#ml-text").html("");
                    $("#ml-text").html("后台名录汇总数据已更新! " + getNowFormatDate());
                    refalshML();
                }
            });

        SGIS.API.get("dataComparse/gs/refalsh")
            .json(function(re){
                if(re){
                    $("#gs-text").html("");
                    $("#gs-text").html("后台工商汇总数据已更新! " + getNowFormatDate());
                    refalshGS();
                }
            });
    })

    /**
     * 下载汇总结果表 - 页面显示那个表就下载那个表
     */
    $("#download").on("click",function(){
        var th = "";
        $("#tables-gs tr th").each(function(){
            th += $(this).text()+",";
        });
       if(confirm("确定下载汇总结果吗?")){
           var etps = $("#etps").html();
           if(etps == "gs"){
               SGIS.API.get("dataComparse/download")
                   .data({
                       excel_head:th.substr(0,th.length-1),
                       etp:"gs",
                       title:"工商汇总数据"
                   })
                   .text(function(){
                       SGIS.Util.downloadData("ifr-download",SGIS.API.getURL("common/download/excel"));
                       SGIS.UI.alert("下载工商汇总数据成功！");
                   });
           }
           if(etps == "ml"){
               SGIS.API.get("dataComparse/download")
                   .data({
                       excel_head:th.substr(0,th.length-1),
                       etp:"ml",
                       title:"名录汇总数据"
                   })
                   .text(function(){
                       SGIS.Util.downloadData("ifr-download",SGIS.API.getURL("common/download/excel"));
                       SGIS.UI.alert("下载名录汇总数据成功！");
                   });
           }

        };

    });

});

/**
 * 刷新工商汇总表函数
 */
function refalshGS(){
    getTable("dataComparse/gs/getData","#tables-gs");
}

/**
 * 刷新名录汇总表函数
 */
function refalshML(){
    getTable("dataComparse/ml/getData","#tables-ml");
}

/**
 * 构建数据比对 table
 * @param Url 后台数据比对表接口
 * @param tables 后台数据比对表
 */
var getTable = function(Url,tables){
    $(tables).html("");
    var th = "<thead><tr>" +
        "<th>数据提取时间</th>"+
        "<th>企业信息明细</th>"+
        "<th>股东信息</th>"+
        "<th>法定代表人信息</th>"+
        "<th>财务负责人信息</th>"+
        "<th>变更登记信息</th>"+
        "<th>企业注销信息</th>"+
        "<th>隶属企业信息</th>"+
        "<th>年报基本信息</th>"+
        "<th>股东出资信息</th>"+
        "<th>对外投资信息</th>"+
        "<th>网站网店信息</th>"+
        "<th>股权变更信息</th>"+
        "<th>经营异常名录</th>"+
        "<th>违法失信企业</th>"+
        "</tr></thead>";
    var table = "";
    var count1 = 0;
    var count2 = 0;
    var count3 = 0;
    var count4 = 0;
    var count5 = 0;
    var count6 = 0;
    var count7 = 0;
    var count8 = 0;
    var count9 = 0;
    var count10 = 0;
    var count11 = 0;
    var count12 = 0;
    var count13 = 0;
    var count14 = 0;
    SGIS.API.get(Url)
        .json(function(re){
            for(var i = 0;i < re.length;i++){
                var tr =
                    " <tr class='gs-base'>"+
                        "<td style='min-width: 100px;'>"+re[i][0]+"</td>"+
                        "<td>"+format(re[i][1])+"</td>"+
                        "<td>"+format(re[i][2])+"</td>"+
                        "<td>"+format(re[i][3])+"</td>"+
                        "<td>"+format(re[i][4])+"</td>"+
                        "<td>"+format(re[i][5])+"</td>"+
                        "<td>"+format(re[i][6])+"</td>"+
                        "<td>"+format(re[i][7])+"</td>"+
                        "<td>"+format(re[i][8])+"</td>"+
                        "<td>"+format(re[i][9])+"</td>"+
                        "<td>"+format(re[i][10])+"</td>"+
                        "<td>"+format(re[i][11])+"</td>"+
                        "<td>"+format(re[i][12])+"</td>"+
                        "<td>"+format(re[i][13])+"</td>"+
                        "<td>"+format(re[i][14])+"</td>"+
                    "</tr>";
                table+=tr;
                count1 = parseInt(format(count1)) + parseInt(format(re[i][1]));
                count2 = parseInt(format(count2)) + parseInt(format(re[i][2]));
                count3 = parseInt(format(count3)) + parseInt(format(re[i][3]));
                count4 = parseInt(format(count4)) + parseInt(format(re[i][4]));
                count5 = parseInt(format(count5)) + parseInt(format(re[i][5]));
                count6 = parseInt(format(count6)) + parseInt(format(re[i][6]));
                count7 = parseInt(format(count7)) + parseInt(format(re[i][7]));
                count8 = parseInt(format(count8)) + parseInt(format(re[i][8]));
                count9 = parseInt(format(count9)) + parseInt(format(re[i][9]));
                count10 = parseInt(format(count10)) + parseInt(format(re[i][10]));
                count11 = parseInt(format(count11)) + parseInt(format(re[i][11]));
                count12 = parseInt(format(count12)) + parseInt(format(re[i][12]));
                count13 = parseInt(format(count13)) + parseInt(format(re[i][13]));
                count14 = parseInt(format(count14)) + parseInt(format(re[i][14]));

            }
            table +=  " <tr class='gs-base' style='color: crimson;font-weight: bold'>"+
                        "<td> 汇总结果 </td>"+
                        "<td>"+count1+"</td>"+
                        "<td>"+count2+"</td>"+
                        "<td>"+count3+"</td>"+
                        "<td>"+count4+"</td>"+
                        "<td>"+count5+"</td>"+
                        "<td>"+count6+"</td>"+
                        "<td>"+count7+"</td>"+
                        "<td>"+count8+"</td>"+
                        "<td>"+count9+"</td>"+
                        "<td>"+count10+"</td>"+
                        "<td>"+count11+"</td>"+
                        "<td>"+count12+"</td>"+
                        "<td>"+count13+"</td>"+
                        "<td>"+count14+"</td>"+
                       "</tr>";
            $(tables).html(th+table);
        });


    function format(re){
       if(re == null){
           return 0;
       }else{
           return re;
       }
    }

}


/**
 * 当前系统时间 yyyy-mm-dd
 * @returns {string}
 */
function getNowFormatDate() {
    var date = new Date();
    var seperator = "-";
    var month = date.getMonth() + 1;
    if(month <= 9){
        month = "0" + month;
    }
    var strDate = date.getDate();
    if(strDate <= 9 && strDate > 0){
        strDate = "0" + strDate;
    }

    var t = ":";
    var hour = date.getHours();
    if(hour <= 9){
        hour = "0"+hour;
    }
    var min = date.getMinutes();
    if(min <= 9){
        min = "0"+min;
    }
    var sec = date.getSeconds();
    if(sec <= 9){
        sec = "0"+sec;
    }
    return date.getFullYear() + seperator + month + seperator + strDate + " " + hour + t + min + t + sec + " ";
}

